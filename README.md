<h1 align="center"> ✨ Power price ✨ </h1>

[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=PowerPrice%3AGridSimulationService&metric=alert_status)](https://sonarcloud.io/dashboard?id=PowerPrice%3AGridSimulationService)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=PowerPrice%3AGridSimulationService&metric=vulnerabilities)](https://sonarcloud.io/dashboard?id=PowerPrice%3AGridSimulationService)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=PowerPrice%3AGridSimulationService&metric=code_smells)](https://sonarcloud.io/dashboard?id=PowerPrice%3AGridSimulationService)

<img src="docs/powerprice-logo.jpg"  style="width:300px;"/>

Een simulatie van het Brabantse energienet gebaseerd op schaalbare microservices.

> Het “Energy Grid” project is een idee van Fontys Hogescholen om een regionaal energienet zo realistisch mogelijk te simuleren. De projectgroep “Energyneers” is één van de groepen die hiermee aan de slag gaat. Het project waarmee de projectgroep het Energy Grid idee wil realiseren heet “PowerPrice”.

> Om een energienet te simuleren moet er rekening gehouden worden met verschillende factoren. Zo heeft het Nederlandse energienet verschillende lagen. Allereerst is er de systeem operator, TenneT, die de enige verantwoordelijke stakeholder is voor het managen van het hoogspanningsnet. In de tweede laag komen de regionale energienetten terug. Deze netten zijn in handen van verschillende bedrijven, genaamd netbeheerders zoals Eneco en Essent.

### Beschikbare URLs
*Staging environment is in te zien door middel van een `staging.` prefix voor de gewenste URL*

| URL                                 | Beschrijving                                                                        |
| ----------                          | ---------------                                                                     |
| powerprice.marstan.net              | Dashboard voor energieoverzicht van een klant |                                     |
| backoffice.powerprice.marstan.net   | Dashboard voor medewerkers van netbeheeder                                          |
| api.powerprice.marstan.net          | API-gateway voor powerprice.marstan.net en backoffice.powerprice-marstan.net        |
| grafana.powerprice.marstan.net      | Grafana dashboard voor monitoring van het K8S cluster                               |
| kiali.powerprice.marstan.net        | Kiali dashboard voor monitoring van het K8S cluster                                 |
| identity.powerprice.marstan.net     | Keycloak instantie voor powerprice.marstan.net en backoffice.powerprice.marstan.net |
| kafka.powerprice.marstan.net        | GUI van monitoring van Kafka genaamd AKHQ (https://github.com/tchiotludo/akhq)      |
| external.powerprice.marstan.net     | Externe API om te gebruiken door andere groepen van het Energy Grid project         |


### Technology Stack

| Component          | Technology                                                                              |
| ------------------ | --------------------------------------------------------------------------------------- |
| Frontend           | [Angular 9.x](https://github.com/angular/angular) (Typescript)                          |
| Backend (REST)     | [SpringBoot 2.x](https://projects.spring.io/spring-boot) (Java)                         |
| Security           | Identity with KeyCloak and Spring Security and [JWT](https://github.com/auth0/java-jwt) |
| REST Documentation | [Swagger UI / Springfox](https://github.com/springfox/springfox))                       |
| REST Spec          | [Open API Standard](https://www.openapis.org/)                                          |
| Databases          | PostgreSQL                                                                              |
| Messaging bus      | Apache Kafka                                                                            |
| Persistence        | Spring JPA using Hibernate                                                              |
| Client Build Tools | [angular-cli](https://github.com/angular/angular-cli), Webpack, npm                     |
| Server Build Tools | Maven (Java 11)                                                                         |

### Accessing microservices for developers

| Component               | PORT             |
| ----------------------- | ---------------- |
| grid simulation service | 7100             |
| user service            | 7200             |
| windmill                | 7300             |
| solar panel             | 7400             |
| grid service            | 7500             |
| api gateway             | 7700             |
| external price          | 7800             |   
| keycloak                | 8080             |

## FHICT GitLab code repositories

[Repo link](https://git.fhict.nl/I359216/powerprice.git)

## Architectuur

<img src="docs/architectuur.png"  style="width:300px;"/>

### Configuration

Helm values to define for Grafana:

| Parameter  | Description      | Default |
| ---------- | ---------------  | ------- |
| `credentials.username` | Username to log in on Grafana | `""` |
| `credentials.passphrase` | Password to log in on Grafana | `""` |
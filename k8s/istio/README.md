# Istio configuratie van PowerPrice
[Klik hier voor quick start van Istio](https://istio.io/docs/setup/install/istioctl/)

**HUIDIG GEBRUIKTE VERSIE IS 1.6.0**

## Prerequisites
- istioctl is geinstalleerd

## Installeer secrets en Ingress resources
In de directory van deze README zijn twee Helm charts te vinden, namelijk voor Kiali en voor Grafana. Deze hebben ieder een Ingress resource, om de service bloot te stellen aan het internet en een secret. De secrets bevatten de credentials
om in te loggen op Kiali/Grafana. De username en password moeten een waarde krijgen door middel van Helm values. Hieronder twee voorbeelden van commands om beide Helm charts te deployen:

`helm install grafana grafana/ --namespace istio-system --set credentials.username=<base64 encoded username> --set credentials.passphrase=<base64 encoded password>`

`helm install kiali kiali/ --namespace istio-system --set credentials.username=<base64 encoded username> --set credentials.passphrase=<base64 encoded password>`

De Ingress resources zouden ook veranderd kunnen worden in Istio Ingress gateways, maar dit valt buiten de scope van het PowerPrice project om nog geconfigureerd te krijgen.


## Installatie van Istio op het cluster
PowerPrice gebruikt de demo configuratie van Istio. Dit betekent dat alle resources zoals Kiali, Grafana, Pilot, Jaeger etc. allemaal uitgerold worden en onder de istio-system namespace komen van het cluster. Om via de demo profile wel de preconfigureerde credentials te gebruiken in de vorige stap, zal de volgende command gebruikt moeten worden om istio te installeren op het cluster:

`istioctl manifest apply --set profile=demo --set values.grafana.security.enabled=true --set values.kiali.createDemoSecret=false`


## Istio labelled namespaces
Een namespace moet de `istio-injection=enabled` label hebben om geanalyseerd te worden door Istio. Dit wordt nu alleen nog gedaan voor de `powerprice-staging` namespace. Als het nodig is om nog andere namespaces te analyzeren, zorg dan dat de command `kubectl label namespace <namespace hier> istio-injection=enabled` uitgevoerd wordt. Istio zal op deze manier automatisch Envoy proxies toevoegen aan de pods die in deze namespace uitgerold worden, zodat traffic kan worden nagekeken.

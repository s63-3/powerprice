### Configuration

Helm values to define for Kiali:

| Parameter  | Description      | Default |
| ---------- | ---------------  | ------- |
| `credentials.username` | Username to log in with on Kiali | `""` |
| `credentials.password` | Password to log in with on Kiali | `""` |

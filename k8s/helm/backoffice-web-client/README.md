### Configuration

Helm values to define for backoffice-web-client:

| Parameter  | Description      | Default |
| ---------- | ---------------  | ------- |
| `urls.gatewayUrl` | URL to the gateway to communicate with |   `https://api.powerprice.marstan.net`  |
| `urls.keycloakUrl` | URL to the Keycloak instance for auth | `https://identity.powerprice.marstan.net` |
| `urls.hostname` | Hostname where current instance is hosted on | `backoffice.powerprice.marstan.net` |
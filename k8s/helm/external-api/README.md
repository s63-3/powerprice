### Configuration

Helm values to define for external-api:

| Parameter  | Description      | Default |
| ---------- | ---------------  | ------- |
| `urls.hostname` | Hostname where current instance is hosted on | `external.powerprice.marstan.net` |
| `urls.keycloak` | URL to the Keycloak instance | `https://staging.identity.powerprice.marstan.net` |
### Configuration

Helm values to define for kafkahq:

| Parameter  | Description      | Default |
| ---------- | ---------------  | ------- |
| `urls.hostname` | Hostname where current instance is hosted on | `kafka.powerprice.marstan.net` |

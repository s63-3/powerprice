### Configuration

Helm values to define for weather-service:

| Parameter  | Description      | Default |
| ---------- | ---------------  | ------- |
| `weather.secret` | API key of the external weather API to connect with | `""` |

### Configuration

Helm values to define for keycloak:

| Parameter  | Description      | Default |
| ---------- | ---------------  | ------- |
| `urls.hostname` | Hostname where current instance is hosted on | `identity.powerprice.marstan.net` |

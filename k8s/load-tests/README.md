# K6 Load Testing


## Toevoegen van een nieuw script
Wanneer er een nieuw script toegevoegd moet worden, moeten de volgende zaken geregeld worden om een nieuw Grafana dashboard met data te krijgen:

### Nieuwe InfluxDB database toevoegen
Voor iedere load test moet een nieuwe Grafana datasource aangemaakt worden, zodat de load test data gescheiden blijft. Voeg hiervoor een nieuwe `CREATE DATABASE <database naam>` line toe onder `influxdb/influx_init.iql`.

### Nieuwe datasource toevoegen aan Grafana
Onder `grafana/datasources/datasources.yml` zal een nieuwe config toegevoegd moeten worden, zodat Grafana weet waar de nieuwe database te vinden is. De database kan dan later gebruikt worden om de data van de load test op te halen. Gebruik het volgende format in de `datasources.yml`:
```
- name: <naam van de datasource>
  type: influxdb
  access: proxy
  database: <database naam>
  url: http://influxdb:8086
```

### Uitbreiden van webpack.config.js
Alle scripts maken gebruik van webpack, zodat er gebruikt kan worden gemaakt van modules [Zie hier voor meer informatie](https://k6.io/docs/using-k6/modules). Hier zal het nieuwe load-test script aan toegevoegd moeten worden onder de `options`, op deze manier:
```
options: {
  <output file name>: <directory naar het script die dient als entrypoint>
}
```

### Uitbreiden van run-all-load-tests.sh
In de root van de load-tests directory is een .sh script te vinden die alle load-tests sequentieel runt. Hier zal het nieuwe gemaakte script via webpack uitgevoerd moeten worden met `k6 run`. Zorg dat ook `--out http://influxdb:8086/<database naam>` als argument is toegevoegd, zodat de resultaten worden geexporteerd naar de nieuwe database.

### Nieuw Grafana dashboard aanmaken
Als je de resultaten wilt zien op het Grafana dashboard, dan zul je handmatig een nieuw dashboard moeten aanmaken. Het externe dashboard wat momenteel gebruikt wordt in andere load tests is [deze](https://grafana.com/grafana/dashboards/11837). Zodra je deze importeert, kun je de datasource opgeven van je nieuwe load test. Als de docker-compose
gestopt wordt, zal het dashboard niet vergeten worden. Deze wordt opgeslagen in een docker volume.


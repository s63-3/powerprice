#!/bin/sh

# Keycloak Scripts
k6 run dist/keycloakLogInAsAdmin.bundle.js \
--out influxdb=http://influxdb:8086/keycloak-log-in-as-admin \
-e KEYCLOAK_ADMIN_USERNAME=${KEYCLOAK_ADMIN_USERNAME} \
-e KEYCLOAK_ADMIN_PASSWORD=${KEYCLOAK_ADMIN_PASSWORD}

k6 run dist/keycloakLogInAsPowerPriceUser.bundle.js \
--out influxdb=http://influxdb:8086/keycloak-log-in-as-powerprice-user \
-e POWERPRICE_EMPLOYEE_USERNAME=${POWERPRICE_EMPLOYEE_USERNAME} \
-e POWERPRICE_EMPLOYEE_PASSWORD=${POWERPRICE_EMPLOYEE_PASSWORD}

# Customer Web Client Scripts
k6 run dist/customerGetLandingPage.bundle.js \
--out influxdb=http://influxdb:8086/customer-web-client-get-landing-page

# Backoffice Web Client Scripts
k6 run dist/backofficeGetLandingPage.bundle.js \
--out influxdb=http://influxdb:8086/backoffice-web-client-get-landing-page

# API Gateway
k6 run dist/apiGatewayGetEmployeeDashboard.bundle.js \
--out influxdb=http://influxdb:8086/api-gateway-get-employee-dashboard
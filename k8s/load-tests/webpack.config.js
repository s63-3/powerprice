const path = require('path');

module.exports = {

  mode: 'development',

  entry: {
    // API GATEWAY
    apiGatewayGetEmployeeDashboard: './src/api-gateway/get-employee-dashboard.js',
    
    // KEYCLOAK
    keycloakLogInAsAdmin: './src/keycloak/log-in-as-admin.js',
    keycloakLogInAsPowerPriceUser: './src/keycloak/log-in-as-powerprice-user.js',

    // CUSTOMER WEB CLIENT
    customerGetLandingPage: './src/customer-web-client/get-landing-page.js',

    // BACKOFFICE WEB CLIENT
    backofficeGetLandingPage: './src/backoffice-web-client/get-landing-page.js'
  },
  
  devtool: 'inline-source-map',

  output: {
    path: path.resolve(__dirname, 'dist'),
    libraryTarget: 'commonjs',
    filename: '[name].bundle.js',
  },
  
  module: {
    rules: [
      { 
        test: /\.js$/, 
        use: 'babel-loader' 
      }
    ],
  },
  
  target: 'web',
  
  externals: /k6(\/.*)?/,
};
import http from 'k6/http';
import { check, sleep, fail } from 'k6';
import { authenticateWithKeycloak } from '../helpers/authenticate.helper';

export let options = {
  vus: 10,
  duration: '30s'
}

export function setup() {
  const AUTH_URL = 'https://staging.identity.powerprice.marstan.net/auth/realms/powerprice/protocol/openid-connect/token';
  const USERNAME = __ENV.POWERPRICE_EMPLOYEE_USERNAME;
  const PASSWORD = __ENV.POWERPRICE_EMPLOYEE_PASSWORD;
  const CLIENT_ID = 'webapp';
  const GRANT_TYPE = 'password';

  if (!USERNAME || !PASSWORD) {
    fail("Unable to resolve credentials for Keycloak authentication. Make sure to specify the POWERPRICE_EMPLOYEE_USERNAME and POWERPRICE_EMPLOYEE_PASSWORD variables through the k6 command.");
  }

  const authResponse = authenticateWithKeycloak(AUTH_URL, USERNAME, PASSWORD, CLIENT_ID, GRANT_TYPE);

  return {
    token: authResponse.access_token
  }
}

export default function(data) {
  const token = data.token;

  if (!token) {
    fail("Unable to resolve token. Test was unsuccessful during setup while authenticating.");
  }

  const options = {
    headers: {
      Authorization: `Bearer ${token}`
    }
  }

  const employeeDashboardUsageRequest = http.get('https://staging.api.powerprice.marstan.net/grid/employees/usage', options);
  const employeeDashboardProductionRequest = http.get('https://staging.api.powerprice.marstan.net/grid/employees/production', options);

  check(employeeDashboardUsageRequest, {
    'GET /grid/employees/usage is statuscode 200': r => r.status === 200,
  });
  check(employeeDashboardProductionRequest, {
    'GET /grid/employees/production is statuscode 200': r => r.status === 200
  });
  sleep(1);
}
import { fail } from 'k6';
import http from 'k6/http';

export function authenticateWithKeycloak(url, username, password, clientId, grantType) {

  const authResponse = http.post(url, {
    username: username,
    password: password,
    client_id: clientId,
    grant_type: grantType
  });

  if (authResponse.status !== 200) {
    fail('Request to authenticate with Keycloak as an employee failed. Are you sure you specified the correct credentials?');
  }

  return JSON.parse(authResponse.body);
}
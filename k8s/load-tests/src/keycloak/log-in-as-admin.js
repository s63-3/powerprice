import http from 'k6/http';
import {check, sleep, fail} from 'k6';

export let options = {
  vus: 10,
  duration: '30s'
}

const BASE_URL = 'https://staging.identity.powerprice.marstan.net';
const USERNAME = __ENV.KEYCLOAK_ADMIN_USERNAME;
const PASSWORD = __ENV.KEYCLOAK_ADMIN_PASSWORD;
const CLIENT_ID = 'admin-cli';
const GRANT_TYPE = 'password';

if (!USERNAME || !PASSWORD) {
  fail("Unable to resolve credentials for Keycloak authentication. Make sure to specify the KEYCLOAK_ADMIN_USERNAME and KEYCLOAK_ADMIN_PASSWORD variables through the k6 command.");
}

export default () => {
  let res = http.post(`${BASE_URL}/auth/realms/master/protocol/openid-connect/token/`, {
    username: USERNAME,
    password: PASSWORD,
    client_id: CLIENT_ID,
    grant_type: GRANT_TYPE
  });

  check(res, {
    'status is 200': r => r.status === 200
  });

  sleep(1);
}
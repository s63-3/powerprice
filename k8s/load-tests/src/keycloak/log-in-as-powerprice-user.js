import http from 'k6/http';
import {check, sleep, fail} from 'k6';

export let options = {
  vus: 10,
  duration: '30s'
}

const URL = 'https://staging.identity.powerprice.marstan.net/auth/realms/powerprice/protocol/openid-connect/token';
const USERNAME = __ENV.POWERPRICE_EMPLOYEE_USERNAME;
const PASSWORD = __ENV.POWERPRICE_EMPLOYEE_PASSWORD;
const CLIENT_ID = 'webapp';
const GRANT_TYPE = 'password';

if (!USERNAME || !PASSWORD) {
  fail("Unable to resolve credentials for Keycloak authentication. Make sure to specify the POWERPRICE_USERNAME and POWERPRICE_PASSWORD variables through the k6 command.");
}

export default () => {
  let res = http.post(URL, {
    username: USERNAME,
    password: PASSWORD,
    client_id: CLIENT_ID,
    grant_type: GRANT_TYPE
  });

  check(res, {
    'status is 200': r => r.status === 200
  });

  sleep(1);
}
package com.powerprice.userservice;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.powerprice.userservice.controller.CustomerController;
import com.powerprice.userservice.enums.BuildingType;
import com.powerprice.userservice.model.Customer;
import com.powerprice.userservice.service.CustomerService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;


import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;


@WebMvcTest(CustomerController.class)
public class CustomerControllerTest {
  @Autowired private MockMvc mockMvc;


  @MockBean
  CustomerService customerService;

  @MockBean private CustomerController customerController;


  private static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));
  @Test
  public void testGetCustomerById() throws Exception {

    when(customerController.getCustomerById(1L))
        .thenReturn(
            new Customer(
                1L,
                "code",
                "email@email.com",
                "firstname",
                "lastname",
                "streetname",
                "streetnumber",
                "postaladdress",
                "country",
                BuildingType.APPARTEMENT,
                "language",
                true,
                "companyname",
                1L,
                2L,
                3L));

    this.mockMvc.perform(get("/customers/1")).andDo(print()).andExpect(status().isOk());
  }

  @Test
  public void getIdByEmail() throws Exception {
    when(customerService.findCustomerIdByEmail("email@email.com"))
            .thenReturn(100L);

    this.mockMvc.perform(get("/customers/email/email@email.com")).andDo(print()).andExpect(status().isOk());
  }

  @Test
  public void registerCustomer() throws Exception {
    Customer newCustomer = new Customer(
                    100L,
                    "code",
                    "email@email.com",
                    "firstname",
                    "lastname",
                    "streetname",
                    "streetnumber",
                    "postaladdress",
                    "country",
                    BuildingType.APPARTEMENT,
                    "language",
                    true,
                    "companyname",
                    1L,
                    2L,
                    3L
            );
    when(customerService.createCustomer(newCustomer)).thenReturn(100L);

    ObjectMapper mapper = new ObjectMapper();
    mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
    ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
    String userJson =  ow.writeValueAsString(newCustomer);
    this.mockMvc.perform(post("/customers/register").contentType(APPLICATION_JSON_UTF8).content(userJson)).andDo(print()).andExpect(status().isOk());
  }

  @Test
  public void getUsageOfCustomer() throws Exception{
    when(customerService.getUsageOfCustomer(1L)).thenReturn(100);
    this.mockMvc.perform(get("/customers/usage/1")).andDo(print()).andExpect(status().isOk());
  }

  @Test
  public void getTotalUsageOfCustomer() throws Exception {
    when(customerService.getTotalUsage()).thenReturn(12345);
    this.mockMvc.perform(get("/customers/usage")).andDo(print()).andExpect(status().isOk());
  }

  @Test
  public void getAllCustomers() throws Exception {
    List<Customer> customerList = new ArrayList<>();
    customerList.add(new Customer(
            1L,
            "code",
            "email1@email.com",
            "firstname",
            "lastname",
            "streetname",
            "streetnumber",
            "postaladdress",
            "country",
            BuildingType.APPARTEMENT,
            "language",
            true,
            "companyname",
            1L,
            2L,
            3L
    ));
    customerList.add(new Customer(
            2L,
            "code",
            "email2@email.com",
            "firstname",
            "lastname",
            "streetname",
            "streetnumber",
            "postaladdress",
            "country",
            BuildingType.APPARTEMENT,
            "language",
            true,
            "companyname",
            1L,
            2L,
            3L
    ));
    when(customerService.getAllCustomers()).thenReturn(customerList);
    this.mockMvc.perform(get("/customers")).andDo(print()).andExpect(status().isOk());
  }
}

package com.powerprice.userservice;

import static org.assertj.core.api.Assertions.assertThat;

import com.powerprice.userservice.controller.CustomerController;
import com.powerprice.userservice.controller.EmployeeController;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@WebMvcTest(CustomerController.class)
class UserServiceApplicationTests {

  @MockBean private CustomerController customerController;
  @MockBean private EmployeeController employeeController;

  @Test
  void contextLoads() throws Exception {
    assertThat(customerController).isNotNull();
    assertThat(employeeController).isNotNull();
  }
}

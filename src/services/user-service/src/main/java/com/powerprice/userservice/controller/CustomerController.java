package com.powerprice.userservice.controller;

import com.powerprice.userservice.model.Customer;
import com.powerprice.userservice.service.CustomerService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/customers")
public class CustomerController {
  private CustomerService customerService;

  @Autowired
  public CustomerController(CustomerService customerService) {
    this.customerService = customerService;
  }

  @GetMapping
  public List<Customer> getCustomers() {
    return customerService.getAllCustomers();
  }

  @GetMapping("/{id}")
  public Customer getCustomerById(@PathVariable Long id) {
    return customerService.findCustomerById(id);
  }

  @PostMapping("/register")
  public Long registerCustomer(@RequestBody Customer customer) throws Exception {
    return customerService.createCustomer(customer);
  }

  @GetMapping("/email/{email}")
  public Long getIdByEmail(@PathVariable String email) {
    return customerService.findCustomerIdByEmail(email);
  }

  @GetMapping("/usage/{id}")
  public ResponseEntity<Integer> getUsageOfCustomer(@PathVariable Long id) {
    int usage = customerService.getUsageOfCustomer(id);
    return new ResponseEntity<Integer>(usage, HttpStatus.OK);
  }

  @GetMapping("/usage")
  public ResponseEntity<Integer> getTotalUsage() {
    int usage = customerService.getTotalUsage();
    return new ResponseEntity<Integer>(usage, HttpStatus.OK);
  }
}

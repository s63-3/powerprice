package com.powerprice.userservice.enums;

public enum BuildingType {
  RIJTJESWONING,
  VRIJSTAANDEWONING,
  APPARTEMENT,
  BEDRIJFSGEBOUW,
}

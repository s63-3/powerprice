package com.powerprice.userservice.service;

import java.util.Arrays;
import javax.ws.rs.core.Response;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.keycloak.admin.client.CreatedResponseUtil;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class KeycloakService {
  private final Logger logger = LogManager.getLogger(KeycloakService.class);

  @Value("${keycloak.url}")
  private String keycloakUrl;

  @Value("${keycloak.realm}")
  private String realm;

  @Value("${keycloak.adminUsername}")
  private String adminUsername;

  @Value("${keycloak.adminPassword}")
  private String adminPassword;

  @Value("${keycloak.clientId}")
  private String clientId;

  public Keycloak getInstance() {
    return Keycloak.getInstance(keycloakUrl, realm, adminUsername, adminPassword, clientId);
  }

  public UserRepresentation getUserRepresentation(String name, String email) {
    UserRepresentation keycloakUser = new UserRepresentation();
    keycloakUser.setEnabled(true);
    keycloakUser.setUsername(name);
    keycloakUser.setEmail(email);
    return keycloakUser;
  }

  public RealmResource getRealmResource(Keycloak keycloak) {
    return keycloak.realm(realm);
  }

  public Response createUser(RealmResource realmResource, UserRepresentation userRepresentation) {
    UsersResource usersRessource = realmResource.users();
    Response response = usersRessource.create(userRepresentation);
    String userId = CreatedResponseUtil.getCreatedId(response);
    UserResource userResource = usersRessource.get(userId);
    RoleRepresentation testerRealmRole = realmResource.roles().get("customer").toRepresentation();
    userResource.roles().realmLevel().add(Arrays.asList(testerRealmRole));
    return response;
  }

  public Response dataLoaderCreateUser(
      RealmResource realmResource, UserRepresentation userRepresentation) {
    UsersResource usersRessource = realmResource.users();
    Response response = usersRessource.create(userRepresentation);
    if (response.getStatusInfo() == Response.Status.CREATED) {
      String userId = CreatedResponseUtil.getCreatedId(response);
      UserResource userResource = usersRessource.get(userId);
      RoleRepresentation testerRealmRole = realmResource.roles().get("customer").toRepresentation();

      userResource.roles().realmLevel().add(Arrays.asList(testerRealmRole));

      return response;
    } else if (response.getStatusInfo() == Response.Status.CONFLICT) {
      logger.log(Level.INFO, "A user with the same email already exists, please change it");
    } else {
      logger.log(
          Level.INFO,
          "Our authentication server is not communicating correctly, please try again later or contact us");
    }
    return null;
  }

  public UserRepresentation dataLoaderGetUserRepresentation(String name, String email) {
    CredentialRepresentation credential = new CredentialRepresentation();
    credential.setType(CredentialRepresentation.PASSWORD);
    credential.setValue("tasty1234");
    UserRepresentation keycloakUser = new UserRepresentation();
    keycloakUser.setEnabled(true);
    keycloakUser.setUsername(name);
    keycloakUser.setEmail(email);
    keycloakUser.setCredentials(Arrays.asList(credential));
    return keycloakUser;
  }
}

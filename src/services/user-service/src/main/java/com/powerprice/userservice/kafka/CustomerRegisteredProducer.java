package com.powerprice.userservice.kafka;

import com.powerprice.userservice.model.Customer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class CustomerRegisteredProducer {
  private static final Logger logger = LoggerFactory.getLogger(CustomerRegisteredProducer.class);
  private static final String TOPIC = "customerRegistered";

  private KafkaTemplate<String, Customer> kafkaTemplate;

  public CustomerRegisteredProducer(KafkaTemplate<String, Customer> kafkaTemplate) {
    this.kafkaTemplate = kafkaTemplate;
  }

  public void sendMessage(Customer customer) {
    logger.info(
        String.format(
            "Publishing user registered event to Kafka with customer id %d", customer.getId()));
    this.kafkaTemplate.send(TOPIC, customer);
  }
}

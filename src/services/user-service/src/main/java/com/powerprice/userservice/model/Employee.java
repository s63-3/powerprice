package com.powerprice.userservice.model;

import javax.persistence.*;

@Entity
public class Employee {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private String code;

  @Column(name = "email_address")
  private String emailAddress;

  @Column(name = "first_name")
  private String firstName;

  @Column(name = "last_name")
  private String lastName;

  @Column(name = "company_name")
  private String companyName;

  public Employee() {}

  public Employee(
      Long id,
      String code,
      String emailAddress,
      String firstName,
      String lastName,
      String companyName) {
    this.id = id;
    this.code = code;
    this.emailAddress = emailAddress;
    this.firstName = firstName;
    this.lastName = lastName;
    this.companyName = companyName;
  }

  public Long getId() {
    return id;
  }

  public String getCode() {
    return code;
  }

  public String getEmailAddress() {
    return emailAddress;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public String getCompanyName() {
    return companyName;
  }
}

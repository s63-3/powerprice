package com.powerprice.userservice.listener;

import org.springframework.stereotype.Service;

@Service
public class KafkaConsumer {

  //    @KafkaListener(topics = "CustomerTopic", groupId = "group_id")
  public void consume(String message) {
    System.out.println("Received message: " + message);
  }
}

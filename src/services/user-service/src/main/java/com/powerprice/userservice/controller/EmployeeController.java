package com.powerprice.userservice.controller;

import com.github.javafaker.Faker;
import com.powerprice.userservice.model.Employee;
import com.powerprice.userservice.repo.EmployeeRepo;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/employees")
public class EmployeeController {
  @Autowired EmployeeRepo repo;

  Faker faker = new Faker(new Locale("nl"));

  @PostMapping("/register")
  public void insertFakeEmployees() {
    List<Employee> generatedEmployees = generateEmployees();
    repo.saveAll(generatedEmployees);
  }

  private List<Employee> generateEmployees() {
    List<Employee> generatedEmployees = new ArrayList<>();

    for (int i = 0; i < 100; i++) {
      Long id = i + 1L;
      String code = "C" + (i + 1000);
      String email = faker.internet().emailAddress();
      String firstName = faker.name().firstName();
      String lastName = faker.name().lastName();
      String companyName = faker.company().name();

      generatedEmployees.add(new Employee(id, code, email, firstName, lastName, companyName));
    }
    return generatedEmployees;
  }
}

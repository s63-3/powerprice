package com.powerprice.userservice.model;

import com.powerprice.userservice.enums.BuildingType;
import javax.persistence.*;

@Entity
public class Customer {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private String code;

  @Column(name = "email_address")
  private String emailAddress;

  @Column(name = "first_name")
  private String firstName;

  @Column(name = "last_name")
  private String lastName;

  @Column(name = "street_name")
  private String streetName;

  @Column(name = "street_number")
  private String streetNumber;

  @Column(name = "post_address")
  private String postAddress;

  private String country;

  @Column(name = "building_type")
  private BuildingType buildingType;

  @Column(name = "preffered_language")
  private String preferredLanguage;

  @Column(name = "is_verified_account")
  private boolean isVerifiedAccount;

  @Column(name = "company_name")
  private String companyName;

  @Column(name = "max_voltage_level")
  private Long maxVoltageLevel;

  @Column(name = "base_load")
  private Long baseLoad;

  @Column(name = "peak_load")
  private Long peakLoad;

  public Customer() {}

  public Customer(
      Long id,
      String code,
      String emailAddress,
      String firstName,
      String lastName,
      String streetName,
      String streetNumber,
      String postAddress,
      String country,
      BuildingType buildingType,
      String preferredLanguage,
      boolean isVerifiedAccount,
      String companyName,
      Long maxVoltageLevel,
      Long baseLoad,
      Long peakLoad) {
    this.id = id;
    this.code = code;
    this.emailAddress = emailAddress;
    this.firstName = firstName;
    this.lastName = lastName;
    this.streetName = streetName;
    this.streetNumber = streetNumber;
    this.postAddress = postAddress;
    this.country = country;
    this.buildingType = buildingType;
    this.preferredLanguage = preferredLanguage;
    this.isVerifiedAccount = isVerifiedAccount;
    this.companyName = companyName;
    this.maxVoltageLevel = maxVoltageLevel;
    this.baseLoad = baseLoad;
    this.peakLoad = peakLoad;
  }

  public Customer(
      String code,
      String emailAddress,
      String firstName,
      String lastName,
      String streetName,
      String streetNumber,
      String postAddress,
      String country,
      BuildingType buildingType,
      String preferredLanguage,
      boolean isVerifiedAccount,
      String companyName,
      Long maxVoltageLevel,
      Long baseLoad,
      Long peakLoad) {
    this.code = code;
    this.emailAddress = emailAddress;
    this.firstName = firstName;
    this.lastName = lastName;
    this.streetName = streetName;
    this.streetNumber = streetNumber;
    this.postAddress = postAddress;
    this.country = country;
    this.buildingType = buildingType;
    this.preferredLanguage = preferredLanguage;
    this.isVerifiedAccount = isVerifiedAccount;
    this.companyName = companyName;
    this.maxVoltageLevel = maxVoltageLevel;
    this.baseLoad = baseLoad;
    this.peakLoad = peakLoad;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getEmailAddress() {
    return emailAddress;
  }

  public void setEmailAddress(String emailAddress) {
    this.emailAddress = emailAddress;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getStreetName() {
    return streetName;
  }

  public void setStreetName(String streetName) {
    this.streetName = streetName;
  }

  public String getStreetNumber() {
    return streetNumber;
  }

  public void setStreetNumber(String streetNumber) {
    this.streetNumber = streetNumber;
  }

  public String getPostAddress() {
    return postAddress;
  }

  public void setPostAddress(String postAddress) {
    this.postAddress = postAddress;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public BuildingType getBuildingType() {
    return buildingType;
  }

  public void setBuildingType(BuildingType buildingType) {
    this.buildingType = buildingType;
  }

  public String getPreferredLanguage() {
    return preferredLanguage;
  }

  public void setPreferredLanguage(String preferredLanguage) {
    this.preferredLanguage = preferredLanguage;
  }

  public boolean isVerifiedAccount() {
    return isVerifiedAccount;
  }

  public void setVerifiedAccount(boolean verifiedAccount) {
    isVerifiedAccount = verifiedAccount;
  }

  public String getCompanyName() {
    return companyName;
  }

  public void setCompanyName(String companyName) {
    this.companyName = companyName;
  }

  public Long getMaxVoltageLevel() {
    return maxVoltageLevel;
  }

  public void setMaxVoltageLevel(Long maxVoltageLevel) {
    this.maxVoltageLevel = maxVoltageLevel;
  }

  public Long getBaseLoad() {
    return baseLoad;
  }

  public void setBaseLoad(Long baseLoad) {
    this.baseLoad = baseLoad;
  }

  public Long getPeakLoad() {
    return peakLoad;
  }

  public void setPeakLoad(Long peakLoad) {
    this.peakLoad = peakLoad;
  }
}

package com.powerprice.userservice.config;

import com.powerprice.userservice.enums.BuildingType;
import com.powerprice.userservice.model.Customer;
import com.powerprice.userservice.repo.CustomerRepo;
import com.powerprice.userservice.service.CustomerService;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;

import org.apache.http.conn.HttpHostConnectException;
import org.springframework.stereotype.Component;

@Component
public class DataLoader {
  private CustomerRepo customerRepo;
  private CustomerService customerService;

  public DataLoader(CustomerRepo customerRepo, CustomerService customerService) {
    this.customerRepo = customerRepo;
    this.customerService = customerService;
  }

  @PostConstruct
  void loadData() {
    dataThread();
  }

  private List<Customer> generateCustomers() {
    List<Customer> generatedCustomers = new ArrayList<>();

    for (int i = 10001; i < 10021; i++) {
      var id = (long) i;
      String code = "C" + Integer.toString(i + 1000);
      String email = "john" + Integer.toString(i + 1) + "@hotmail.com";
      String firstName = "john" + Integer.toString(i + 1);
      String lastName = "doe" + Integer.toString(i + 1);
      String streetName = "Dijkstraat";
      String streetNumber = "11";
      String postCode = "5543AA";
      String country = "Nederland";
      BuildingType buildingType = BuildingType.APPARTEMENT;
      String prefferedLanguage = "Nederlands";
      String companyName = "Google";
      int maxVoltageLevel = 100;
      int baseLoad = 100;
      int peakLoad = 100;

      Customer customer =
          new Customer(
              id,
              code,
              email,
              firstName,
              lastName,
              streetName,
              streetNumber,
              postCode,
              country,
              buildingType,
              prefferedLanguage,
              true,
              companyName,
              Long.valueOf(maxVoltageLevel),
              Long.valueOf(baseLoad),
              Long.valueOf(peakLoad));
      generatedCustomers.add(customer);
      try {
        customerService.dataLoaderCreateCustomer(customer);
      } catch (Exception e) {
        java.lang.System.exit(0);
      }
    }
    return generatedCustomers;
  }

  private void dataThread(){
    Runnable generateDataThread = () -> {
      while (true) {
        customerRepo.deleteAll();
        List<Customer> customers = generateCustomers();
        customerRepo.saveAll(customers);
        break;
      }
    };
    new Thread(generateDataThread).start();
  }
}

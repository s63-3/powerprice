package com.powerprice.userservice.repo;

import com.powerprice.userservice.model.Customer;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface CustomerRepo extends JpaRepository<Customer, Long> {
  @Query(value = "Select * from customer", nativeQuery = true)
  List<Customer> getAllCustomers();

  Customer findByEmailAddress(String emailAddress);

  Customer findCustomerById(long id);
}

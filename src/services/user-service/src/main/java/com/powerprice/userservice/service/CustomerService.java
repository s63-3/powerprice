package com.powerprice.userservice.service;

import com.powerprice.userservice.enums.BuildingType;
import com.powerprice.userservice.kafka.CustomerRegisteredProducer;
import com.powerprice.userservice.model.Customer;
import com.powerprice.userservice.repo.CustomerRepo;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerService {
  private final Logger logger = LogManager.getLogger(CustomerService.class);
  private CustomerRepo customerRepo;

  private KeycloakService keycloakService;

  private CustomerRegisteredProducer customerRegisteredProducer;

  @Autowired
  public CustomerService(
      CustomerRepo customerRepo,
      KeycloakService keycloakService,
      CustomerRegisteredProducer customerRegisteredProducer) {
    this.customerRepo = customerRepo;
    this.keycloakService = keycloakService;
    this.customerRegisteredProducer = customerRegisteredProducer;
  }

  @PostConstruct
  public void init() {
    // Default Customer accounts
    Customer defaultCustomer =
        new Customer(
            1L,
            "code",
            "customer@gmail.com",
            "Harrie",
            "Harrinson",
            "streetname",
            "streetnumber",
            "postaladdress",
            "country",
            BuildingType.APPARTEMENT,
            "language",
            true,
            "companyname",
            1L,
            2L,
            3L);
    customerRepo.save(defaultCustomer);
  }

  public List<Customer> getAllCustomers() {
    return customerRepo.getAllCustomers();
  }

  public Customer findCustomerById(Long id) {
    return customerRepo.findCustomerById(id);
  }

  public Long findCustomerIdByEmail(String email) {
    customerRepo.findAll().forEach(customer -> System.out.println(customer.getEmailAddress()));
    Customer customer = customerRepo.findByEmailAddress(email);
    return customer.getId();
  }

  public int getUsageOfCustomer(Long customerId) {
    Customer customer = customerRepo.findCustomerById(customerId);
    BuildingType buildingType = customer.getBuildingType();
    return getUsageFromBuildingType(buildingType);
  }

  public int getTotalUsage() {
    List<Customer> customers = customerRepo.findAll();
    List<BuildingType> buildingTypes =
        customers.stream().map(Customer::getBuildingType).collect(Collectors.toList());
    int usage = 0;
    for (BuildingType buildingType : buildingTypes) {
      usage += this.getUsageFromBuildingType(buildingType);
    }
    return usage;
  }

  public Long createCustomer(Customer customer) throws Exception {
    Keycloak keycloak = keycloakService.getInstance();
    RealmResource keycloakRealm = keycloakService.getRealmResource(keycloak);
    UserRepresentation userRepresentation =
        keycloakService.getUserRepresentation(customer.getFirstName(), customer.getEmailAddress());
    Response response = keycloakService.createUser(keycloakRealm, userRepresentation);
    if (response.getStatusInfo() == Status.CREATED) {
      customerRepo.save(customer);
      customerRegisteredProducer.sendMessage(customer);
      return customer.getId();
    } else if (response.getStatusInfo() == Status.CONFLICT) {
      throw new Exception("A user with the same email already exists, please change it");
    } else {
      throw new Exception(
          "Our authentication server is not communicating correctly, please try again later or contact us");
    }
  }

  public void dataLoaderCreateCustomer(Customer customer) {
    Keycloak keycloak = keycloakService.getInstance();
    RealmResource keycloakRealm = keycloakService.getRealmResource(keycloak);
    UserRepresentation userRepresentation =
        keycloakService.dataLoaderGetUserRepresentation(
            customer.getFirstName(), customer.getEmailAddress());
    Response response = keycloakService.dataLoaderCreateUser(keycloakRealm, userRepresentation);
    if (response == null) {
      return;
    } else if (response.getStatusInfo() == Status.CREATED) {
      customerRegisteredProducer.sendMessage(customer);
      logger.log(Level.INFO, "A user with is created.");
    } else if (response.getStatusInfo() == Status.CONFLICT) {
      logger.log(Level.INFO, "A user with the same email already exists, please change it");
    } else {
      logger.log(
          Level.INFO,
          "Our authentication server is not communicating correctly, please try again later or contact us");
    }
  }

  private int getUsageFromBuildingType(BuildingType buildingType) {
    switch (buildingType) {
      case APPARTEMENT:
        // jaarlijks 2280 kwh
        // per maand 190 kwh
        return 190;
      case RIJTJESWONING:
        // jaarlijks 2860 kwh
        // per maand 238 kwh
        return 238;
      case BEDRIJFSGEBOUW:
        // jaarlijks 80.000 kwh
        // per maand 6667 kwh
        return 6667;
      case VRIJSTAANDEWONING:
        // jaarlijks 4490 kwh
        // per maand 374 kwh
        return 374;
      default:
        return 0;
    }
  }
}

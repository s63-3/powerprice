package com.powerprice.userservice.enums;

public enum GeneratorType {
  ZONNEPANEEL,
  WINDMOLEN,
  ELEKTRICITEITSCENTRALE,
  KOLENCENTRALE,
  KERNCENTRALE,
  GASCENTRALE,
}

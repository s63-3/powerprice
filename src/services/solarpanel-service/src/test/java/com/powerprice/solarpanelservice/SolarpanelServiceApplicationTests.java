package com.powerprice.solarpanelservice;

import static org.assertj.core.api.Assertions.assertThat;

import com.powerprice.solarpanelservice.controller.SolarPanelController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class SolarpanelServiceApplicationTests {
  @Autowired
  private SolarPanelController solarPanelController;

  @Test
  public void contextLoads() throws Exception {
    assertThat(solarPanelController).isNotNull();
  }
}

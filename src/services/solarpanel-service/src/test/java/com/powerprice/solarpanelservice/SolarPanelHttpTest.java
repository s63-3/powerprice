package com.powerprice.solarpanelservice;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.powerprice.solarpanelservice.model.SolarPanel;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class SolarPanelHttpTest {
  public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(
    MediaType.APPLICATION_JSON.getType(),
    MediaType.APPLICATION_JSON.getSubtype(),
    Charset.forName("utf8")
  );

  @Autowired
  private MockMvc mockMvc;

  @Test
  public void addSolarPanelsEndpointTest() throws Exception {
    //given
    List<SolarPanel> solarPanelList = new ArrayList<SolarPanel>() {

      {
        add(new SolarPanel(3, 10, 5));
        add(new SolarPanel(3, 12, 7));
        add(new SolarPanel(3, 8, 9));
      }
    };

    ObjectMapper mapper = new ObjectMapper();
    mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
    ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
    String requestJson = ow.writeValueAsString(solarPanelList);

    //when//then
    this.mockMvc.perform(
        post("/solarPanels")
          .contentType(APPLICATION_JSON_UTF8)
          .content(requestJson)
      )
      .andExpect(status().isCreated());
  }
}

package com.powerprice.solarpanelservice;

import com.powerprice.solarpanelservice.model.SolarPanel;
import com.powerprice.solarpanelservice.repo.SolarPanelRepo;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class SolarPanelRepositoryTest {
  @Autowired
  private TestEntityManager testEntityManager;

  @Autowired
  private SolarPanelRepo solarPanelRepo;

  @Test
  public void insertSolarPanelsTest() {
    //given
    List<SolarPanel> solarPanelList = new ArrayList<SolarPanel>() {

      {
        add(new SolarPanel(3, 10, 5));
        add(new SolarPanel(3, 12, 7));
        add(new SolarPanel(3, 8, 9));
      }
    };

    //when
    solarPanelRepo.saveAll(solarPanelList);

    //then
    List<SolarPanel> foundSolarPanelList = solarPanelRepo.findAll();
    Assert.assertEquals(solarPanelList, foundSolarPanelList);
  }
}

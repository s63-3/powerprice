package com.powerprice.solarpanelservice.service;

import com.powerprice.solarpanelservice.enums.ProductionSource;
import com.powerprice.solarpanelservice.kafka.ProductionProducer;
import com.powerprice.solarpanelservice.model.Production;
import com.powerprice.solarpanelservice.model.SolarPanel;
import com.powerprice.solarpanelservice.repo.SolarPanelRepo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SolarPanelService {
    private ProductionProducer productionProducer;
    private SolarPanelRepo solarPanelRepo;

    @Autowired
    public SolarPanelService(
            SolarPanelRepo solarPanelRepo,
            ProductionProducer productionProducer
    ) {
        this.solarPanelRepo = solarPanelRepo;
        this.productionProducer = productionProducer;
    }

    public List<SolarPanel> addSolarPanels(List<SolarPanel> solarPanels) {
        List<SolarPanel> insertedSolarPanels = solarPanelRepo.saveAll(solarPanels);
        return insertedSolarPanels;
    }

    public void getSolarPanelsProduction(
            double temperature,
            int uvIndex,
            double cloudCover,
            Long time
    ) {
        // Get all solarPanels of the grid.
        List<SolarPanel> solarPanelList = solarPanelRepo.findAll();

        // Calculate the production for each solar panel.
        for (SolarPanel solarPanel : solarPanelList) {
            double solarPanelProduction = 0;
            solarPanelProduction += getProductionOnTemperature(temperature);
            solarPanelProduction += getProductionOnUvIndex(uvIndex);
            solarPanelProduction += getProductionOnCloudCover(cloudCover);
            double totalSolarPanelProduction =
                    solarPanelProduction * solarPanel.getNumberOfSquareMeters();

            // Production each 4 minutes
            double productionPer4Minutes = totalSolarPanelProduction / 15;

            Production production = new Production(
                    ProductionSource.SOLAR_PANEL,
                    solarPanel.getCustomerId(),
                    productionPer4Minutes,
                    time
            );

            // Produce production to production topic.
            productionProducer.sendProduction(production);
        }
    }

    public double getWeatherSimulationProduction(
            double temperature,
            int uvIndex,
            double cloudCover
    ) {
        // Get all solarPanels of the grid.
        List<SolarPanel> solarPanelList = solarPanelRepo.findAll();

        double weatherSimulationProduction = 0;

        // Calculate the production for each solar panel.
        for (SolarPanel solarPanel : solarPanelList) {
            double solarPanelProduction = 0;
            solarPanelProduction += getProductionOnTemperature(temperature);
            solarPanelProduction += getProductionOnUvIndex(uvIndex);
            solarPanelProduction += getProductionOnCloudCover(cloudCover);
            double totalSolarPanelProduction =
                    solarPanelProduction * solarPanel.getNumberOfSquareMeters();

            weatherSimulationProduction += totalSolarPanelProduction;
        }
        return weatherSimulationProduction;
    }

    private double getProductionOnTemperature(double temperature) {
        double temperatureWatt = 0;
        if (temperature > 0 && temperature < 20) {
            temperatureWatt = (temperature * 5.25);
        } else if (temperature >= 20) {
            temperatureWatt = (20 * 5.25);
        }
        return temperatureWatt;
    }

    private double getProductionOnUvIndex(int uvIndex) {
        double uvIndexWatt = 0;
        if (uvIndex > 0 && uvIndex < 4) {
            uvIndexWatt = (uvIndex * 15);
        } else if (uvIndex >= 4) {
            uvIndexWatt = (4 * 15);
        }
        return uvIndexWatt;
    }

    private double getProductionOnCloudCover(double cloudCover) {
        double cloudCoverWatt = 0;
        if (cloudCover > 0.50 && cloudCover <= 1) {
            cloudCoverWatt = ((1 - cloudCover) * 100);
        } else if (cloudCover <= 0.50) {
            cloudCoverWatt = ((1 - 0.50) * 100);
        }
        return cloudCoverWatt;
    }
}

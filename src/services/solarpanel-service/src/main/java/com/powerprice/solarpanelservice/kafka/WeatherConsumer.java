package com.powerprice.solarpanelservice.kafka;

import com.powerprice.solarpanelservice.model.CurrentWeather;
import com.powerprice.solarpanelservice.service.SolarPanelService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class WeatherConsumer {
  private final Logger logger = LoggerFactory.getLogger(WeatherConsumer.class);
  private SolarPanelService solarPanelService;
  private static final String ConsumedMessage =
    "#### -> Consumed message -> %s";

  @Autowired
  public WeatherConsumer(SolarPanelService solarPanelService) {
    this.solarPanelService = solarPanelService;
  }

  @KafkaListener(topics = "weather", groupId = "WeatherConsumerGroup2")
  public void consume(CurrentWeather currentWeather) throws Exception {
    //Consume message from weather topic.
    logger.info(String.format(ConsumedMessage, currentWeather));

    //Calculate the production.
    solarPanelService.getSolarPanelsProduction(
      currentWeather.getCurrently().getTemperature(),
      currentWeather.getCurrently().getUvIndex(),
      currentWeather.getCurrently().getCloudCover(),
      currentWeather.getCurrently().getTime()
    );
  }
}

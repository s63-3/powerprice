package com.powerprice.solarpanelservice.controller;

import com.powerprice.solarpanelservice.model.SolarPanel;
import com.powerprice.solarpanelservice.model.WeatherForecast;
import com.powerprice.solarpanelservice.service.SolarPanelService;
import java.net.URISyntaxException;
import java.util.List;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

@RestController
@RequestMapping("/solar-panels")
public class SolarPanelController {
  private final Logger logger = LogManager.getLogger(
    SolarPanelController.class
  );
  private SolarPanelService solarPanelService;

  @Autowired
  public SolarPanelController(SolarPanelService solarPanelService) {
    this.solarPanelService = solarPanelService;
  }

  @InitBinder
  public void initBinder(WebDataBinder binder, WebRequest request) {
    binder.setAllowedFields("customerId", "numberOfSquareMeters", "amount");
  }

  @PostMapping
  public ResponseEntity<List<SolarPanel>> addSolarPanels(
    @RequestBody List<SolarPanel> solarPanels
  )
    throws URISyntaxException {
    List<SolarPanel> solarPanelList = solarPanelService.addSolarPanels(
      solarPanels
    );
    return new ResponseEntity<>(solarPanelList, HttpStatus.CREATED);
  }

  @PostMapping(value = "/weather-forecast/production")
  public ResponseEntity<Double> getProductionBasedOnWeather(
    @RequestBody WeatherForecast weatherForecast
  ) {
    logger.log(Level.INFO, "GetSolarPanelProductionOfWeather");
    return ResponseEntity.ok(
      solarPanelService.getWeatherSimulationProduction(
        weatherForecast.getTemperature(),
        weatherForecast.getUvIndex(),
        weatherForecast.getCloudCover()
      )
    );
  }
}

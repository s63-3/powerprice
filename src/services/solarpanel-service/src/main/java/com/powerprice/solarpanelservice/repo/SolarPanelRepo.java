package com.powerprice.solarpanelservice.repo;

import com.powerprice.solarpanelservice.model.SolarPanel;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SolarPanelRepo extends JpaRepository<SolarPanel, Long> {
  List<SolarPanel> findAllByCustomerId(int customerId);
}

package com.powerprice.solarpanelservice.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import net.minidev.json.annotate.JsonIgnore;

@Entity
public class SolarPanel {
  @JsonIgnore
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private int customerId;
  private int numberOfSquareMeters;
  private int amount;

  public SolarPanel() {}

  public SolarPanel(int customerId, int numberOfSquareMeters, int amount) {
    this.customerId = customerId;
    this.numberOfSquareMeters = numberOfSquareMeters;
    this.amount = amount;
  }

  public int getCustomerId() {
    return customerId;
  }

  public int getNumberOfSquareMeters() {
    return numberOfSquareMeters;
  }

  public int getAmount() {
    return amount;
  }

  public void setCustomerId(int customerId) {
    this.customerId = customerId;
  }

  public void setNumberOfSquareMeters(int numberOfSquareMeters) {
    this.numberOfSquareMeters = numberOfSquareMeters;
  }

  public void setAmount(int amount) {
    this.amount = amount;
  }
}

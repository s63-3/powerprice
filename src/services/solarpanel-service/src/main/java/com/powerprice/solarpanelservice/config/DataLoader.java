package com.powerprice.solarpanelservice.config;

import com.powerprice.solarpanelservice.model.SolarPanel;
import com.powerprice.solarpanelservice.repo.SolarPanelRepo;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Component
public class DataLoader {
  private final SolarPanelRepo solarPanelRepo;

  public DataLoader(SolarPanelRepo solarPanelRepo) {
    this.solarPanelRepo = solarPanelRepo;
  }

  @PostConstruct
  void loadData() {
    dataThread();
  }

  private List<SolarPanel> generateSolarPanels() {
    List<SolarPanel> generatedSolarPanels = new ArrayList<>();

    for (int i = 0; i < 20; i++) {
      int customerId = i + 1;

      generatedSolarPanels.add(new SolarPanel(customerId, 10, 3));
      generatedSolarPanels.add(new SolarPanel(customerId, 15, 4));
      generatedSolarPanels.add(new SolarPanel(customerId, 20, 2));
    }
    return generatedSolarPanels;
  }

  private void dataThread(){
    Runnable generateDataThread = () -> {
      while (true) {
        solarPanelRepo.deleteAll();
        List<SolarPanel> solarPanelList = generateSolarPanels();
        solarPanelRepo.saveAll(solarPanelList);
        break;
      }
    };
    new Thread(generateDataThread).start();
  }
}

package com.powerprice.solarpanelservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CurrentWeather {
  double latitude;
  double longitude;
  String timezone;
  Currently currently;
}

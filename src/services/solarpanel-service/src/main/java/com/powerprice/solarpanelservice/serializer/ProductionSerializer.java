package com.powerprice.solarpanelservice.serializer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.powerprice.solarpanelservice.model.Production;
import java.util.Map;
import org.apache.kafka.common.serialization.Serializer;

public class ProductionSerializer implements Serializer<Production> {

  @Override
  public byte[] serialize(String arg0, Production production) {
    byte[] retVal = null;
    ObjectMapper objectMapper = new ObjectMapper();
    try {
      retVal = objectMapper.writeValueAsString(production).getBytes();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return retVal;
  }
}

package com.powerprice.solarpanelservice.model;

import com.powerprice.solarpanelservice.enums.ProductionSource;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Production {
  private ProductionSource source;
  private int userId;
  private double produced;
  private Long time;
}

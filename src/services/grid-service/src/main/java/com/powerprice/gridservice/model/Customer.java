package com.powerprice.gridservice.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.powerprice.gridservice.enums.BuildingType;
import javax.persistence.*;
import org.springframework.hateoas.RepresentationModel;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class Customer extends RepresentationModel {
  @Id
  private Long id;

  private String code;

  private String firstName;

  private String lastName;

  private BuildingType buildingType;
  private String companyName;

  @ManyToOne(cascade = CascadeType.ALL)
  private Grid grid;

  public Customer(String companyName) {
    this.companyName = companyName;
  }

  public Customer(BuildingType buildingType) {
    this.buildingType = buildingType;
  }

  public Customer() {}

  public Long getId() {
    return id;
  }

  public String getCode() {
    return code;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public BuildingType getBuildingType() {
    return buildingType;
  }

  public String getCompanyName() {
    return companyName;
  }

  public Grid getGrid() {
    return grid;
  }

  @Override
  public String toString() {
    return (
      "Customer{" +
      "id=" +
      id +
      ", code=" +
      code +
      ", firstName=" +
      firstName +
      ", lastName=" +
      lastName +
      ", buildingType=" +
      buildingType +
      ", companyName='" +
      companyName +
      '\'' +
      ", grid=" +
      grid +
      '}'
    );
  }
}

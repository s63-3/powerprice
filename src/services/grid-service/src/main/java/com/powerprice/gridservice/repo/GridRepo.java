package com.powerprice.gridservice.repo;

import com.powerprice.gridservice.model.Grid;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface GridRepo
  extends JpaRepository<Grid, Long>, QueryByExampleExecutor<Grid> {
  @Query(
    value = "SELECT SUM(total_energy_production_k_wh) FROM grid",
    nativeQuery = true
  )
  int findCurrentEnergyProduction();

  @Query(
    value = "SELECT SUM(total_energy_usage_k_wh) FROM grid",
    nativeQuery = true
  )
  int findCurrentEnergyUsage();

  @Query(value = "SELECT COUNT(id) FROM customer", nativeQuery = true)
  int findTotalCustomers();
}

package com.powerprice.gridservice.model;

import com.powerprice.gridservice.enums.ProductionSource;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.minidev.json.annotate.JsonIgnore;
import org.springframework.hateoas.RepresentationModel;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Production extends RepresentationModel {
  @JsonIgnore
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private ProductionSource source;
  private int userId;
  private double produced;
  private Long time;

  public Production(ProductionSource productionSource, int userId, double produced, Long time){
    this.source = productionSource;
    this.userId = userId;
    this.produced = produced;
    this.time = time;
  }
}

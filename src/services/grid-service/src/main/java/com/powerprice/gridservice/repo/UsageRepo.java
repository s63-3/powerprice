package com.powerprice.gridservice.repo;

import com.powerprice.gridservice.model.Usage;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UsageRepo extends JpaRepository<Usage, Long> {
  List<Usage> findAllByUserId(Long userId);

  @Query(
          value = "SELECT SUM(used) FROM _usage u WHERE u.`time` > UNIX_TIMESTAMP() - 3600",
          nativeQuery = true
  )
  Long findUsageOfLastHour();

  @Query(value = "SELECT * FROM _usage us WHERE us.time > UNIX_TIMESTAMP() - 3600 AND user_id = ?1",  nativeQuery = true)
  List<Usage> findAllLastHourByUserId(Long userId);
}

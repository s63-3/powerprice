package com.powerprice.gridservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GridServiceApplication {

  public static void main(String[] args) {
    SpringApplication.run(GridServiceApplication.class, args);
  }
}

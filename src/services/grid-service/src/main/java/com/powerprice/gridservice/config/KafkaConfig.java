package com.powerprice.gridservice.config;

import com.powerprice.gridservice.deserializer.CustomerRegisteredDeserializer;
import com.powerprice.gridservice.deserializer.ProductionDeserializer;
import com.powerprice.gridservice.kafka.CustomerRegisteredConsumer;
import com.powerprice.gridservice.kafka.ProductionConsumer;
import com.powerprice.gridservice.model.Customer;
import com.powerprice.gridservice.model.Production;
import java.util.HashMap;
import java.util.Map;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;

@Configuration
@EnableKafka
public class KafkaConfig {
  @Value("${sp.bootstrapServersConfig}")
  private String bootstrapServersConfig;

  private Map<String, Object> getKafkaDefaultProps() {
    Map<String, Object> props = new HashMap<>();
    props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServersConfig);
    props.put(
      ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
      "org.apache.kafka.common.serialization.StringDeserializer"
    );
    props.put(ConsumerConfig.GROUP_ID_CONFIG, "grid-service");
    props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

    return props;
  }

  @Bean
  public ConsumerFactory<String, Production> productionConsumerFactory() {
    var props = getKafkaDefaultProps();
    props.put(
      ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
      ProductionDeserializer.class
    );

    return new DefaultKafkaConsumerFactory<>(props);
  }

  @Bean
  public ConsumerFactory<String, Customer> customerRegisteredConsumerFactory() {
    var props = getKafkaDefaultProps();
    props.put(
      ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
      CustomerRegisteredDeserializer.class
    );

    return new DefaultKafkaConsumerFactory<>(props);
  }

  @Bean(name = "productionContainerFactory")
  public ConcurrentKafkaListenerContainerFactory<String, Production> productionContainerFactory() {
    var factory = new ConcurrentKafkaListenerContainerFactory<String, Production>();
    factory.setConsumerFactory(productionConsumerFactory());
    return factory;
  }

  @Bean(name = "customerRegisteredContainerFactory")
  public ConcurrentKafkaListenerContainerFactory<String, Customer> customerRegisteredContainerFactory() {
    var factory = new ConcurrentKafkaListenerContainerFactory<String, Customer>();
    factory.setConsumerFactory(customerRegisteredConsumerFactory());
    return factory;
  }
}

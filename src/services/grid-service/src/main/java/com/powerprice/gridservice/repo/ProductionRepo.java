package com.powerprice.gridservice.repo;

import com.powerprice.gridservice.model.Production;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductionRepo extends JpaRepository<Production, Long> {

  @Query(value = "SELECT * FROM production pr WHERE pr.user_id=?1 ORDER BY pr.`time` ASC", nativeQuery = true)
  List<Production> findAllByUserId(Long userId);

  @Query(
    value = "SELECT SUM(produced) FROM production pr WHERE pr.`time` > UNIX_TIMESTAMP() - 3600",
    nativeQuery = true
  )
  Long findProductionOfLastHour();

  @Query(
    value = "SELECT * FROM production pr WHERE pr.time > UNIX_TIMESTAMP() - 3600 AND user_id = ?1",
    nativeQuery = true
  )
  List<Production> findCurrentProductionByUserId(Long userId);

}

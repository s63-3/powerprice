package com.powerprice.gridservice.model;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import org.springframework.hateoas.RepresentationModel;

@Entity
public class HourlyEnergyCosts extends RepresentationModel {
  @Id
  @GeneratedValue
  private Long id;

  private int pricePerMHw;
  private Date date;

  public HourlyEnergyCosts(int pricePerMHw, Date date) {
    this.pricePerMHw = pricePerMHw;
    this.date = date;
  }

  public HourlyEnergyCosts() {}

  public HourlyEnergyCosts(Long id) {
    this.id = id;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public int getPricePerMHw() {
    return pricePerMHw;
  }

  public void setPricePerMHw(int pricePerMHw) {
    this.pricePerMHw = pricePerMHw;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }
}

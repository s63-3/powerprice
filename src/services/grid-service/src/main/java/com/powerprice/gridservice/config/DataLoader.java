package com.powerprice.gridservice.config;

import com.powerprice.gridservice.enums.ProductionSource;
import com.powerprice.gridservice.model.Production;
import com.powerprice.gridservice.repo.ProductionRepo;
import lombok.SneakyThrows;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Component
public class DataLoader {
  private ProductionRepo productionRepo;

  private final Logger logger = LogManager.getLogger(DataLoader.class);

  public DataLoader(ProductionRepo productionRepo) {
    this.productionRepo = productionRepo;
  }

  @PostConstruct
  void loadData() {
    dataThread();
  }

  private List<Production> generateProductions(){
    List<Production> generatedProductions = new ArrayList<>();
    for (int a = 0; a < 720; a++){
      long unixTime = Instant.now().minus(Duration.ofHours(a)).getEpochSecond();
      for (int i = 0; i < 20; i++) {
        int customerId = i + 1;
        generatedProductions.add(new Production(ProductionSource.WINDMILL, customerId, 800, unixTime));
        generatedProductions.add(new Production(ProductionSource.WINDMILL, customerId, 1000, unixTime));
        generatedProductions.add(new Production(ProductionSource.WINDMILL, customerId, 700, unixTime));
        generatedProductions.add(new Production(ProductionSource.SOLAR_PANEL, customerId, 1200, unixTime));
        generatedProductions.add(new Production(ProductionSource.SOLAR_PANEL, customerId, 950, unixTime));
        generatedProductions.add(new Production(ProductionSource.SOLAR_PANEL, customerId, 900, unixTime));
      }
    }
    return generatedProductions;
  }

  private void dataThread(){
    Runnable generateDataThread = () -> {
      while (true) {
        productionRepo.deleteAll();
        List<Production> productions = generateProductions();
        productionRepo.saveAll(productions);
        break;
      }
    };
    new Thread(generateDataThread).start();
  }
}

package com.powerprice.gridservice.enums;

public enum BuildingType {
  RIJTJESWONING,
  VRIJSTAANDEWONING,
  APPARTEMENT,
  BEDRIJFSGEBOUW,
}

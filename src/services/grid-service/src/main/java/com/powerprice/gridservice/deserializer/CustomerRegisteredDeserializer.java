package com.powerprice.gridservice.deserializer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.powerprice.gridservice.model.Customer;
import org.apache.kafka.common.serialization.Deserializer;

public class CustomerRegisteredDeserializer implements Deserializer<Customer> {

  @Override
  public Customer deserialize(String arg0, byte[] bytes) {
    ObjectMapper mapper = new ObjectMapper();
    Customer customer = null;
    try {
      customer = mapper.readValue(bytes, Customer.class);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return customer;
  }
}

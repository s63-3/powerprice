package com.powerprice.gridservice.kafka;

import com.powerprice.gridservice.model.Customer;
import com.powerprice.gridservice.repo.CustomerRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class CustomerRegisteredConsumer {
  private final Logger logger = LoggerFactory.getLogger(
    CustomerRegisteredConsumer.class
  );
  private CustomerRepo customerRepo;

  @Autowired
  public CustomerRegisteredConsumer(CustomerRepo customerRepo) {
    this.customerRepo = customerRepo;
  }

  @KafkaListener(
    topics = "customerRegistered",
    containerFactory = "customerRegisteredContainerFactory"
  )
  public void consume(Customer customer) {
    logger.info(String.format("Consumed message -> %s", customer));
    customerRepo.save(customer);
  }
}

package com.powerprice.gridservice.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.*;
import org.springframework.hateoas.RepresentationModel;

@Entity
public class Grid extends RepresentationModel {
  @Id
  @GeneratedValue
  private Long id;

  private int totalEnergyUsage_kWh;
  private int totalEnergyProduction_kWh;
  private int totalCustomers;
  private BigDecimal price;

  @OneToMany(cascade = CascadeType.ALL)
  private Collection<Customer> customers = new ArrayList<>();

  public Grid(int EnergyProduction) {
    this.totalEnergyProduction_kWh = EnergyProduction;
  }

  public Grid() {}

  public int getTotalEnergyUsage_kWh() {
    return totalEnergyUsage_kWh;
  }

  public void setTotalEnergyUsage_kWh(int totalEnergyUsage) {
    this.totalEnergyUsage_kWh = totalEnergyUsage;
  }

  public int getTotalEnergyProduction_kWh() {
    return totalEnergyProduction_kWh;
  }

  public void setTotalEnergyProduction_kWh(int totalEnergyProduction) {
    this.totalEnergyProduction_kWh = totalEnergyProduction;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  public int getTotalCustomers() {
    return totalCustomers;
  }

  public void setTotalCustomers(int totalCustomers) {
    this.totalCustomers = totalCustomers;
  }
}

package com.powerprice.gridservice.kafka;

import com.powerprice.gridservice.enums.ProductionSource;
import com.powerprice.gridservice.model.Production;
import com.powerprice.gridservice.model.Usage;
import com.powerprice.gridservice.repo.ProductionRepo;
import com.powerprice.gridservice.repo.UsageRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class ProductionConsumer {
  private final Logger logger = LoggerFactory.getLogger(
    ProductionConsumer.class
  );
  private static final String ConsumedMessage =
    "#### -> Consumed message -> %s";

  private ProductionRepo productionRepo;

  private UsageRepo usageRepo;

  @Autowired
  public ProductionConsumer(
    ProductionRepo productionRepo,
    UsageRepo usageRepo
  ) {
    this.productionRepo = productionRepo;
    this.usageRepo = usageRepo;
  }

  @KafkaListener(
    topics = "production",
    containerFactory = "productionContainerFactory"
  )
  public void consume(Production production) throws Exception {
    // Consume message from production topic.
    logger.info(String.format(ConsumedMessage, production));
    this.productionRepo.save(production);
    // Dirty fix to only save the usage once when a production is consumed
    if (production.getSource() == ProductionSource.SOLAR_PANEL) {
      this.usageRepo.save(
              new Usage(
                      production.getId(),
                      new Long(production.getUserId()),
                      1000,
                      production.getTime()
              )
      );
    }
  }
}

package com.powerprice.gridservice.deserializer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.powerprice.gridservice.model.Production;
import org.apache.kafka.common.serialization.Deserializer;

public class ProductionDeserializer implements Deserializer<Production> {

  @Override
  public Production deserialize(String arg0, byte[] bytes) {
    ObjectMapper mapper = new ObjectMapper();
    Production production = null;
    try {
      production = mapper.readValue(bytes, Production.class);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return production;
  }
}

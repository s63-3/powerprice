package com.powerprice.gridservice.service;

import com.powerprice.gridservice.model.Customer;
import com.powerprice.gridservice.model.Production;
import com.powerprice.gridservice.model.Usage;
import com.powerprice.gridservice.repo.CustomerRepo;
import com.powerprice.gridservice.repo.ProductionRepo;
import com.powerprice.gridservice.repo.UsageRepo;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

@Service
public class GridRestService {
  @Value("${service.userUrl}")
  private String userServiceUrl;

  private ProductionRepo productionRepo;

  private UsageRepo usageRepo;

  private CustomerRepo customerRepo;

  @Value("${service.solarpanelUrl}")
  private String solarPanelServiceUrl;

  @Value("${service.windmillUrl}")
  private String windmillServiceUrl;

  private WebClient solarPanelClient;

  private WebClient windMillClient;

  private WebClient userClient;

  final Double energyPrice = 0.22;
  final Double sellEnergyPrice = 0.22;

  @Autowired
  public GridRestService(
    ProductionRepo productionRepo,
    UsageRepo usageRepo,
    CustomerRepo customerRepo
  ) {
    this.productionRepo = productionRepo;
    this.usageRepo = usageRepo;
    this.customerRepo = customerRepo;
  }

  private final Logger logger = Logger.getLogger(
    GridRestService.class.getName()
  );

  @PostConstruct
  public void init() {
    solarPanelClient = WebClient.create(solarPanelServiceUrl);
    windMillClient = WebClient.create(windmillServiceUrl);
    userClient = WebClient.create(userServiceUrl);
  }

  public Integer getCustomerProduction(Long userId) {
    List<Production> productionList = productionRepo.findCurrentProductionByUserId(
      userId
    );
    int productionProduced = 0;
    for (Production production : productionList) {
      productionProduced += production.getProduced();
    }
    return productionProduced;
  }

  public Production[] getCustomerProductionObjects(Long userId) {
    List<Production> productionList = productionRepo.findAllByUserId(userId);
    Production[] productionArray = new Production[productionList.size()];
    productionList.toArray(productionArray);
    return productionArray;
  }

  /**
   * Get usage of a houshold or company building based on customer.BuildingType
   * from user-service *
   */
  public Integer getCustomerUsage(Long userId) {
    List<Usage> usageList = usageRepo.findAllLastHourByUserId(userId);
    double usage = 0;
    for(Usage usageObject : usageList) {
      usage += usageObject.getUsed();
    }
    return (int)usage;
  }

  public Usage[] getCustomerUsageObjects(Long user_id) {
    List<Usage> usageList = usageRepo.findAllByUserId(user_id);
    Usage[] usageArray = new Usage[usageList.size()];
    usageList.toArray(usageArray);
    return usageArray;
  }

  // For whole grid get total usage, again with mocked values
  public Long getGridUsage() {
    return usageRepo.findUsageOfLastHour();
  }

  public Integer getHourlyGridUsage() {
    return 0;
  }

  public Long getGridProduction() {
    logger.log(Level.INFO, "get grid production");
    return productionRepo.findProductionOfLastHour();
  }

  public Double calculateProductionPrice(Long userId) {
    return getCustomerProduction(userId) * sellEnergyPrice;
  }

  public Double calculateUsagePrice(Long userId) {
    return getCustomerUsage(userId) * energyPrice;
  }

  public void saveProduction(Production production) {
    productionRepo.save(production);
  }
}

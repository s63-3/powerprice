package com.powerprice.gridservice.cucumber;

import static org.junit.jupiter.api.Assertions.*;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

public class GetCustomerEnergyBillStepDefs
  extends GetCustomerEnergyBillIntegrationTest {
  private final Logger logger = LogManager.getLogger(
    GetCustomerEnergyBillStepDefs.class
  );

  private JSONObject price;

  // TODO: integration tests with other microservices with or without mocking
  @When("customer requests price")
  public void customer_requests_price() {
    //    Long customerId = 1L;
    //    price = getPrice(customerId).block();
  }

  @Then("customer gets calculated energyprice of the last hour")
  public void customer_gets_calculated_energyprice_of_the_last_hour() {
    //      if (generators.size() == 0) {
    //        logger.warn("Warning there are no generators found. Are there no users in the
    // database?");
    //        assertTrue(generators.size() == 0);
    //      } else if (generators.size() >= 1) {
    //        assertTrue(generators.size() > 1, "Should at least have 1 generator");
    //
    //        Generator generator = generators.get(0);
    //
    //        assertAll(
    //            "generator",
    //            () -> assertTrue(generator.getId() != null, "Should contain an id for generator"),
    //            () ->
    //                assertTrue(
    //                    generator.getEnergyUsage().iterator().next().getAmount() > 0,
    //                    "Should have an energy usage"),
    //            () ->
    //                assertNotNull(
    //                    generator.getCustomer().getId(), "generator should have a customer id"));
    //      }
  }
}

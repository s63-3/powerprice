package com.powerprice.windmillservice;

import com.powerprice.windmillservice.enums.WindmillType;
import com.powerprice.windmillservice.model.Windmill;
import com.powerprice.windmillservice.repo.WindmillRepo;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class WindmillRepositoryTest {
  @Autowired
  private TestEntityManager testEntityManager;

  @Autowired
  private WindmillRepo windmillRepo;

  @Test
  public void insertWindmillsTest() {
    //given
    List<Windmill> windmillList = new ArrayList<Windmill>() {

      {
        add(new Windmill(3, WindmillType.TURBINE, 5));
        add(new Windmill(3, WindmillType.DARRIEUS, 7));
        add(new Windmill(3, WindmillType.MOD_2, 13));
        add(new Windmill(3, WindmillType.GROWIAN, 4));
        add(new Windmill(3, WindmillType.SAVONIUS, 20));
      }
    };

    //when
    windmillRepo.saveAll(windmillList);

    //then
    List<Windmill> foundWindmillList = windmillRepo.findAll();
    Assert.assertEquals(windmillList, foundWindmillList);
  }
}

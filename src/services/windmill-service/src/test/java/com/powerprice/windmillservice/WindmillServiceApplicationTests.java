package com.powerprice.windmillservice;

import static org.assertj.core.api.Assertions.assertThat;

import com.powerprice.windmillservice.controller.WindmillController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class WindmillServiceApplicationTests {
  @Autowired
  private WindmillController windmillController;

  @Test
  public void contextLoads() throws Exception {
    assertThat(windmillController).isNotNull();
  }
}

package com.powerprice.windmillservice.enums;

public enum WindmillType {
  TURBINE(500, 900),
  MOD_2(500, 1200),
  SAVONIUS(700, 1000),
  DARRIEUS(200, 1300),
  GROWIAN(400, 1000);

  private final int minHourCapacity;
  private final int maxHourCapacity;

  private WindmillType(int minHourCapacity, int maxHourCapacity) {
    this.minHourCapacity = minHourCapacity;
    this.maxHourCapacity = maxHourCapacity;
  }

  public int getMaxHourCapacity() {
    return maxHourCapacity;
  }

  public int getMinHourCapacity() {
    return minHourCapacity;
  }
}

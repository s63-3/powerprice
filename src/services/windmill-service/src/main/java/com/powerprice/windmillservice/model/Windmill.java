package com.powerprice.windmillservice.model;

import com.powerprice.windmillservice.enums.WindmillType;
import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.minidev.json.annotate.JsonIgnore;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Windmill {
  @JsonIgnore
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private int customerId;
  private WindmillType windmillType;
  private int amount;

  public Windmill(int customerId, WindmillType windmillType, int amount) {
    this.customerId = customerId;
    this.windmillType = windmillType;
    this.amount = amount;
  }
}

package com.powerprice.windmillservice.service;

import com.powerprice.windmillservice.enums.ProductionSource;
import com.powerprice.windmillservice.kafka.ProductionProducer;
import com.powerprice.windmillservice.model.CurrentWeather;
import com.powerprice.windmillservice.model.Production;
import com.powerprice.windmillservice.model.WeatherForecast;
import com.powerprice.windmillservice.model.Windmill;
import com.powerprice.windmillservice.repo.WindmillRepo;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WindmillService {
  private ProductionProducer productionProducer;
  private WindmillRepo windmillRepo;

  @Autowired
  public WindmillService(
    WindmillRepo windmillRepo,
    ProductionProducer productionProducer
  ) {
    this.windmillRepo = windmillRepo;
    this.productionProducer = productionProducer;
  }

  public void addWindmills(List<Windmill> windmills) {
    windmillRepo.saveAll(windmills);
  }

  private double getPercentageBasedOnPressure(double pressure) {
    double productionPercentPressure = 100;
    if (pressure > 1013.2 && pressure < 1084.8) {
      productionPercentPressure = 100 - ((1084.4 - pressure) / 2);
    } else if (pressure > 1084.8) {
      productionPercentPressure = 0;
    }
    return productionPercentPressure;
  }

  private double getPercentageBasedOnWindSpeed(double windSpeed) {
    double productionPercentWindSpeed = 0;
    if (windSpeed > 9) {
      productionPercentWindSpeed = 80;
    } else if (windSpeed > 20) {
      productionPercentWindSpeed = 0;
    } else if (windSpeed >= 7 && windSpeed <= 9) {
      productionPercentWindSpeed = 100;
    } else if (windSpeed < 7) {
      productionPercentWindSpeed = (windSpeed * 14);
    }
    return productionPercentWindSpeed;
  }

  public void getWindmillProduction(
    double windSpeed,
    double pressure,
    Long time
  ) {
    //Get all windmills of the grid.
    List<Windmill> windmillList = windmillRepo.findAll();

    //Get production percentage based on pressure and wind speed.
    double windSpeedPercentage = getPercentageBasedOnWindSpeed(windSpeed);
    double pressurePercentage = getPercentageBasedOnPressure(pressure);
    double productionPercentage =
      ((windSpeedPercentage + pressurePercentage) / 2) / 100;

    //Get production for each windmill
    for (Windmill windmill : windmillList) {
      double totalWindmillProduction =
        windmill.getWindmillType().getMaxHourCapacity() * productionPercentage;

      //Production for each 4 minutes
      double productionPer4Minutes = totalWindmillProduction / 15;

      Production production = new Production(
        ProductionSource.WINDMILL,
        windmill.getCustomerId(),
        productionPer4Minutes,
        time
      );

      //Produce production to production topic.
      productionProducer.sendProduction(production);
    }
  }

  public double getWeatherSimulationProduction(
    double windSpeed,
    double pressure
  ) {
    //Get all windmills of the grid.
    List<Windmill> windmillList = windmillRepo.findAll();

    //Get production percentage based on pressure and wind speed.
    double windSpeedPercentage = getPercentageBasedOnWindSpeed(windSpeed);
    double pressurePercentage = getPercentageBasedOnPressure(pressure);
    double productionPercentage =
      ((windSpeedPercentage + pressurePercentage) / 2) / 100;

    double weatherSimulationProduction = 0;

    //Get production for each windmill
    for (Windmill windmill : windmillList) {
      double totalWindmillProduction =
        windmill.getWindmillType().getMaxHourCapacity() * productionPercentage;
      weatherSimulationProduction += totalWindmillProduction;
    }
    return weatherSimulationProduction;
  }
}

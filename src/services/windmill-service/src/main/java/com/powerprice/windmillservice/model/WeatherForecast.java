package com.powerprice.windmillservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WeatherForecast {
  double windSpeed;
  double pressure;
  int uvIndex;
  double temperature;
  double cloudCover;
}

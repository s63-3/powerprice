package com.powerprice.windmillservice.repo;

import com.powerprice.windmillservice.model.Windmill;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WindmillRepo extends JpaRepository<Windmill, Long> {
  List<Windmill> findAllByCustomerId(int customerId);
}

package com.powerprice.windmillservice.kafka;

import com.powerprice.windmillservice.model.CurrentWeather;
import com.powerprice.windmillservice.service.WindmillService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class WeatherConsumer {
  private final Logger logger = LoggerFactory.getLogger(WeatherConsumer.class);
  private WindmillService windmillService;
  private static final String ConsumedMessage =
    "#### -> Consumed message -> %s";

  @Autowired
  public WeatherConsumer(WindmillService windmillService) {
    this.windmillService = windmillService;
  }

  @KafkaListener(topics = "weather", groupId = "WeatherConsumerGroup1")
  public void consume(CurrentWeather currentWeather) throws Exception {
    //Consume message from weather topic.
    logger.info(String.format(ConsumedMessage, currentWeather));

    //Calculate the production.
    windmillService.getWindmillProduction(
      currentWeather.getCurrently().getWindSpeed(),
      currentWeather.getCurrently().getPressure(),
      currentWeather.getCurrently().getTime()
    );
  }
}

package com.powerprice.windmillservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Currently {
  Long time;
  String summary;
  double temperature;
  double humidity;
  double pressure;
  double windSpeed;
  double cloudCover;
  int uvIndex;
  double visibility;
}

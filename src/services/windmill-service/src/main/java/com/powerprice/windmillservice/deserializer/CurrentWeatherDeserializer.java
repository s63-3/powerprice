package com.powerprice.windmillservice.deserializer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.powerprice.windmillservice.model.CurrentWeather;
import org.apache.kafka.common.serialization.Deserializer;

public class CurrentWeatherDeserializer
  implements Deserializer<CurrentWeather> {

  @Override
  public CurrentWeather deserialize(String arg0, byte[] bytes) {
    ObjectMapper mapper = new ObjectMapper();
    CurrentWeather currentWeather = null;
    try {
      currentWeather = mapper.readValue(bytes, CurrentWeather.class);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return currentWeather;
  }
}

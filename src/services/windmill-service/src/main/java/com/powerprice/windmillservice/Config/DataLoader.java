package com.powerprice.windmillservice.Config;

import com.powerprice.windmillservice.enums.WindmillType;
import com.powerprice.windmillservice.model.Windmill;
import com.powerprice.windmillservice.repo.WindmillRepo;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Component
public class DataLoader {
  private final WindmillRepo windmillRepo;

  public DataLoader(WindmillRepo windmillRepo) {
    this.windmillRepo = windmillRepo;
  }

  @PostConstruct
  void loadData() {
    dataThread();
  }

  private List<Windmill> generateWindmills() {
    List<Windmill> generatedWindmills = new ArrayList<>();

    for (int i = 0; i < 20; i++) {
      int customerId = i + 1;
      generatedWindmills.add(new Windmill(customerId, WindmillType.GROWIAN, 3));
      generatedWindmills.add(new Windmill(customerId, WindmillType.MOD_2, 4));
      generatedWindmills.add(new Windmill(customerId, WindmillType.TURBINE, 2));
    }
    return generatedWindmills;
  }

  private void dataThread(){
    Runnable generateDataThread = () -> {
      while (true) {
        windmillRepo.deleteAll();
        List<Windmill> windmillList = generateWindmills();
        windmillRepo.saveAll(windmillList);
        break;
      }
    };
    new Thread(generateDataThread).start();
  }
}

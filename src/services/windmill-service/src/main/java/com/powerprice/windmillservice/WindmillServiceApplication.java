package com.powerprice.windmillservice;

import com.powerprice.windmillservice.kafka.ProductionProducer;
import com.powerprice.windmillservice.model.CurrentWeather;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EntityScan
@EnableJpaRepositories
@SpringBootApplication
public class WindmillServiceApplication {

  public static void main(String[] args) {
    SpringApplication.run(WindmillServiceApplication.class, args);
  }
}

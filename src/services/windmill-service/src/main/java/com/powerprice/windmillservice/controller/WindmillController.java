package com.powerprice.windmillservice.controller;

import com.powerprice.windmillservice.model.CurrentWeather;
import com.powerprice.windmillservice.model.WeatherForecast;
import com.powerprice.windmillservice.model.Windmill;
import com.powerprice.windmillservice.service.WindmillService;
import java.util.List;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.PostMapping;

@RestController
@RequestMapping("/windmills")
public class WindmillController {
  private final Logger logger = LogManager.getLogger(WindmillController.class);
  private WindmillService windmillService;

  @Autowired
  public WindmillController(WindmillService windmillService) {
    this.windmillService = windmillService;
  }

  @PostMapping
  public ResponseEntity<String> addWindmills(
    @RequestBody List<Windmill> windmills
  ) {
    windmillService.addWindmills(windmills);
    return new ResponseEntity<>("Successful created.", HttpStatus.CREATED);
  }

  @PostMapping(value = "/weather-forecast/production")
  public ResponseEntity<Double> getProductionBasedOnWeather(
    @RequestBody WeatherForecast weatherForecast
  ) {
    logger.log(Level.INFO, "GetWindmillProductionOfWeather");
    return ResponseEntity.ok(
      windmillService.getWeatherSimulationProduction(
        weatherForecast.getWindSpeed(),
        weatherForecast.getPressure()
      )
    );
  }
}

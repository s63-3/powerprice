package com.powerprice.windmillservice.kafka;

import com.powerprice.windmillservice.model.Production;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class ProductionProducer {
  private static final Logger logger = LoggerFactory.getLogger(
    ProductionProducer.class
  );
  private static final String Topic = "production";
  private static final String ProducedMessage =
    "#### -> Producing message -> %s";
  private KafkaTemplate<String, Production> kafkaTemplate;

  @Autowired
  public ProductionProducer(KafkaTemplate<String, Production> kafkaTemplate) {
    this.kafkaTemplate = kafkaTemplate;
  }

  public void sendProduction(Production production) {
    logger.info(String.format(ProducedMessage, production));
    this.kafkaTemplate.send(Topic, production);
  }
}

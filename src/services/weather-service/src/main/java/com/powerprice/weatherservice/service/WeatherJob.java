package com.powerprice.weatherservice.service;

import com.powerprice.weatherservice.kafka.WeatherProducer;
import com.powerprice.weatherservice.model.CurrentWeather;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class WeatherJob implements Job {
  @Autowired
  WeatherProducer weatherProducer;

  @Autowired
  WeatherRestService weatherRestService;

  public void execute(JobExecutionContext context) {
    CurrentWeather currentWeather = weatherRestService.getCurrentWeatherFromApi();
    sendCurrentWeather(currentWeather);
  }

  private void sendCurrentWeather(CurrentWeather currentWeather) {
    weatherProducer.sendMessage(currentWeather);
  }
}

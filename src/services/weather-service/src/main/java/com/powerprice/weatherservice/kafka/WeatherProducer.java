package com.powerprice.weatherservice.kafka;

import com.powerprice.weatherservice.model.CurrentWeather;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class WeatherProducer {
  private static final Logger logger = LoggerFactory.getLogger(
    WeatherProducer.class
  );
  private static final String TOPIC = "weather";

  private KafkaTemplate<String, CurrentWeather> kafkaTemplate;

  public WeatherProducer(KafkaTemplate<String, CurrentWeather> kafkaTemplate) {
    this.kafkaTemplate = kafkaTemplate;
  }

  public void sendMessage(CurrentWeather currentWeather) {
    logger.info(String.format("Producing message -> %s", currentWeather));
    this.kafkaTemplate.send(TOPIC, currentWeather);
  }
}

package com.powerprice.weatherservice.config;

import com.powerprice.weatherservice.service.WeatherJob;
import java.util.Calendar;
import java.util.Properties;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Trigger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.*;

@Slf4j
@Configuration
public class QuartzConfig {
  private static final String CRON_EVERY_FOUR_MINUTES = "0 0/4 * ? * * *";

  private ApplicationContext applicationContext;

  public QuartzConfig(ApplicationContext applicationContext) {
    this.applicationContext = applicationContext;
  }

  @Bean
  public SpringBeanJobFactory springBeanJobFactory() {
    AutowiringSpringBeanJobFactory jobFactory = new AutowiringSpringBeanJobFactory();
    jobFactory.setApplicationContext(applicationContext);
    return jobFactory;
  }

  @Bean
  public SchedulerFactoryBean scheduler(Trigger... triggers) {
    SchedulerFactoryBean schedulerFactory = new SchedulerFactoryBean();
    Properties properties = new Properties();
    properties.setProperty(
      "org.quartz.scheduler.instanceName",
      "MyInstanceName"
    );
    properties.setProperty("org.quartz.scheduler.instanceId", "Instance1");
    schedulerFactory.setOverwriteExistingJobs(true);
    schedulerFactory.setAutoStartup(true);
    schedulerFactory.setQuartzProperties(properties);
    schedulerFactory.setJobFactory(springBeanJobFactory());
    schedulerFactory.setWaitForJobsToCompleteOnShutdown(true);
    if (ArrayUtils.isNotEmpty(triggers)) {
      schedulerFactory.setTriggers(triggers);
    }
    return schedulerFactory;
  }

  static CronTriggerFactoryBean createCronTrigger(
    JobDetail jobDetail,
    String cronExpression,
    String triggerName
  ) {
    log.debug(
      "createCronTrigger(jobDetail={}, cronExpression={}, triggerName={})",
      jobDetail.toString(),
      cronExpression,
      triggerName
    );
    // To fix an issue with time-based cron jobs
    Calendar calendar = Calendar.getInstance();
    calendar.set(Calendar.SECOND, 0);
    calendar.set(Calendar.MILLISECOND, 0);
    CronTriggerFactoryBean factoryBean = new CronTriggerFactoryBean();
    factoryBean.setJobDetail(jobDetail);
    factoryBean.setCronExpression(cronExpression);
    factoryBean.setStartTime(calendar.getTime());
    factoryBean.setStartDelay(0L);
    factoryBean.setName(triggerName);
    factoryBean.setMisfireInstruction(
      CronTrigger.MISFIRE_INSTRUCTION_DO_NOTHING
    );
    return factoryBean;
  }

  static JobDetailFactoryBean createJobDetail(Class jobClass, String jobName) {
    log.debug(
      "createJobDetail(jobClass={}, jobName={})",
      jobClass.getName(),
      jobName
    );
    JobDetailFactoryBean factoryBean = new JobDetailFactoryBean();
    factoryBean.setName(jobName);
    factoryBean.setJobClass(jobClass);
    factoryBean.setDurability(true);
    return factoryBean;
  }

  @Bean(name = "weather")
  public JobDetailFactoryBean jobWeather() {
    return QuartzConfig.createJobDetail(WeatherJob.class, "Weather Job");
  }

  @Bean(name = "weatherTrigger")
  public CronTriggerFactoryBean triggerWeather(
    @Qualifier("weather") JobDetail jobDetail
  ) {
    return QuartzConfig.createCronTrigger(
      jobDetail,
      CRON_EVERY_FOUR_MINUTES,
      "Weather Trigger"
    );
  }
}

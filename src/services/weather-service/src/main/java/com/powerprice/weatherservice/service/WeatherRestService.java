package com.powerprice.weatherservice.service;

import com.powerprice.weatherservice.model.CurrentWeather;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
public class WeatherRestService {
  @Value("${weather.key}")
  private String key;

  @Value("${weather.latlon}")
  private String latLon;

  @Value("${weather.baseUrl}")
  private String baseUrl;

  WebClient weatherClient = WebClient.create(baseUrl);

  public CurrentWeather getCurrentWeatherFromApi() {
    Mono<CurrentWeather> weather = weatherClient
      .get()
      .uri(
        baseUrl +
        "/" +
        key +
        "/" +
        latLon +
        "?exclude=hourly&exclude=daily&exclude=flags&units=si&lang=nl"
      )
      .retrieve()
      .bodyToMono(CurrentWeather.class);
    return weather.block();
  }
}

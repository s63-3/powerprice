package com.powerprice.gridsimulationservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.powerprice.gridsimulationservice.controller.GridSimulationController;
import com.powerprice.gridsimulationservice.model.WeatherForecast;
import com.powerprice.gridsimulationservice.model.WeatherSimulation;
import com.powerprice.gridsimulationservice.service.GridSimulationService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.Charset;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(GridSimulationController.class)
public class GridControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private GridSimulationService service;

    private static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));
    @Test
    public void getProductionBasedOnWeather() throws Exception{
        WeatherForecast weatherForecast = new WeatherForecast();
        WeatherSimulation weatherSimulation = new WeatherSimulation(100.123, 200.321);
        when(service.getSolarPanelProductionBasedOnWeather(weatherForecast)).thenReturn(100.123);
        when(service.getWindmillProductionBasedOnWeather(weatherForecast)).thenReturn(200.321);
        when(service.getProductionBasedOnWeather(weatherForecast)).thenReturn(weatherSimulation);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String weatherJson =  ow.writeValueAsString(weatherForecast);
        this.mockMvc.perform(post("/simulation/weather").contentType(APPLICATION_JSON_UTF8).content(weatherJson)).andDo(print()).andExpect(status().isOk());

    }
}

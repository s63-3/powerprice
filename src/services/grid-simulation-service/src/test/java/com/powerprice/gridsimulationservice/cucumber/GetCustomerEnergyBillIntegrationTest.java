package com.powerprice.gridsimulationservice.cucumber;

import org.json.JSONObject;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@ContextConfiguration
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class GetCustomerEnergyBillIntegrationTest {
  private WebClient webClient;

  @LocalServerPort int port;

  Mono<JSONObject> getPrice(Long customerId) {
    ExchangeStrategies exchangeStrategies =
        ExchangeStrategies.builder()
            .codecs(configurer -> configurer.defaultCodecs().maxInMemorySize(1024 * 10000))
            .build();

    webClient =
        WebClient.builder()
            .exchangeStrategies(exchangeStrategies)
            .baseUrl("http://localhost:" + port)
            .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .build();

    return webClient
        .get()
        .uri("/grid/price/last-hour-netto?customerId=" + customerId)
        .retrieve()
        .bodyToMono(JSONObject.class);
  }
}

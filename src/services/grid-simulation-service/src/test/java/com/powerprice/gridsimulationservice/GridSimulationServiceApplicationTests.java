package com.powerprice.gridsimulationservice;

import com.powerprice.gridsimulationservice.controller.GridSimulationController;
import com.powerprice.gridsimulationservice.service.GridSimulationService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class GridSimulationServiceApplicationTests {

    @Autowired
    GridSimulationController controller;

    @Test
    public void contextLoads() {
        assertThat(controller).isNotNull();

    }
}

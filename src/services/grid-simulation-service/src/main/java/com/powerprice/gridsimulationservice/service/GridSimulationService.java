package com.powerprice.gridsimulationservice.service;

import com.powerprice.gridsimulationservice.model.WeatherForecast;
import com.powerprice.gridsimulationservice.model.WeatherSimulation;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
public class GridSimulationService {
  private WebClient solarPanelWebClient;
  private WebClient windmillWebClient;
  private String solarPanelServiceUrl;
  private String windmillServiceUrl;

  @Autowired
  public GridSimulationService(
      @Value("${service.solarpanelUrl}") String solarPanelServiceUrl,
      @Value("${service.windmillUrl}") String windmillServiceUrl) {
    this.solarPanelServiceUrl = solarPanelServiceUrl;
    this.windmillServiceUrl = windmillServiceUrl;
  }

  @PostConstruct
  private void init() {
    this.solarPanelWebClient =
        WebClient.builder()
            .baseUrl(solarPanelServiceUrl)
            .defaultHeader(HttpHeaders.CONTENT_TYPE, "application/json")
            .build();
    this.windmillWebClient =
        WebClient.builder()
            .baseUrl(windmillServiceUrl)
            .defaultHeader(HttpHeaders.CONTENT_TYPE, "application/json")
            .build();
  }

  public WeatherSimulation getProductionBasedOnWeather(WeatherForecast weatherForecast) {
    WeatherSimulation weatherSimulation = new WeatherSimulation();
    weatherSimulation.setSolarPanelProduction(
        getSolarPanelProductionBasedOnWeather(weatherForecast));
    weatherSimulation.setWindmillProduction(getWindmillProductionBasedOnWeather(weatherForecast));
    return weatherSimulation;
  }

  public Double getSolarPanelProductionBasedOnWeather(WeatherForecast weatherForecast) {
    return solarPanelWebClient
        .post()
        .uri("/solar-panels/weather-forecast/production")
        .body(Mono.just(weatherForecast), WeatherForecast.class)
        .retrieve()
        .bodyToMono(Double.class)
        .block();
  }

  public Double getWindmillProductionBasedOnWeather(WeatherForecast weatherForecast) {
    return windmillWebClient
        .post()
        .uri("/windmills/weather-forecast/production")
        .body(Mono.just(weatherForecast), WeatherForecast.class)
        .retrieve()
        .bodyToMono(Double.class)
        .block();
  }
}

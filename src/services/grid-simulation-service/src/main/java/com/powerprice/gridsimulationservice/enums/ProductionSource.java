package com.powerprice.gridsimulationservice.enums;

public enum ProductionSource {
  SOLAR_PANEL,
  WINDMILL,
}

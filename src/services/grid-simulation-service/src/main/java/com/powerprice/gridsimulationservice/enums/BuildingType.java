package com.powerprice.gridsimulationservice.enums;

public enum BuildingType {
  RIJTJESWONING,
  VRIJSTAANDEWONING,
  APPARTEMENT,
  BEDRIJFSGEBOUW,
}

package com.powerprice.gridsimulationservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WeatherSimulation {
  private double solarPanelProduction;
  private double windmillProduction;
}

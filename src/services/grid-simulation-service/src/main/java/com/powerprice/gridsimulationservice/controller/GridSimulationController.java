package com.powerprice.gridsimulationservice.controller;

import com.powerprice.gridsimulationservice.model.WeatherForecast;
import com.powerprice.gridsimulationservice.model.WeatherSimulation;
import com.powerprice.gridsimulationservice.service.GridSimulationService;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.PostMapping;

@RestController
@RequestMapping("/simulation")
public class GridSimulationController {
  private final Logger logger = LogManager.getLogger(GridSimulationController.class);
  private GridSimulationService gridSimulationService;

  @Autowired
  public GridSimulationController(GridSimulationService gridSimulationService) {
    this.gridSimulationService = gridSimulationService;
  }

  @PostMapping("/weather")
  public ResponseEntity<WeatherSimulation> GetProductionBasedOnWeather(
      @RequestBody WeatherForecast weatherForecast) {
    logger.log(Level.INFO, "GetProductionOfWeather");
    return ResponseEntity.ok(gridSimulationService.getProductionBasedOnWeather(weatherForecast));
  }
}

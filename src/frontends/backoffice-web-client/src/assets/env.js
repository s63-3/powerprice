(function(window) {
  window.env = window.env || {};

  // Environment variables
  window.env.keycloakUrl = 'http://KEYCLOAK-IP:8088/auth',
  window.keycloakRealm = 'your-realm-name',
  window.keycloakClientId = 'your-client-id',
  window.gatewayUrl = 'http://gateway-url'
})(this);
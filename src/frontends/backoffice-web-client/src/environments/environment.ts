import { KeycloakConfig } from 'keycloak-angular';

const keycloakConfig: KeycloakConfig = {
  url: 'http://localhost:8080/auth/',
  realm: 'powerprice',
  clientId: 'webapp',
};
export const environment = {
  production: false,
  keycloakConfig,
  gatewayUrl: 'http://localhost:7700'
};


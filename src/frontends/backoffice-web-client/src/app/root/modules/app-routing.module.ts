import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from 'src/app/features/login/login.component';
import { DashboardComponent } from 'src/app/features/dashboard/dashboard.component';
import { AuthGuard } from 'src/app/core/guards/auth.guard';
import { AddCustomerComponent } from 'src/app/features/add-customer/add-customer.component';
import { WindmillComponent } from 'src/app/features/windmill/windmill.component';
import { SolarPanelComponent } from 'src/app/features/solar-panel/solar-panel.component';
import { SimulationComponent } from 'src/app/features/simulation/simulation.component';
import { RegionDashboardComponent } from 'src/app/features/region-dashboard/region-dashboard.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
  { path: 'add-customer', component: AddCustomerComponent, canActivate: [AuthGuard] },
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
  { path: 'add-customer', component: AddCustomerComponent, canActivate: [AuthGuard] },
  { path: 'add-windmill/:id', component: WindmillComponent, canActivate: [AuthGuard] },
  { path: 'add-solar-panel/:id', component: SolarPanelComponent, canActivate: [AuthGuard] },
  { path: 'simulation', component: SimulationComponent, canActivate: [AuthGuard] },
  { path: 'region-dashboard', component: RegionDashboardComponent, canActivate: [AuthGuard] },
  { path: '', redirectTo: '/login', pathMatch: 'full' }
];


@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
})
export class AppRoutingModule { }

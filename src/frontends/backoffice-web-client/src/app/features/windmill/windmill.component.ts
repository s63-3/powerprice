import { Component, OnInit } from '@angular/core';
import { WindmillType } from './../../core/models/windmill-type.enum';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { WindmillService } from '../../core/services/windmill/windmill.service';
import { Windmill } from './../../core/models/windmill';
import { MatTableDataSource } from '@angular/material/table';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-windmill',
  templateUrl: './windmill.component.html',
  styleUrls: ['./windmill.component.scss']
})
export class WindmillComponent implements OnInit {

  customerId: number;
  windmillEnum = WindmillType;
  windmillTypes: string[];
  selectedType: string;
  displayedColumns: string[] = ['amount', 'windmillType'];
  columnsToDisplay: string[] = this.displayedColumns.slice();
  windmillList: Windmill[] = [];
  dataSource = new MatTableDataSource<Windmill>(this.windmillList);
  windmillAmount = new FormControl();

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private windmillService: WindmillService
  ) {

  }

  ngOnInit() {
    const key = 'id';
    this.route.params.subscribe(
      params => {
        this.customerId = +params[key];
      }
    );
    this.windmillTypes = Object.keys(this.windmillEnum);
  }

  addWindmillClick() {

    const result = this.windmillList.find(windmill => {
      return windmill.windmillType === WindmillType[this.selectedType];
    });


    // check if inserted values are existing
    if (this.windmillAmount.value > 0 && WindmillType[this.selectedType] !== undefined) {

      // Added windmill type already exists do not add new but update current values
      if (result == null) {
        this.windmillList.push(new Windmill(this.customerId, WindmillType[this.selectedType], this.windmillAmount.value));
        this.dataSource._updateChangeSubscription();
      }
      else {
        result.amount += this.windmillAmount.value;
      }

    }
  }

  addSolarpanelClick() {
    this.windmillService.addWindmills(this.windmillList);
    this.router.navigate(['add-solar-panel', this.customerId]);
  }
}


import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { WindmillComponent } from './windmill.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Router, ActivatedRoute } from '@angular/router';
import { WindmillService } from 'src/app/core/services/windmill/windmill.service';
import { of } from 'rxjs';
import { Windmill } from 'src/app/core/models/windmill';
import { WindmillType } from 'src/app/core/models/windmill-type.enum';
import { FormControl } from '@angular/forms';

describe('WindmillComponent', () => {
  let component: WindmillComponent;
  let fixture: ComponentFixture<WindmillComponent>;
  let windmillService: WindmillService;
  let router: Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule
      ],
      declarations: [
        WindmillComponent
      ],
      providers: [
        RouterTestingModule,
        WindmillService,
        {
          provide: ActivatedRoute,
          useValue: {
            params: of({ id: '8' })
          }
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WindmillComponent);
    component = fixture.componentInstance;

    windmillService = TestBed.inject(WindmillService);
    router = TestBed.inject(Router);
  });

  describe('#ngOnInit', () => {
    // ActivatedRouteMock is injected in TestBed
    it('should resolve id from route params', () => {
      // Act
      fixture.detectChanges();

      // Assert
      expect(component.customerId).toBe(8);
    });
  });

  describe('#addWindmillClick', () => {
    it('should add total windmill amount with the same windmill type together', () => {
      // Arrange
      const windmills = [
        new Windmill(1, WindmillType.DARRIEUS, 10)
      ];
      component.windmillList = windmills;
      component.selectedType = WindmillType.DARRIEUS;
      component.windmillAmount = new FormControl(20);

      // Act
      component.addWindmillClick();

      // Assert
      expect(windmills.length).toBe(1);
      expect(windmills[0].windmillType).toBe(WindmillType.DARRIEUS);
      expect(windmills[0].amount).toBe(30);
    });

    it('should add new windmill object when windmill with given type does not exist', () => {
      // Arrange
      const windmills = [
        new Windmill(1, WindmillType.DARRIEUS, 10)
      ];
      component.windmillList = windmills;
      component.selectedType = WindmillType.MOD_2;
      component.windmillAmount = new FormControl(20);

      // Act
      component.addWindmillClick();

      // Assert
      expect(windmills.length).toBe(2);
      expect(windmills[1].windmillType).toBe(WindmillType.MOD_2);
      expect(windmills[1].amount).toBe(20);
    });
  });

  describe('#addSolarPanelClick', () => {
    it('should post currently saved list of windmills', () => {
      // Arrange
      const windmills = [
        new Windmill(1, WindmillType.DARRIEUS, 10),
        new Windmill(1, WindmillType.MOD_2, 30)
      ];
      component.windmillList = windmills;

      spyOn(router, 'navigate');
      spyOn(windmillService, 'addWindmills').and.stub();

      // Act
      component.addSolarpanelClick();

      // Assert
      expect(windmillService.addWindmills).toHaveBeenCalledWith(windmills);
    });

    it('should navigate to add-solar-panel when user is finished', () => {
      // Arrange
      spyOn(router, 'navigate');
      spyOn(windmillService, 'addWindmills').and.stub();
      component.customerId = 1;

      // Act
      component.addSolarpanelClick();

      // Assert
      expect(router.navigate).toHaveBeenCalledWith(['add-solar-panel', 1]);
    });
  });
});

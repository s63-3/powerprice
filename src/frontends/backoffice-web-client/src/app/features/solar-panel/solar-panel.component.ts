import { Component, OnInit } from '@angular/core';
import { SolarPanel } from './../../core/models/solar-panel';
import { MatTableDataSource } from '@angular/material/table';
import { FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { SolarPanelService } from '../../core/services/solar-panel/solar-panel.service';


@Component({
  selector: 'app-solar-panel',
  templateUrl: './solar-panel.component.html',
  styleUrls: ['./solar-panel.component.scss']
})
export class SolarPanelComponent implements OnInit {

  customerId: number;
  displayedColumns: string[] = ['amount', 'numberOfSquareMeters'];
  columnsToDisplay: string[] = this.displayedColumns.slice();
  solarPanelList: SolarPanel[] = [];
  dataSource = new MatTableDataSource<SolarPanel>(this.solarPanelList);
  solarPanelAmount = new FormControl();
  numberOfSquares = new FormControl();

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private solarPanelService: SolarPanelService
  ) {

  }

  ngOnInit() {
    const key = 'id';
    this.route.params.subscribe(
      params => {
        this.customerId = +params[key];
      }
    );
  }

  addSolarPanelClick() {
    if (this.numberOfSquares.value !== null &&
      this.numberOfSquares.value > 0 &&
      this.solarPanelAmount.value !== null &&
      this.solarPanelAmount.value > 0) {
      const result = this.solarPanelList.find(solarPanel => {
        return solarPanel.numberOfSquareMeters === this.numberOfSquares.value;
      });

      if (!result) {
        this.solarPanelList.push(new SolarPanel(this.customerId, this.numberOfSquares.value, this.solarPanelAmount.value));
        this.dataSource._updateChangeSubscription();
      } else {
        result.amount += this.solarPanelAmount.value;
      }
    }
  }

  finishClick() {
    this.solarPanelService.addSolarPanels(this.solarPanelList);
    this.router.navigate(['login']);
  }
}

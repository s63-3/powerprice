import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { SolarPanelComponent } from './solar-panel.component';
import { SolarPanelService } from 'src/app/core/services/solar-panel/solar-panel.service';
import { ActivatedRoute, Router } from '@angular/router';
import { of } from 'rxjs';
import { SolarPanel } from 'src/app/core/models/solar-panel';
import { LoginComponent } from '../login/login.component';
import { FormControl } from '@angular/forms';

describe('SolarPanelComponent', () => {
  let component: SolarPanelComponent;
  let fixture: ComponentFixture<SolarPanelComponent>;
  let solarPanelService: SolarPanelService;
  let router: Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule
      ],
      declarations: [
        SolarPanelComponent
      ],
      providers: [
        RouterTestingModule,
        SolarPanelService,
        {
          provide: ActivatedRoute,
          useValue: {
            params: of({ id: '8' })
          }
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolarPanelComponent);
    component = fixture.componentInstance;

    solarPanelService = TestBed.inject(SolarPanelService);
    router = TestBed.inject(Router);
  });

  describe('#ngOnit', () => {
    // ActivatedRouteMock is injected in TestBed
    it('should resolve id from route params', () => {
      // Act
      fixture.detectChanges();

      // Assert
      expect(component.customerId).toBe(8);
    });
  });

  describe('#addSolarPanelClick', () => {
    it('should add total solar panel amount with the same size together', () => {
      // Arrange
      const solarPanels = [
        new SolarPanel(1, 10, 5)
      ];
      component.solarPanelList = solarPanels;
      component.numberOfSquares = new FormControl(10);
      component.solarPanelAmount = new FormControl(20);

      // Act
      component.addSolarPanelClick();

      // Assert
      expect(component.solarPanelList.length).toBe(1);
      expect(component.solarPanelList[0].amount).toBe(25);
      expect(component.solarPanelList[0].numberOfSquareMeters).toBe(10);
    });

    it('should add new solar panel object when solar panel with specified size does not exist', () => {
      // Arrange
      const solarPanels = [
        new SolarPanel(1, 10, 5)
      ];
      component.solarPanelList = solarPanels;
      component.numberOfSquares = new FormControl(20);
      component.solarPanelAmount = new FormControl(30);

      // Act
      component.addSolarPanelClick();

      // Assert
      expect(component.solarPanelList.length).toBe(2);
      expect(component.solarPanelList[1].amount).toBe(30);
      expect(component.solarPanelList[1].numberOfSquareMeters).toBe(20);
    });
  });

  describe('#finishClick', () => {
    it('should post currently saved list of solar panels', () => {
      // Arrange
      const solarPanels = [
        new SolarPanel(1, 10, 8),
        new SolarPanel(1, 90, 1)
      ];
      component.solarPanelList = solarPanels;

      spyOn(router, 'navigate');
      spyOn(solarPanelService, 'addSolarPanels').and.stub();

      // Act
      component.finishClick();

      // Assert
      expect(solarPanelService.addSolarPanels).toHaveBeenCalledWith(solarPanels);
    });

    it('should route to login when finished', () => {
      // Arrange
      spyOn(router, 'navigate');
      spyOn(solarPanelService, 'addSolarPanels').and.stub();

      // Act
      component.finishClick();

      // Assert
      expect(router.navigate).toHaveBeenCalledWith(['login']);
    });
  });
});

import { RouterTestingModule } from '@angular/router/testing';
import { DashboardComponent } from './dashboard.component';
import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { GridSimulationService } from 'src/app/core/services/grid-simulation/grid-simulation.service';

describe('DashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;
  let gridSimulationService: GridSimulationService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule
      ],
      declarations: [
        DashboardComponent
      ],
      providers: [
        GridSimulationService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;

    gridSimulationService = TestBed.inject(GridSimulationService);
  });

  describe('#ngOnInit', () => {
    it('should get grid usage on init', fakeAsync(async () => {
      // Arrange
      spyOn(gridSimulationService, 'getGridUsage').and.resolveTo(20);
      spyOn(gridSimulationService, 'getGridProduction').and.stub();

      // Act
      fixture.detectChanges();
      tick();

      // Assert
      expect(component.gridUsage).toBe(20);
    }));

    it('should get grid production on init', fakeAsync(async () => {
      // Arrange
      spyOn(gridSimulationService, 'getGridUsage').and.stub();
      spyOn(gridSimulationService, 'getGridProduction').and.resolveTo(10);

      // Act
      fixture.detectChanges();
      tick();

      // Assert
      expect(component.gridProduction).toBe(10);
    }));
  });
});
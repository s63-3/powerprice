import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { GridSimulationService } from 'src/app/core/services/grid-simulation/grid-simulation.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  gridUsage = -1;
  gridProduction = -1;

  constructor(private gridSimulationService: GridSimulationService) { }

  async ngOnInit() {
    this.gridUsage = await this.gridSimulationService.getGridUsage();
    this.gridProduction = await this.gridSimulationService.getGridProduction();
  }
}

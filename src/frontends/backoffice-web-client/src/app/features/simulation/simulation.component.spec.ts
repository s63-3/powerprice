import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { SimulationComponent } from './simulation.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { GridSimulationService } from 'src/app/core/services/grid-simulation/grid-simulation.service';
import { WeatherSimulation } from 'src/app/core/models/weather-simulation';

describe('SimulationComponent', () => {
  let component: SimulationComponent;
  let fixture: ComponentFixture<SimulationComponent>;
  let gridSimulationService: GridSimulationService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      declarations: [ SimulationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimulationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    gridSimulationService = TestBed.inject(GridSimulationService);
  });

  it('should initialize with simulated field on false', () => {
    expect(component.simulated).toBeFalse();
  });

  describe('#simulate', () => {
    describe('When a field is undefined', () => {
      it('should show alert about having to fill every field when given windspeed is undefined', fakeAsync(async () => {
        // Arrange
        spyOn(window, 'alert');

        component.windSpeed = undefined;
        component.pressure = 1050;
        component.uvIndex = 5;
        component.temperature = 20;
        component.cloudCover = 0.5;

        // Act
        await component.simulate();
        tick();

        // Assert
        expect(alert).toHaveBeenCalledWith('Fill in every field.');
      }));

      it('should show alert about having to fill in every field when given pressure is undefined', fakeAsync(async () => {
        // Arrange
        spyOn(window, 'alert');

        component.windSpeed = 20;
        component.pressure = undefined;
        component.uvIndex = 5;
        component.temperature = 20;
        component.cloudCover = 0.5;

        // Act
        await component.simulate();
        tick();

        // Assert
        expect(alert).toHaveBeenCalledWith('Fill in every field.');
      }));

      it('should show alert about having to fill in every field when given uvIndex is undefined', fakeAsync(async () => {
        // Arrange
        spyOn(window, 'alert');

        component.windSpeed = 20;
        component.pressure = 1050;
        component.uvIndex = undefined;
        component.temperature = 20;
        component.cloudCover = 0.5;

        // Act
        await component.simulate();
        tick();

        // Assert
        expect(alert).toHaveBeenCalledWith('Fill in every field.');
      }));

      it('should show alert about having to fill in every field when given temperature is undefined', fakeAsync(async () => {
        // Arrange
        spyOn(window, 'alert');

        component.windSpeed = 20;
        component.pressure = 1050;
        component.uvIndex = 5;
        component.temperature = undefined;
        component.cloudCover = 0.5;

        // Act
        await component.simulate();
        tick();

        // Assert
        expect(alert).toHaveBeenCalledWith('Fill in every field.');
      }));

      it('should show alert about having to fill in every field when given cloudCover is undefined', fakeAsync(async () => {
        // Arrange
        spyOn(window, 'alert');

        component.windSpeed = 20;
        component.pressure = 1050;
        component.uvIndex = 5;
        component.temperature = 20;
        component.cloudCover = undefined;

        // Act
        await component.simulate();
        tick();

        // Assert
        expect(alert).toHaveBeenCalledWith('Fill in every field.');
      }));
    });

    describe('When a given field is not within the required range', () => {
      it('should show alert about checking weather ranges when windSpeed is below minimum', fakeAsync(async () => {
        // Arrange
        spyOn(window, 'alert');

        component.windSpeed = -1;
        component.pressure = 1050;
        component.uvIndex = 5;
        component.temperature = 20;
        component.cloudCover = 0.5;

        // Act
        await component.simulate();
        tick();

        // Assert
        expect(alert).toHaveBeenCalledWith('Check weather ranges, one of the ranges is incorrect.');
      }));

      it('should show alert about checking weather ranges when windSpeed is above maximum', fakeAsync(async () => {
        // Arrange
        spyOn(window, 'alert');

        component.windSpeed = 51;
        component.pressure = 1050;
        component.uvIndex = 5;
        component.temperature = 20;
        component.cloudCover = 0.5;

        // Act
        await component.simulate();
        tick();

        // Assert
        expect(alert).toHaveBeenCalledWith('Check weather ranges, one of the ranges is incorrect.');
      }));

      it('should show alert about checking weather ranges when pressure is below minimum', fakeAsync(async () => {
        // Arrange
        spyOn(window, 'alert');

        component.windSpeed = 30;
        component.pressure = 999;
        component.uvIndex = 5;
        component.temperature = 20;
        component.cloudCover = 0.5;

        // Act
        await component.simulate();
        tick();

        // Assert
        expect(alert).toHaveBeenCalledWith('Check weather ranges, one of the ranges is incorrect.');
      }));

      it('should show alert about checking weather ranges when pressure is above maximum', fakeAsync(async () => {
        // Arrange
        spyOn(window, 'alert');

        component.windSpeed = 30;
        component.pressure = 1101;
        component.uvIndex = 5;
        component.temperature = 20;
        component.cloudCover = 0.5;

        // Act
        await component.simulate();
        tick();

        // Assert
        expect(alert).toHaveBeenCalledWith('Check weather ranges, one of the ranges is incorrect.');
      }));

      it('should show alert about checking weather ranges when uvIndex is below minimum', fakeAsync(async () => {
        // Arrange
        spyOn(window, 'alert');

        component.windSpeed = 30;
        component.pressure = 1050;
        component.uvIndex = -1;
        component.temperature = 20;
        component.cloudCover = 0.5;

        // Act
        await component.simulate();
        tick();

        // Assert
        expect(alert).toHaveBeenCalledWith('Check weather ranges, one of the ranges is incorrect.');
      }));

      it('should show alert about checking weather ranges when uvIndex is above maximum', fakeAsync(async () => {
        // Arrange
        spyOn(window, 'alert');

        component.windSpeed = 30;
        component.pressure = 1050;
        component.uvIndex = 11;
        component.temperature = 20;
        component.cloudCover = 0.5;

        // Act
        await component.simulate();
        tick();

        // Assert
        expect(alert).toHaveBeenCalledWith('Check weather ranges, one of the ranges is incorrect.');
      }));

      it('should show alert about checking weather ranges when temperature is below minimum', fakeAsync(async () => {
        // Arrange
        spyOn(window, 'alert');

        component.windSpeed = 30;
        component.pressure = 1050;
        component.uvIndex = 5;
        component.temperature = -31;
        component.cloudCover = 0.5;

        // Act
        await component.simulate();
        tick();

        // Assert
        expect(alert).toHaveBeenCalledWith('Check weather ranges, one of the ranges is incorrect.');
      }));

      it('should show alert about checking weather ranges when temperature is above maximum', fakeAsync(async () => {
        // Arrange
        spyOn(window, 'alert');

        component.windSpeed = 30;
        component.pressure = 1050;
        component.uvIndex = 5;
        component.temperature = 51;
        component.cloudCover = 0.5;

        // Act
        await component.simulate();
        tick();

        // Assert
        expect(alert).toHaveBeenCalledWith('Check weather ranges, one of the ranges is incorrect.');
      }));

      it('should show alert about checking weather ranges when cloudCover is below minimum', fakeAsync(async () => {
        // Arrange
        spyOn(window, 'alert');

        component.windSpeed = 30;
        component.pressure = 1050;
        component.uvIndex = 5;
        component.temperature = 20;
        component.cloudCover = -0.1;

        // Act
        await component.simulate();
        tick();

        // Assert
        expect(alert).toHaveBeenCalledWith('Check weather ranges, one of the ranges is incorrect.');
      }));

      it('should show alert about checking weather ranges when cloudCover is above maximum', fakeAsync(async () => {
        // Arrange
        spyOn(window, 'alert');

        component.windSpeed = 30;
        component.pressure = 1050;
        component.uvIndex = 5;
        component.temperature = 20;
        component.cloudCover = 1.1;

        // Act
        await component.simulate();
        tick();

        // Assert
        expect(alert).toHaveBeenCalledWith('Check weather ranges, one of the ranges is incorrect.');
      }));
    });

    it('should get weather simulation results when given weather data has maximum allowed values', fakeAsync(async () => {
      // Arrange
      const expectedSimulationData = new WeatherSimulation(10, 20);

      spyOn(window, 'alert');
      spyOn(gridSimulationService, 'getSimulatedProduction').and.resolveTo(expectedSimulationData);

      component.windSpeed = 50;
      component.pressure = 1100;
      component.uvIndex = 10;
      component.temperature = 50;
      component.cloudCover = 1;

      // Act
      await component.simulate();
      tick();

      // Assert
      expect(expectedSimulationData).toEqual(component.weatherSimulation);
      expect(component.simulated).toBeTrue();
    }));

    it('should get weather simulation results when given weather data has minimum allowed values', fakeAsync(async () => {
      // Arrange
      const expectedSimulationData = new WeatherSimulation(10, 20);

      spyOn(window, 'alert');
      spyOn(gridSimulationService, 'getSimulatedProduction').and.resolveTo(expectedSimulationData);

      component.windSpeed = 0;
      component.pressure = 1000;
      component.uvIndex = 0;
      component.temperature = -30;
      component.cloudCover = 0;

      // Act
      await component.simulate();
      tick();

      // Assert
      expect(expectedSimulationData).toEqual(component.weatherSimulation);
      expect(component.simulated).toBeTrue();
    }));

    describe('#ifSimulated', () => {
      it('should return the value of the "simulated" field', () => {
        expect(component.ifSimulated()).toBeFalse();
        component.simulated = true;
        expect(component.ifSimulated()).toBeTrue();
      });
    });
  });
});

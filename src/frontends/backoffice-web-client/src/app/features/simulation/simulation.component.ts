import { Component, OnInit } from '@angular/core';
import { GridSimulationService } from 'src/app/core/services/grid-simulation/grid-simulation.service';
import { Weather } from 'src/app/core/models/weather';
import { WeatherSimulation } from 'src/app/core/models/weather-simulation';

@Component({
  selector: 'app-simulation',
  templateUrl: './simulation.component.html',
  styleUrls: ['./simulation.component.scss']
})
export class SimulationComponent implements OnInit {

  windSpeed: number;
  pressure: number;
  uvIndex: number;
  temperature: number;
  cloudCover: number;
  weatherSimulation: WeatherSimulation;
  solarPanelProduction: number;
  windmillProduction: number;
  simulated: boolean;

  constructor(private gridSimulationService: GridSimulationService) {
    this.simulated = false;
  }

  ngOnInit(): void {
  }

  async simulate() {
    const weather = new Weather(this.windSpeed, this.pressure, this.uvIndex, this.temperature, this.cloudCover);
    if(weather.checkIfValid()) {
      if(weather.checkRanges()) {
        this.weatherSimulation = await this.gridSimulationService.getSimulatedProduction(weather);
        this.simulated = true;
        this.solarPanelProduction = this.weatherSimulation.solarPanelProduction;
        this.windmillProduction = this.weatherSimulation.windmillProduction;
      } else {
        alert('Check weather ranges, one of the ranges is incorrect.');
      }
    } else {
      alert('Fill in every field.');
    }
  }

  ifSimulated() {
    return this.simulated;
  }
}

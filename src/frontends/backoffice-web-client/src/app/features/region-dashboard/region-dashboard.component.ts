import { Component, OnInit } from '@angular/core';
import { RegionGridService } from 'src/app/core/services/region-grid/region-grid.service';
import { RegionGrid } from 'src/app/core/models/region-grid';

@Component({
  selector: 'app-region-dashboard',
  templateUrl: './region-dashboard.component.html',
  styleUrls: ['./region-dashboard.component.scss']
})
export class RegionDashboardComponent implements OnInit {

  group1Status: RegionGrid;
  group2Status: RegionGrid;
  group3Status: RegionGrid;
  group4Status: RegionGrid;

  group1Available = true;
  group2Available = true;
  group3Available = true;
  group4Available = true;

  constructor(private regionGridService: RegionGridService) { }

  async ngOnInit(): Promise<void> {
    try {
      const grids = await this.regionGridService.getGridStatusOtherRegions();
      console.log(grids);

      if (grids[0] != null) {
        this.group1Status = grids[0];
      } else {
        this.group1Available = false;
      }
      if (grids[1] != null) {
        this.group2Status = grids[1];
      } else {
        this.group2Available = false;
      }
      if (grids[2] != null) {
        this.group4Status = grids[2];
      } else {
        this.group4Available = false;
      }
    } catch (e) {
      console.log(e);
      this.group1Available = false;
      this.group2Available = false;
      this.group4Available = false;
    }

    try {
      this.group3Status = await this.regionGridService.getGridStatusBrabant();
    } catch (e) {
      console.log(e);
      this.group3Available = false;
    }
    if (this.group3Status == null) {
      this.group3Available = false;
    }
  }


}

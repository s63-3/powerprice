import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { RegionDashboardComponent } from './region-dashboard.component';
import { RegionGridService } from 'src/app/core/services/region-grid/region-grid.service';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RegionGrid } from 'src/app/core/models/region-grid';

describe('RegionDashboardComponent', () => {
  let component: RegionDashboardComponent;
  let fixture: ComponentFixture<RegionDashboardComponent>;
  let regionService: RegionGridService;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientTestingModule],
      declarations: [ RegionDashboardComponent ],
      providers: [RegionGridService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegionDashboardComponent);
    component = fixture.componentInstance;
    regionService = TestBed.inject(RegionGridService);
  });

  describe('#ngOnInit', () => {
    it('should get all gridStatus of other regions', fakeAsync(async () => {
      const expectedGrids = [
        new RegionGrid(new Date(), 'Limburg', 2000, 3000),
        new RegionGrid(new Date(), 'Zeeland', 1000, 800),
        new RegionGrid(new Date(), 'Zuid-Holland', 6000, 8000)
      ];
      spyOn(regionService, 'getGridStatusOtherRegions').and.resolveTo(expectedGrids);
      spyOn(regionService, 'getGridStatusBrabant').and.stub();


      fixture.detectChanges();
      tick();

      expect(component.group1Status).toBe(expectedGrids[0]);
      expect(component.group2Status).toBe(expectedGrids[1]);
      expect(component.group4Status).toBe(expectedGrids[2]);
      expect(component.group1Available).toBeTrue();
      expect(component.group2Available).toBeTrue();
      expect(component.group4Available).toBeTrue();
    }))

    it('should get own gridStatus', fakeAsync(async () => {
      const expectedGrid = new RegionGrid(new Date(), 'Noord-Brabant', 1000, 1000);
      spyOn(regionService, 'getGridStatusOtherRegions').and.resolveTo([]);
      spyOn(regionService, 'getGridStatusBrabant').and.resolveTo(expectedGrid);


      fixture.detectChanges();
      tick();

      expect(component.group3Status).toBe(expectedGrid);
      expect(component.group3Available).toBeTrue();
    }))
  })
});

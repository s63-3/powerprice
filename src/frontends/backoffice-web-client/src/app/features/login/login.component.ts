import { Component, OnInit } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loggedIn = false;
  constructor(private keycloak: KeycloakService) { }

  ngOnInit(): void {
    this.checkLogin();
  }

  async checkLogin() {
    this.loggedIn = await this.keycloak.isLoggedIn();
  }

  login() {
    this.keycloak.login();
  }

}

import { async, ComponentFixture, TestBed, tick, fakeAsync } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AddCustomerComponent } from './add-customer.component';
import { RouterTestingModule } from '@angular/router/testing';
import { CustomerType } from 'src/app/core/models/customer-type';
import { Router } from '@angular/router';
import { WindmillComponent } from '../windmill/windmill.component';
import { UserService } from 'src/app/core/services/user/user.service';
import { NewCustomer } from 'src/app/core/models/newcustomer';

describe('AddCustomerComponent', () => {
  let component: AddCustomerComponent;
  let fixture: ComponentFixture<AddCustomerComponent>;
  let router: Router;
  let userService: UserService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes(
          [
            { path: 'add-windmill', component: WindmillComponent }
          ]
        ),
        HttpClientTestingModule
      ],
      declarations: [
        AddCustomerComponent
      ],
      providers: [
        UserService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCustomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    router = TestBed.inject(Router);
    userService = TestBed.inject(UserService);
  });

  describe('#addCustomerClick', () => {
    describe('test invalid user input', () => {
      const invalidCustomers = [
        {
          undefinedParameter: 'customer code',
          code: undefined,
          emailAddress: 'johndoe@gmail.com',
          firstName: 'John',
          lastName: 'Doe',
          streetName: 'Hopjesstraat',
          streetNumber: 40,
          postAddress: '4208AB',
          country: 'The Netherlands',
          buildingType: CustomerType.BEDRIJFSGEBOUW,
          companyName: 'Google Inc.',
          preferredLanguage: 'nl'
        },
        {
          undefinedParameter: 'email address',
          code: '1390VI',
          emailAddress: undefined,
          firstName: 'John',
          lastName: 'Doe',
          streetName: 'Hopjesstraat',
          streetNumber: 40,
          postAddress: '4208AB',
          country: 'The Netherlands',
          buildingType: CustomerType.BEDRIJFSGEBOUW,
          companyName: 'Google Inc.',
          preferredLanguage: 'nl'
        },
        {
          undefinedParameter: 'first name',
          code: '1910AG',
          firstName: undefined,
          lastName: 'Doe',
          streetName: 'Hopjesstraat',
          streetNumber: 40,
          postAddress: '4208AB',
          country: 'The Netherlands',
          buildingType: CustomerType.BEDRIJFSGEBOUW,
          companyName: 'Google Inc.',
          preferredLanguage: 'nl'
        },
        {
          undefinedParameter: 'last name',
          code: '1910AG',
          emailAddress: 'johndoe@gmail.com',
          firstName: 'John',
          lastName: undefined,
          streetName: 'Hopjesstraat',
          streetNumber: 40,
          postAddress: '4208AB',
          country: 'The Netherlands',
          buildingType: CustomerType.BEDRIJFSGEBOUW,
          companyName: 'Google Inc.',
          preferredLanguage: 'nl'
        },
        {
          undefinedParameter: 'street name',
          code: '1910AG',
          emailAddress: 'johndoe@gmail.com',
          firstName: 'John',
          lastName: 'Doe',
          streetName: undefined,
          streetNumber: 40,
          postAddress: '4208AB',
          country: 'The Netherlands',
          buildingType: CustomerType.BEDRIJFSGEBOUW,
          companyName: 'Google Inc.',
          preferredLanguage: 'nl'
        },
        {
          undefinedParameter: 'street number',
          code: '1910AG',
          emailAddress: 'johndoe@gmail.com',
          firstName: 'John',
          lastName: 'Doe',
          streetName: 'Hopjesstraat',
          streetNumber: undefined,
          postAddress: '4208AB',
          country: 'The Netherlands',
          buildingType: CustomerType.BEDRIJFSGEBOUW,
          companyName: 'Google Inc.',
          preferredLanguage: 'nl'
        },
        {
          undefinedParameter: 'postal code',
          code: '1910AG',
          emailAddress: 'johndoe@gmail.com',
          firstName: 'John',
          lastName: 'Doe',
          streetName: 'Hopjesstraat',
          streetNumber: 40,
          postAddress: undefined,
          country: 'The Netherlands',
          buildingType: CustomerType.BEDRIJFSGEBOUW,
          companyName: 'Google Inc.',
          preferredLanguage: 'nl'
        },
        {
          undefinedParameter: 'country',
          code: '1910AG',
          emailAddress: 'johndoe@gmail.com',
          firstName: 'John',
          lastName: 'Doe',
          streetName: 'Hopjesstraat',
          streetNumber: 40,
          postAddress: '4208AB',
          country: undefined,
          buildingType: CustomerType.BEDRIJFSGEBOUW,
          companyName: 'Google Inc.',
          preferredLanguage: 'nl'
        },
        {
          undefinedParameter: 'building type',
          code: '1910AG',
          emailAddress: 'johndoe@gmail.com',
          firstName: 'John',
          lastName: 'Doe',
          streetName: 'Hopjesstraat',
          streetNumber: 40,
          postAddress: '4208AB',
          country: 'The Netherlands',
          buildingType: undefined,
          companyName: 'Google Inc.',
          preferredLanguage: 'nl'
        },
        {
          undefinedParameter: 'company name',
          code: '1910AG',
          emailAddress: 'johndoe@gmail.com',
          firstName: 'John',
          lastName: 'Doe',
          streetName: 'Hopjesstraat',
          streetNumber: 40,
          postAddress: '4208AB',
          country: 'The Netherlands',
          buildingType: CustomerType.BEDRIJFSGEBOUW,
          companyName: undefined,
          preferredLanguage: 'nl'
        },
        {
          undefinedParameter: 'preferred language',
          code: '1910AG',
          emailAddress: 'johndoe@gmail.com',
          firstName: 'John',
          lastName: 'Doe',
          streetName: 'Hopjesstraat',
          streetNumber: 40,
          postAddress: '4208AB',
          country: 'The Netherlands',
          buildingType: CustomerType.BEDRIJFSGEBOUW,
          companyName: 'Google Inc.',
          preferredLanguage: undefined
        }
      ];

      invalidCustomers.forEach((invalidCustomer) => {
        it(`should show error message when ${invalidCustomer.undefinedParameter} was not specified`, async () => {
          // Arrange
          component.code = invalidCustomer.code;
          component.emailAddress = invalidCustomer.emailAddress;
          component.firstName = invalidCustomer.firstName;
          component.lastName = invalidCustomer.lastName;
          component.streetName = invalidCustomer.streetName;
          component.streetNumber = invalidCustomer.streetNumber;
          component.postAddress = invalidCustomer.postAddress;
          component.country = invalidCustomer.country;
          component.buildingType = invalidCustomer.buildingType;
          component.companyName = invalidCustomer.companyName;
          component.preferredLanguage = invalidCustomer.preferredLanguage;

          // Act
          await component.addCustomerClick();

          // Assert
          expect(component.errorMessage).toBe('Fill in every field');
        });
      });
    });

    it('should post created user and retrieve the id from the response', async () => {
      // Arrange
      const newCustomer = new NewCustomer(
        '1910AG',
        'johndoe@gmail.com',
        'John',
        'Doe',
        'Hopjesstraat',
        40,
        '1901AG',
        'The Netherlands',
        CustomerType.RIJTJESWONING,
        'Google Inc.',
        'nl',
        10,
        14,
        220
      );

      component.code = newCustomer.code;
      component.firstName = newCustomer.firstName;
      component.lastName = newCustomer.lastName;
      component.emailAddress = newCustomer.emailAddress;
      component.streetName = newCustomer.streetName;
      component.streetNumber = newCustomer.streetNumber;
      component.postAddress = newCustomer.postAddress;
      component.country = newCustomer.country;
      component.buildingType = newCustomer.buildingType;
      component.companyName = newCustomer.companyName;
      component.preferredLanguage = newCustomer.preferredLanguage;
      component.baseLoad = newCustomer.baseLoad;
      component.peakLoad = newCustomer.peakLoad;
      component.maxVoltageLevel = newCustomer.maxVoltageLevel;

      spyOn(userService, 'addCustomer').and.resolveTo(5);
      spyOn(router, 'navigate').and.stub();

      // Act
      await component.addCustomerClick();

      // Assert
      expect(userService.addCustomer).toHaveBeenCalledWith(newCustomer);
      expect(component.id).toBe(5);
    });

    it('should navigate user to windmill component when user details are submitted', async () => {
      // Arrange
      component.code = '1910AG';
      component.emailAddress = 'johndoe@gmail.com';
      component.firstName = 'John';
      component.lastName = 'Doe';
      component.streetName = 'Hopjesstraat';
      component.streetNumber = 40;
      component.postAddress = '1901AG';
      component.country = 'The Netherlands';
      component.buildingType = CustomerType.RIJTJESWONING;
      component.companyName = 'Google Inc.';
      component.preferredLanguage = 'nl';
      component.baseLoad = 10;
      component.peakLoad = 14;
      component.maxVoltageLevel = 220;

      spyOn(router, 'navigate');
      spyOn(userService, 'addCustomer').and.resolveTo(1);

      // Act
      await component.addCustomerClick();

      // Assert
      expect(router.navigate).toHaveBeenCalledWith(['add-windmill', 1]);
    });
  });
});

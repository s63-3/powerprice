import { Component, OnInit } from '@angular/core';
import { CustomerType } from 'src/app/core/models/customer-type';
import { NewCustomer } from 'src/app/core/models/newcustomer';
import { UserService } from 'src/app/core/services/user/user.service';
import { Router } from '@angular/router';
import { LanguageType } from 'src/app/core/models/language-type';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-customer',
  templateUrl: './add-customer.component.html',
  styleUrls: ['./add-customer.component.scss']
})
export class AddCustomerComponent implements OnInit {

  errorMessage = '';
  buildingTypes: string[] = [
    CustomerType.RIJTJESWONING,
    CustomerType.VRIJSTAANDEWONING,
    CustomerType.APPARTEMENT,
    CustomerType.BEDRIJFSGEBOUW
  ];

  languages: string[] = [
    LanguageType.ENGLISH,
    LanguageType.DUTCH
  ];


  newCustomer: NewCustomer;

  id: number;
  code: string;
  emailAddress: string;
  firstName: string;
  lastName: string;
  streetName: string;
  streetNumber: number;
  postAddress: string;
  country: string;
  buildingType: CustomerType;
  companyName: string;
  preferredLanguage: string;
  baseLoad: number;
  peakLoad: number;
  maxVoltageLevel: number;

  constructor(private router: Router, private userService: UserService) { }

  ngOnInit(): void {
  }

  async addCustomerClick(): Promise<void> {
    this.newCustomer = new NewCustomer(this.code, this.emailAddress, this.firstName, this.lastName, this.streetName,
      this.streetNumber, this.postAddress, this.country, this.buildingType, this.companyName, this.preferredLanguage,
      this.baseLoad, this.peakLoad, this.maxVoltageLevel);

    if (this.newCustomer.checkIfValid()) {
      this.id = await this.userService.addCustomer(this.newCustomer);
      this.router.navigate(['add-windmill', this.id]);
    } else {
      this.errorMessage = 'Fill in every field';
    }
  }
}

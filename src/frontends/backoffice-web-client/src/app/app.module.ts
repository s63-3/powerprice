import { AuthGuard } from './core/guards/auth.guard';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { AppComponent, initializer } from './root/app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './features/login/login.component';
import { DashboardComponent } from './features/dashboard/dashboard.component';
import { Routes, RouterModule } from '@angular/router';
import { ToolbarComponent } from './shared/layout/toolbar/toolbar.component';
import { HttpClientModule } from '@angular/common/http';
import { AddCustomerComponent } from './features/add-customer/add-customer.component';
import { KeycloakAngularModule, KeycloakService } from 'keycloak-angular';
import { MaterialModule } from './root/modules/material.module';
import { AppRoutingModule } from './root/modules/app-routing.module';
import { UserService } from './core/services/user/user.service';
import { WindmillComponent } from './features/windmill/windmill.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SolarPanelComponent } from './features/solar-panel/solar-panel.component';
import { SimulationComponent } from './features/simulation/simulation.component';
import { RegionDashboardComponent } from './features/region-dashboard/region-dashboard.component';
import { GridStatusComponent } from './shared/grid-status/grid-status.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    ToolbarComponent,
    AddCustomerComponent,
    WindmillComponent,
    SolarPanelComponent,
    SimulationComponent,
    RegionDashboardComponent,
    GridStatusComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FlexLayoutModule,
    KeycloakAngularModule,
    MaterialModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [AuthGuard, {
    provide: APP_INITIALIZER,
    useFactory: initializer,
    multi: true,
    deps: [KeycloakService],
  }, UserService],

  exports: [RouterModule],
  bootstrap: [AppComponent]
})
export class AppModule { }

export class CustomerLocation {

  constructor(street: string, streetNumber: number, postalCode: string) {
    this.street = street;
    this.streetNumber = streetNumber;
    this.postalCode = postalCode;
  }
  street: string;
  streetNumber: number;
  postalCode: string;
}
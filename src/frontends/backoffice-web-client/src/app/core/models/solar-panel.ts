export class SolarPanel {

  numberOfSquareMeters: number;
  customerId: number;
  amount: number;

  constructor(customerId: number, numberOfSquareMeters: number, amount: number) {
    this.numberOfSquareMeters = numberOfSquareMeters;
    this.customerId = customerId;
    this.amount = amount;
  }
}
export class Date {
  day: number;
  month: number;
  year: number;
  timeInHours: number;
  timeInMinutes: number;

}
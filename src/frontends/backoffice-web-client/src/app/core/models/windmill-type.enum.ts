export enum WindmillType {
    TURBINE = 'TURBINE',
    MOD_2 = 'MOD_2',
    SAVONIUS = 'SAVONIUS',
    DARRIEUS = 'DARRIEUS',
    GROWIAN = 'GROWIAN'
}

import { CustomerLocation } from './customer-location';
import { EnergyGenerator } from './energy-generator';
import { CustomerType } from './customer-type';
export class Customer {

  constructor(generators: EnergyGenerator[], location: CustomerLocation, customerType: CustomerType) {
    this.generators = generators;
    this.location = location;
    this.customerType = customerType;
  }
  generators: EnergyGenerator[];
  location: CustomerLocation;
  customerType: CustomerType;

}


export enum LanguageType {
    ENGLISH = 'ENGLISH',
    DUTCH = 'DUTCH'
}
import { WindmillType } from './windmill-type.enum';

export class Windmill {

  windmillType: WindmillType;
  customerId: number;
  amount: number;

  constructor(customerId: number, windmillType: WindmillType, amount: number) {
    this.windmillType = windmillType;
    this.customerId = customerId;
    this.amount = amount;
  }
}

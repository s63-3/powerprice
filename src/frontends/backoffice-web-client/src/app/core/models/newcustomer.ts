import { CustomerType } from './customer-type';

export class NewCustomer {
  code: string;
  emailAddress: string;
  firstName: string;
  lastName: string;
  streetName: string;
  streetNumber: number;
  postAddress: string;
  country: string;
  buildingType: CustomerType;
  companyName: string;
  preferredLanguage: string;
  baseLoad: number;
  peakLoad: number;
  maxVoltageLevel: number;

  constructor(code: string, emailAddress: string, firstName: string, lastName: string, streetName: string, streetNumber: number,
              postAddress: string, country: string, buildingType: CustomerType, companyName: string, preferredLanguage: string,
              baseLoad: number, peakLoad: number, maxVoltageLevel: number) {
    this.code = code;
    this.emailAddress = emailAddress;
    this.firstName = firstName;
    this.lastName = lastName;
    this.streetName = streetName;
    this.streetNumber = streetNumber;
    this.postAddress = postAddress;
    this.country = country;
    this.buildingType = buildingType;
    this.companyName = companyName;
    this.preferredLanguage = preferredLanguage;
    this.baseLoad = baseLoad;
    this.peakLoad = peakLoad;
    this.maxVoltageLevel = maxVoltageLevel;
  }

  checkIfValid() {
    return this.code !== undefined && this.emailAddress !== undefined && this.firstName !== undefined && this.lastName !== undefined
            && this.streetName !== undefined && this.streetNumber !== undefined && this.postAddress !== undefined &&
            this.country !== undefined && this.buildingType !== undefined && this.companyName !== undefined &&
            this.preferredLanguage !== undefined;
  }
}
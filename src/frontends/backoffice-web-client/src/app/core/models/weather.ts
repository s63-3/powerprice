export class Weather {
  windSpeed: number;
  pressure: number;
  uvIndex: number;
  temperature: number;
  cloudCover: number;

  constructor( windSpeed: number, pressure: number, uvIndex: number, temperature: number, cloudCover: number) {
    this.windSpeed = windSpeed;
    this.pressure = pressure;
    this.uvIndex = uvIndex;
    this.temperature = temperature;
    this.cloudCover = cloudCover;
  }

  checkIfValid() {
    return this.windSpeed !== undefined && this.pressure !== undefined &&
    this.uvIndex !== undefined && this.temperature !== undefined && this.cloudCover !== undefined;
  }

  checkRanges(): boolean {
    if(this.windSpeed < 0 || this.windSpeed > 50) {
      return false;
    }
    if(this.pressure < 1000 || this.pressure > 1100) {
      return false;
    }
    if(this.uvIndex < 0 || this.uvIndex > 10) {
      return false;
    }
    if(this.temperature < -30 || this.temperature > 50) {
      return false;
    }
    if(this.cloudCover < 0 || this.cloudCover > 1) {
      return false;
    }
    return true;
  }
}
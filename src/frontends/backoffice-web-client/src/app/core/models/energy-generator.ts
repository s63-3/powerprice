import { EnergyLog } from './energy-log';

export class EnergyGenerator {

  constructor(usage: EnergyLog[], generating: EnergyLog[]) {
    this.usage = usage;
    this.generating = generating;
  }
  usage: EnergyLog[];
  generating: EnergyLog[];
}
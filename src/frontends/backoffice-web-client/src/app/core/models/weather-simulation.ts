export class WeatherSimulation {

  solarPanelProduction: number;
  windmillProduction: number;

  constructor(solarPanelProduction: number, windmillProduction: number) {
    this.solarPanelProduction = solarPanelProduction;
    this.windmillProduction = windmillProduction;
  }
}
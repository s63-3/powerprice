export class RegionGrid {

  date: Date;
  region: string;
  consumption: number;
  production: number;

  constructor(date: Date, region: string, consumption: number, production: number) {
    this.date = date;
    this.consumption = consumption;
    this.region = region;
    this.production = production;
  }
}
export class EnergyLog {
  constructor(amount: number, date: Date) {
    this.amount = amount;
    this.date = date;
  }
  amount: number;
  date: Date;
}
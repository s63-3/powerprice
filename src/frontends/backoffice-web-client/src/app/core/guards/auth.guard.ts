import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { KeycloakService, KeycloakAuthGuard } from 'keycloak-angular';

@Injectable()
export class AuthGuard extends KeycloakAuthGuard {
  constructor(protected router: Router, protected keycloakAngular: KeycloakService) {
    super(router, keycloakAngular);
  }

  isAccessAllowed(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
    return new Promise(async (resolve, reject) => {
      let granted = false;

      if (!this.authenticated) {
        this.router.navigateByUrl('/');
        resolve(granted);
      }

      const requiredRoles = ['employee'];

      for (const requiredRole of requiredRoles) {
        if (this.roles.indexOf(requiredRole) > -1) {
          granted = true;
          break;
        }
      }

      if (granted === false) {
        this.keycloakAngular.logout();
        await this.router.navigateByUrl('/');
      }
      resolve(granted);
    });
  }
}

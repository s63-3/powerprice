import { TestBed, fakeAsync, flushMicrotasks, tick } from '@angular/core/testing';
import { AuthGuard } from './auth.guard';
import { RouterTestingModule } from '@angular/router/testing';
import { KeycloakService } from 'keycloak-angular';
import { Mock } from 'protractor/built/driverProviders';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { DashboardComponent } from 'src/app/features/dashboard/dashboard.component';
import { LoginComponent } from 'src/app/features/login/login.component';

// De AuthGuard tests zijn momenteel afhankelijk van de implementatie van de parent class KeycloakAuthGuard om correct te functioneren
// deze tests kunnen dus breken vanwege een update in de library van keycloak-angular.
describe('AuthGuard', () => {
  let guard: AuthGuard;
  let keycloakService: KeycloakService;
  let mockActivatedRoute: ActivatedRouteSnapshot;
  let mockRouterState: RouterStateSnapshot;
  let router: Router;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes(
          [
            { path: '', component: LoginComponent },
            { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard]}
          ]
        )
      ],
      providers: [
        AuthGuard,
        KeycloakService
      ]
    });
    guard = TestBed.inject(AuthGuard);
    keycloakService = TestBed.inject(KeycloakService);
    router = TestBed.inject(Router);

    mockActivatedRoute = jasmine.createSpyObj<ActivatedRouteSnapshot>('ActivatedRouteSnapshot', ['toString']);
    mockRouterState = jasmine.createSpyObj<RouterStateSnapshot>('RouterStateSnapshot', ['toString']);

    spyOn(keycloakService, 'logout').and.stub();
    spyOn(router, 'navigateByUrl');
  });

  it('should navigate to / when user is not authenticated', async () => {
    // Arrange
    spyOn(keycloakService, 'isLoggedIn').and.resolveTo(false);
    spyOn(keycloakService, 'getUserRoles').and.returnValue(['']);

    // Act
    await guard.canActivate(mockActivatedRoute, mockRouterState);

    // Assert
    expect(router.navigateByUrl).toHaveBeenCalledWith('/');
  });

  it('should resolve to false when user is not authenticated', async () => {
    // Arrange
    spyOn(keycloakService, 'isLoggedIn').and.resolveTo(false);
    spyOn(keycloakService, 'getUserRoles').and.returnValue(['']);

    // Act
    const guardResult = await guard.canActivate(mockActivatedRoute, mockRouterState);

    // Assert
    expect(guardResult).toBe(false);
  });

  it('should resolve to true when user only has employee role', async () => {
    // Arrange
    spyOn(keycloakService, 'isLoggedIn').and.resolveTo(true);
    spyOn(keycloakService, 'getUserRoles').and.returnValue(['employee']);

    // Act
    const guardResult = await guard.canActivate(mockActivatedRoute, mockRouterState);

    // Assert
    expect(guardResult).toBe(true);
  });

  it('should resolve to true when user has multiple roles including employee role', async () => {
    // Arrange
    spyOn(keycloakService, 'isLoggedIn').and.resolveTo(true);
    spyOn(keycloakService, 'getUserRoles').and.returnValue(['employee', 'producer']);

    // Act
    const guardResult = await guard.canActivate(mockActivatedRoute, mockRouterState);

    // Assert
    expect(guardResult).toBe(true);
  });

  it('should resolve to false when user does not have any roles', async () => {
    // Arrange
    spyOn(keycloakService, 'isLoggedIn').and.resolveTo(true);
    spyOn(keycloakService, 'getUserRoles').and.returnValue(['']);

    // Act
    const guardResult = await guard.canActivate(mockActivatedRoute, mockRouterState);

    // Assert
    expect(guardResult).toBe(false);
  });

  it('should resolve to false when user does not have required customer role', async () => {
    // Arrange
    spyOn(keycloakService, 'isLoggedIn').and.resolveTo(true);
    spyOn(keycloakService, 'getUserRoles').and.returnValue(['producer']);

    // Act
    const guardResult = await guard.canActivate(mockActivatedRoute, mockRouterState);

    // Assert
    expect(guardResult).toBe(false);
  });

  it('should log out user when user does not have required customer role', async () => {
    // Arrange
    spyOn(keycloakService, 'isLoggedIn').and.resolveTo(true);
    spyOn(keycloakService, 'getUserRoles').and.returnValue(['producer']);

    // Act
    const guardResult = await guard.canActivate(mockActivatedRoute, mockRouterState);

    // Assert
    expect(keycloakService.logout).toHaveBeenCalledTimes(1);
  });

  it('should navigate user to / when user does not have required customer role', async () => {
    // Arrange
    spyOn(keycloakService, 'isLoggedIn').and.resolveTo(true);
    spyOn(keycloakService, 'getUserRoles').and.returnValue(['producer']);

    // Act
    const guardResult = await guard.canActivate(mockActivatedRoute, mockRouterState);

    // Assert
    expect(router.navigateByUrl).toHaveBeenCalledWith('/');
  });
});

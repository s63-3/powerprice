import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { WeatherSimulation } from '../../models/weather-simulation';
import { Weather } from '../../models/weather';

@Injectable({
  providedIn: 'root',
})
export class GridSimulationService {
  private baseUrl = environment.gatewayUrl;

  constructor(private http: HttpClient) { }

  getGridProduction(): Promise<number> {
    const endpoint = this.baseUrl + '/grid/employees/production';
    return this.http.get<number>(endpoint).toPromise();
  }

  getGridUsage(): Promise<number> {
    const endpoint = this.baseUrl + '/grid/employees/usage';
    return this.http.get<number>(endpoint).toPromise();
  }

  getSimulatedProduction(weather: Weather): Promise<WeatherSimulation> {
    const endpoint = this.baseUrl + '/grid/simulation/weather';
    return this.http.post<WeatherSimulation>(endpoint, weather).toPromise();
  }
}

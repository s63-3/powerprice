import {
  HttpTestingController,
  HttpClientTestingModule,
} from '@angular/common/http/testing';
import { GridSimulationService } from './grid-simulation.service';
import { TestBed } from '@angular/core/testing';
import { environment } from 'src/environments/environment';
import { Weather } from '../../models/weather';
import { WeatherSimulation } from '../../models/weather-simulation';

describe('GridSimulationService', () => {
  let service: GridSimulationService;
  let httpMock: HttpTestingController;
  const baseUrl = environment.gatewayUrl;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [GridSimulationService],
    });

    service = TestBed.inject(GridSimulationService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('#getGridProduction', () => {
    it('should send to valid endpoint and get a valid grid production', async () => {
      // Arrange
      const expectedGridProduction = 25;
      const getGridProductionCall = service.getGridProduction();

      const req = httpMock.expectOne(baseUrl + '/grid/employees/production');
      req.flush(expectedGridProduction);

      // Act
      const actualGridProduction = await getGridProductionCall;

      // Assert
      httpMock.verify();
      expect(actualGridProduction).toBe(expectedGridProduction);
      expect(req.request.method).toBe('GET');
    });
  });

  describe('#getGridUsage', () => {
    it('should send to valid endpoint and get a valid grid usage', async () => {
      // Arrange
      const expectedGridUsage = 15;
      const getGridUsageCall = service.getGridUsage();

      const req = httpMock.expectOne(baseUrl + '/grid/employees/usage');
      req.flush(expectedGridUsage);

      // Act
      const actualGridUsage = await getGridUsageCall;

      // Assert
      httpMock.verify();
      expect(actualGridUsage).toBe(expectedGridUsage);
      expect(req.request.method).toBe('GET');
    });
  });

  describe('#getSimulatedProduction', () => {
    it('should send to valid endpoint and get related WeatherSimulation data back', async () => {
      // Arrange
      const weather = new Weather(10, 10, 10, 10, 10);
      const getSimulatedProductionCall = service.getSimulatedProduction(weather);
      const weatherSimulationResponse = new WeatherSimulation(200, 100);

      const req = httpMock.expectOne(baseUrl + '/grid/simulation/weather');
      req.flush(weatherSimulationResponse);

      // Act
      const actualWeatherSimulationResponse = await getSimulatedProductionCall;

      // Assert
      httpMock.verify();
      expect(actualWeatherSimulationResponse).toEqual(weatherSimulationResponse);
    });
  });
});

import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { NewCustomer } from '../../models/newcustomer';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  customerId: number;
  private baseUrl = environment.gatewayUrl;

  constructor(private http: HttpClient) { }

  addCustomer(customer: NewCustomer): Promise<number> {
    const endpoint = this.baseUrl + '/customers/register';
    return this.http.post<number>(endpoint, customer).toPromise();
  }
}

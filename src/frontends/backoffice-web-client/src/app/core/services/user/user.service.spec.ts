import { UserService } from './user.service';
import {
  HttpTestingController,
  HttpClientTestingModule,
} from '@angular/common/http/testing';
import { environment } from 'src/environments/environment';
import { TestBed, fakeAsync, tick } from '@angular/core/testing';
import { CustomerType } from '../../models/customer-type';
import { NewCustomer } from '../../models/newcustomer';

describe('UserService', () => {
  let service: UserService;
  let httpMock: HttpTestingController;
  const baseUrl = environment.gatewayUrl;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [UserService],
    });

    service = TestBed.inject(UserService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('#addCustomer', () => {
    it('should post customer to valid endpoint and receive sent id back', async () => {
      // Arrange
      const customer = new NewCustomer(
        '1310AB',
        'johndoe@gmail.com',
        'John',
        'Doe',
        'Hopjesstraat',
        14,
        '2490AG',
        'The Netherlands',
        CustomerType.APPARTEMENT,
        'Google inc.',
        'nl',
        10,
        14,
        220
      );
      const addCustomerCall = service.addCustomer(customer);

      const req = httpMock.expectOne(baseUrl + '/customers/register');
      req.flush(1);

      // Act
      const response = await addCustomerCall;

      // Assert
      httpMock.verify();
      expect(response).toBe(1);
      expect(req.request.method).toBe('POST');
      expect(req.request.body).toEqual(customer);
    });
  });
});

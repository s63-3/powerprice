import { SolarPanelService } from './solar-panel.service';
import {
  HttpTestingController,
  HttpClientTestingModule,
} from '@angular/common/http/testing';
import { environment } from 'src/environments/environment';
import { TestBed } from '@angular/core/testing';
import { SolarPanel } from '../../models/solar-panel';

describe('SolarPanelService', () => {
  let service: SolarPanelService;
  let httpMock: HttpTestingController;
  const baseUrl = environment.gatewayUrl;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [SolarPanelService],
    });

    service = TestBed.inject(SolarPanelService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('#addSolarPanels', () => {
    it('should post to valid endpoint with given solar panels', async () => {
      // Arrange
      const expectedSolarPanels = [
        new SolarPanel(1, 5, 10),
        new SolarPanel(2, 8, 4),
      ];
      const addSolarPanelsCall = service.addSolarPanels(expectedSolarPanels);

      const req = httpMock.expectOne(baseUrl + '/employees/solar-panels');
      req.flush({});

      // Act
      await addSolarPanelsCall;

      // Assert
      httpMock.verify();
      expect(req.request.method).toBe('POST');
      expect(req.request.body).toEqual(expectedSolarPanels);
    });
  });
});

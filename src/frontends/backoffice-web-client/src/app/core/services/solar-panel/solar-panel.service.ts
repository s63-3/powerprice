import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { SolarPanel } from '../../models/solar-panel';

@Injectable({
  providedIn: 'root',
})
export class SolarPanelService {
  private baseUrl = environment.gatewayUrl;

  constructor(private http: HttpClient) { }

  addSolarPanels(solarPanels: SolarPanel[]): Promise<void> {
    const endpoint = this.baseUrl + '/employees/solar-panels';
    return this.http.post<void>(endpoint, solarPanels).toPromise();
  }
}


import {
  HttpTestingController,
  HttpClientTestingModule,
} from '@angular/common/http/testing';
import { environment } from 'src/environments/environment';
import { TestBed } from '@angular/core/testing';
import { SolarPanel } from '../../models/solar-panel';
import { RegionGridService } from './region-grid.service';
import { RegionGrid } from '../../models/region-grid';

describe('RegionGridService', () => {
  let service: RegionGridService;
  let httpMock: HttpTestingController;
  const baseUrl = environment.gatewayUrl;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [RegionGridService],
    });

    service = TestBed.inject(RegionGridService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('#getGridStatuses', () => {
    it('should get every other grid status', async () => {
      // Arrange
      const expectedGrids = [
        new RegionGrid(new Date(), 'Limburg', 2000, 3000),
        new RegionGrid(new Date(), 'Zeeland', 1000, 800),
        new RegionGrid(new Date(), 'Zuid-Holland', 6000, 8000)
      ]
      const getGridCall = service.getGridStatusOtherRegions();

      const req = httpMock.expectOne(baseUrl + '/external/status');
      req.flush(expectedGrids);

      // Act
      const actualGrids = await getGridCall;

      // Assert
      httpMock.verify();
      expect(actualGrids).toBe(expectedGrids);
      expect(req.request.method).toBe('GET');
    });

    it('should get own grid status', async () => {
      // Arrange
      const expectedGrid = new RegionGrid(new Date(), 'Noord-Brabant', 500, 600);
      const getGridCalll = service.getGridStatusBrabant();

      const req = httpMock.expectOne(baseUrl + '/grid/employees/status');
      req.flush(expectedGrid);

      // Act
      const actualGrids = await getGridCalll;

      // Assert
      httpMock.verify();
      expect(actualGrids).toBe(expectedGrid);
      expect(req.request.method).toBe('GET');
    });
  });
});

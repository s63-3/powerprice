import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { WeatherSimulation } from '../../models/weather-simulation';
import { Weather } from '../../models/weather';
import { RegionGrid } from '../../models/region-grid';

@Injectable({
  providedIn: 'root',
})
export class RegionGridService {
  private baseUrl = environment.gatewayUrl

  constructor(private http: HttpClient) { }

  getGridStatusOtherRegions(): Promise<RegionGrid[]> {
    const endpoint = this.baseUrl + '/external/status';
    return this.http.get<RegionGrid[]>(endpoint).toPromise();
  }

  getGridStatusBrabant(): Promise<RegionGrid> {
    const endpoint = this.baseUrl + '/grid/employees/status';
    return this.http.get<RegionGrid>(endpoint).toPromise();
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Windmill } from '../../models/windmill';

@Injectable({
  providedIn: 'root',
})
export class WindmillService {
  private baseUrl = environment.gatewayUrl;

  constructor(private http: HttpClient) {}

  addWindmills(windmills: Windmill[]): Promise<void> {
    const endpoint = this.baseUrl + '/employees/windmills';
    return this.http.post<void>(endpoint, windmills).toPromise();
  }
}

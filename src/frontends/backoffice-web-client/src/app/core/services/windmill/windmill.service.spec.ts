import { WindmillService } from './windmill.service';
import {
  HttpTestingController,
  HttpClientTestingModule,
} from '@angular/common/http/testing';
import { environment } from 'src/environments/environment';
import { TestBed, fakeAsync, tick } from '@angular/core/testing';
import { Windmill } from '../../models/windmill';
import { WindmillType } from '../../models/windmill-type.enum';

describe('WindmillService', () => {
  let service: WindmillService;
  let httpMock: HttpTestingController;
  const baseUrl = environment.gatewayUrl;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [WindmillService],
    });

    service = TestBed.inject(WindmillService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('#addWindmills', () => {
    it('should post to valid endpoint with given windmills', async () => {
      // Arrange
      const windmills = [
        new Windmill(1, WindmillType.SAVONIUS, 10),
        new Windmill(1, WindmillType.TURBINE, 5),
      ];
      const addWindmillsCall = service.addWindmills(windmills);

      const req = httpMock.expectOne(baseUrl + '/employees/windmills');
      req.flush({});

      // Act
      await addWindmillsCall;

      // Assert
      httpMock.verify();
      expect(req.request.method).toBe('POST');
      expect(req.request.body).toEqual(windmills);
    });
  });
});

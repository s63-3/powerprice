import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { KeycloakService } from 'keycloak-angular';


@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {

  isLoggedIn = false;
  username = 'XXX';
  constructor(private router: Router, private keycloak: KeycloakService) { }

  ngOnInit(): void {
    // Determine booleans here
    this.setUsername();
  }

  setUsername() {
    this.keycloak.isLoggedIn().then(value => {
      this.isLoggedIn = value;
      if (value) {
        this.username = this.keycloak.getUsername();
      }
    });
  }
  logOut() {
    this.keycloak.logout();
  }

}
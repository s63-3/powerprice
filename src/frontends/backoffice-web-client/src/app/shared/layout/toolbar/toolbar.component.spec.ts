import { KeycloakService } from 'keycloak-angular';
import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { ToolbarComponent } from './toolbar.component';
import { RouterTestingModule } from '@angular/router/testing';

describe('ToolbarComponent', () => {
  let component: ToolbarComponent;
  let fixture: ComponentFixture<ToolbarComponent>;
  let keycloakService: KeycloakService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        ToolbarComponent
      ],
      providers: [
        KeycloakService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToolbarComponent);
    component = fixture.componentInstance;

    keycloakService = TestBed.inject(KeycloakService);
  });

  it('should not be logged in by default', () => {
    expect(component.isLoggedIn).toBe(false);
  });

  it('should have XXX as default username', () => {
    expect(component.username).toBe('XXX');
  });

  describe('#ngOnInit', () => {
    it('should get username from keycloak', fakeAsync(async () => {
      // Arrange
      spyOn(keycloakService, 'isLoggedIn').and.resolveTo(true);
      spyOn(keycloakService, 'getUsername').and.returnValue('John Doe');

      // Act
      fixture.detectChanges();
      tick();

      // Assert
      expect(component.username).toBe('John Doe');
    }));

    it('should set user to logged in when user is logged in', fakeAsync(async () => {
      // Arrange
      spyOn(keycloakService, 'isLoggedIn').and.resolveTo(true);
      spyOn(keycloakService, 'getUsername').and.returnValue('John Doe');

      // Act
      fixture.detectChanges();
      tick();

      // Assert
      expect(component.isLoggedIn).toBe(true);
    }));
  });

  describe('#logout', () => {
    it('should call keycloak logout', fakeAsync(async () => {
      // Arrange
      spyOn(keycloakService, 'logout').and.stub();

      // Act
      component.logOut();
      tick();

      // Assert
      expect(keycloakService.logout).toHaveBeenCalledTimes(1);
    }));
  });
});

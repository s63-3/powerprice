import { Component, OnInit, Input } from '@angular/core';
import { RegionGrid } from 'src/app/core/models/region-grid';

@Component({
  selector: 'app-grid-status',
  templateUrl: './grid-status.component.html',
  styleUrls: ['./grid-status.component.scss']
})
export class GridStatusComponent implements OnInit {

  @Input()
  groupName: string;
  @Input()
  gridStatus: RegionGrid;

  @Input()
  available: boolean;
  constructor() { }

  ngOnInit(): void {
  }

}

import { LoginPage } from './login.po';
import { browser, logging } from 'protractor';

describe('workspace-project App', () => {
  let page: LoginPage;

  beforeEach(() => {
    page = new LoginPage();
  });

  it('should display login button', () => {
    page.navigateTo();
    expect(page.getLoginButton()).toEqual('Login');
  });

  it('should display heading login powerprice', () => {
    page.navigateTo();
    expect(page.getHeadingText()).toEqual('Login PowerPrice');
  });


  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});

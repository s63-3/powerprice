import { browser, by, element } from 'protractor';

export class LoginPage {
  navigateTo(): Promise<unknown> {
    return browser.get(browser.baseUrl) as Promise<unknown>;
  }

  getHeadingText() {
    // Get the home page heading element reference
    return element(by.css('app-login .header-text')).getText();
  }

  getLoginButton() {
    return element(by.css('app-login .login-div')).getText();
  }
}

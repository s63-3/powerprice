module.exports = function (config) {
  config.set({
    mutate: [
      'src/**/*.ts',
      '!src/**/*.spec.ts',
      '!src/test.ts',
      '!src/environments/*.ts'
    ],
    mutator: 'typescript',
    testRunner: 'karma',
    karma: {
      configFile: 'karma.conf.js',
      projectType: 'angular-cli',
      config: {
        browsers: ['HeadlessChromeNoSandbox']
      }
    },
    reporters: ['progress', 'clear-text', 'html'],
    maxConcurrentTestRunners: 1, // Gitlab shared runners have 1 CPU core
    coverageAnalysis: 'off'
  });
};
import { KeycloakConfig } from 'keycloak-angular';

const keycloakConfig: KeycloakConfig = {
  url: 'http://keycloak:8080/auth/',
  realm: 'powerprice',
  clientId: 'webapp',
};
export const environment = {
  production: false,
  keycloakConfig,
  gatewayUrl: 'http://localhost:7700',
  baseUrl: 'http://localhost:4200'
};


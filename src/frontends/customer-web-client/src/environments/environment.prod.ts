import { KeycloakConfig } from 'keycloak-angular';

const keycloakConfig: KeycloakConfig = {
  url: (window as any).env.keycloakUrl,
  realm: (window as any).env.keycloakRealm,
  clientId: (window as any).env.keycloakClientId,
};

export const environment = {
  production: true,
  keycloakConfig,
  gatewayUrl: (window as any).env.gatewayUrl,
  baseUrl: (window as any).env.baseUrl
};

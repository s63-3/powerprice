import { MaterialModule } from './root/modules/material.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER, Injectable } from '@angular/core';

import { AppComponent, initializer } from './root/app.component';
import { HomeComponent } from './features/home/home.component';
import { AppRoutingModule } from './root/modules/app-routing.module';
import { DashboardComponent } from './features/dashboard/dashboard.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToolbarComponent } from './shared/layout/toolbar/toolbar.component';
import { ProductionComponent } from './shared/production/production.component';
import { AuthGuard } from './core/guards/auth/auth.guard';
import { KeycloakService, KeycloakAngularModule } from 'keycloak-angular';
import { RouterModule } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';

import { TranslateLoader, TranslateModule, MissingTranslationHandler, MissingTranslationHandlerParams } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { FooterComponent } from './shared/layout/footer/footer.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { LineGraphComponent } from './shared/line-graph/line-graph.component'

import {
  WavesModule,
  ButtonsModule,
  IconsModule,
  MDBBootstrapModule,
} from 'angular-bootstrap-md';
import { UsageComponent } from './shared/usage/usage.component';
import { LanguageButtonComponent } from './shared/language-button/language-button.component';

@Injectable()
export class MyMissingTranslationHandler implements MissingTranslationHandler {

  handle(params: MissingTranslationHandlerParams): string {
    return `**MISSING KEY: ${params.key}**`;
  }
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    DashboardComponent,
    ToolbarComponent,
    FooterComponent,
    ProductionComponent,
    LineGraphComponent,
    UsageComponent,
    LanguageButtonComponent
  ],

  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MDBBootstrapModule.forRoot(),
    KeycloakAngularModule,
    MaterialModule,
    FlexLayoutModule,
    HttpClientModule,
    NgxChartsModule,
    WavesModule,
    ButtonsModule,
    IconsModule,

    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
  ],
  providers: [
    AuthGuard,
    {
      provide: APP_INITIALIZER,
      useFactory: initializer,
      multi: true,
      deps: [KeycloakService],
    },
    {
      provide: MissingTranslationHandler,
      useClass: MyMissingTranslationHandler
    }
  ],

  exports: [RouterModule],

  bootstrap: [AppComponent],
})
export class AppModule { }

// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}


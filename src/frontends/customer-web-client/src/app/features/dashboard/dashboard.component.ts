import { DashboardService } from '../../core/services/dashboard/dashboard-service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  usage = -1;
  usagePrice = -1;
  producing = -1;
  producingPrice = -1

  constructor(private dashboardService: DashboardService) { }

  async ngOnInit(): Promise<void> {
    this.usage = await this.dashboardService.getUserUsage();
    this.producing = await this.dashboardService.getUserProducing();
    this.usagePrice = await this.dashboardService.getUserUsagePrice();
    this.producingPrice = await this.dashboardService.getUserProducingPrice();
  }

}

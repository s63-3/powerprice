import { DashboardService } from '../../core/services/dashboard/dashboard-service';
import { async, ComponentFixture, TestBed, fakeAsync, tick, flushMicrotasks } from '@angular/core/testing';

import { DashboardComponent } from './dashboard.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpLoaderFactory } from 'src/app/app.module';
import { HttpClient } from '@angular/common/http';
import { By } from 'protractor';

describe('DashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;
  let dashboardService: DashboardService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
          }
        })
      ],
      declarations: [DashboardComponent],
      providers: [DashboardService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;

    dashboardService = TestBed.inject(DashboardService);
  });

  describe('#ngOnInit', () => {
    it('should set usage on init', fakeAsync(async () => {
      // Arrange
      spyOn(dashboardService, 'getUserUsage').and.resolveTo(12);
      spyOn(dashboardService, 'getUserUsagePrice').and.stub();
      spyOn(dashboardService, 'getUserProducing').and.stub();
      spyOn(dashboardService, 'getUserProducingPrice').and.stub();

      // Act
      fixture.detectChanges();
      tick();

      // Assert
      expect(component.usage).toBe(12);
    }));

    it('should set usage price on init', fakeAsync(async () => {
      // Arrange
      spyOn(dashboardService, 'getUserUsage').and.stub();
      spyOn(dashboardService, 'getUserUsagePrice').and.resolveTo(18);
      spyOn(dashboardService, 'getUserProducing').and.stub();
      spyOn(dashboardService, 'getUserProducingPrice').and.stub();

      // Act
      fixture.detectChanges();
      tick();

      // Assert
      expect(component.usagePrice).toBe(18);
    }));

    it('should set producing on init', fakeAsync(async () => {
      // Arrange
      spyOn(dashboardService, 'getUserUsage').and.stub();
      spyOn(dashboardService, 'getUserUsagePrice').and.stub();
      spyOn(dashboardService, 'getUserProducing').and.resolveTo(5);
      spyOn(dashboardService, 'getUserProducingPrice').and.stub();

      // Act
      fixture.detectChanges();
      tick();

      // Assert
      expect(component.producing).toBe(5);
    }));

    it('should set producing price on init', fakeAsync(async () => {
      // Arrange
      spyOn(dashboardService, 'getUserUsage').and.stub();
      spyOn(dashboardService, 'getUserUsagePrice').and.stub();
      spyOn(dashboardService, 'getUserProducing').and.stub();
      spyOn(dashboardService, 'getUserProducingPrice').and.resolveTo(45);
      // Act
      fixture.detectChanges();
      tick();

      // Assert
      expect(component.producingPrice).toBe(45);
    }));
  });
});

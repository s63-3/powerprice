import { MaterialModule } from './../../root/modules/material.module';
import { KeycloakService } from 'keycloak-angular';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeComponent } from './home.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpLoaderFactory } from 'src/app/app.module';
import {HttpClient} from '@angular/common/http';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let keycloakService: KeycloakService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        MaterialModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
          }
        })
      ],
      declarations: [ HomeComponent ],
      providers: [ KeycloakService ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    keycloakService = TestBed.inject(KeycloakService);
  });

  describe('#login', () => {
    it('should call keycloak login', () => {
      // Arrange
      spyOn(keycloakService, 'login').and.stub();

      // Act
      component.login();

      // Assert
      expect(keycloakService.login).toHaveBeenCalledTimes(1);
    });
  });
});

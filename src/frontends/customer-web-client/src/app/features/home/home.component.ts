import { Router } from '@angular/router';
import { KeycloakService } from 'keycloak-angular';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  isLoggedIn = false;

  constructor(private keycloak: KeycloakService) { }

  async ngOnInit() {
    this.isLoggedIn = await this.keycloak.isLoggedIn();

  }

  login() {
    this.keycloak.login();
  }


}


import { AppComponent, initializer } from './app.component';
import { ComponentFixture, async, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpLoaderFactory } from '../app.module';
import { HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { KeycloakService } from 'keycloak-angular';
import { environment } from 'src/environments/environment';


describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let translateService: TranslateService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
          }
        })
      ],
      declarations: [
        AppComponent
      ],
      providers: [
        KeycloakService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;

    translateService = TestBed.inject(TranslateService);
  });

  it('should set nl as default language for TranslateService', () => {
    expect(translateService.defaultLang).toBe('nl');
  });
});

describe('initializer', () => {
  let keycloakService: KeycloakService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        KeycloakService
      ]
    });

    keycloakService = TestBed.inject(KeycloakService);
  });

  it('should throw error when keycloak init fails', fakeAsync(async () => {
    // Arrange
    spyOn(keycloakService, 'init').and.rejectWith('promise failed');

    // Act
    try {
      await initializer(keycloakService)();
      tick();

      fail('The promise should have been rejected.');
    } catch(e) {
      // Assert
      expect(e).toBe('promise failed');
    };
  }));

  it('should call keycloak init with given config', fakeAsync(async () => {
    // Arrange
    spyOn(keycloakService, 'init').and.stub();

    // Act
    try {
      await initializer(keycloakService)();
      tick();

      // Assert
      expect(keycloakService.init).toHaveBeenCalledWith({
        config: environment.keycloakConfig,
        initOptions: {
          onLoad: 'check-sso',
          checkLoginIframe: false
        },
        bearerExcludedUrls: []
      });
    } catch(e) {
      fail('Promise should have been resolved.');
    }
  }));
})
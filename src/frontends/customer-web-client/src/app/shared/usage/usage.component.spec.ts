import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { UsageComponent } from './usage.component';
import { GetTranslationService } from '../../core/services/getTranslation/get-translation-service';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MaterialModule } from '../../root/modules/material.module';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpLoaderFactory } from '../../app.module';
import { HttpClient } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DashboardService } from '../../core/services/dashboard/dashboard-service';
import { Usage } from 'src/app/core/models/usage';

describe('UsageComponent', () => {
  let component: UsageComponent;
  let fixture: ComponentFixture<UsageComponent>;
  let dashboardService: DashboardService;
  let getTranslationService: GetTranslationService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        MaterialModule,
        BrowserAnimationsModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
          }
        })
      ],
      declarations: [ UsageComponent ],
      providers: [GetTranslationService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsageComponent);
    component = fixture.componentInstance;

    dashboardService = TestBed.inject(DashboardService);
    getTranslationService = TestBed.inject(GetTranslationService);
  });

  describe('#ngOnInit', () => {
    it('should set xAxisLabel based on translation service values', fakeAsync(async () => {
      // Arrange
      spyOn(getTranslationService, 'getTranslation')
        .withArgs('dashboard', 'usage-graph-xLabel').and.resolveTo('x-label')
        .and.stub();

      // Act
      fixture.detectChanges();
      tick();

      // Assert
      expect(component.xAxisLabel).toBe('x-label');
    }));

    it('should set yAxisLabel based on translation service values', fakeAsync(async () => {
      // Arrange
      spyOn(getTranslationService, 'getTranslation')
        .withArgs('dashboard', 'usage-graph-yLabel').and.resolveTo('y-label')
        .and.stub();

      // Act
      fixture.detectChanges();
      tick();

      // Assert
      expect(component.yAxisLabel).toBe('y-label');
    }));

    it('should get rawUsageDetails through DashboardService and set usageDetails for display', fakeAsync(() => {
      // Arrange
      spyOn(getTranslationService, 'getTranslation')
        .withArgs('dashboard', 'usage-graph-name').and.resolveTo('graph-name')
        .and.stub();

      const usageData = [
        { id: 1, userId: 1, used: 1000, time: 500 } as Usage,
        { id: 2, userId: 2, used: 600, time: 1000 } as Usage
      ];
      spyOn(dashboardService, 'getUserUsageDetails').and.resolveTo(usageData);

      // Act
      fixture.detectChanges();
      tick();

      // Assert
      expect(component.usageDetails.length).toBe(1);
      expect(component.usageDetails[0].series.length).toBe(2);

      expect(component.usageDetails[0].name).toBe('graph-name');
      expect(component.usageDetails[0].series[0].value).toBe(1000);
      expect(component.usageDetails[0].series[1].name.getDay()).toEqual(new Date(1000).getDay());
    }));
  });

  describe('#onMinDateChange', () => {
    it('should include all usage details when all usage data is within minimum date range', fakeAsync(async () => {
      // Arrange
      const dateChangedEvent = { value: new Date(600_000) } as any;
      const usageData = [
        { id: 1, userId: 1, used: 1000, time: 100_000 } as Usage,
        { id: 2, userId: 2, used: 600, time: 2_000_000 } as Usage
      ];
      spyOn(dashboardService, 'getUserUsageDetails').and.resolveTo(usageData);

      fixture.detectChanges();
      tick();

      // Act
      await component.onMinDateChange(dateChangedEvent);
      tick();

      // Assert
      expect(component.usageDetails[0].series.length).toBe(2);
    }));

    it('should exclude usage detail that is outside of the minimum date range', fakeAsync(async () => {
      // Arrange
      const date = new Date(0);
      date.setUTCSeconds(1_000_000)
      date.setHours(0, 0);
      const dateChangedEvent = { value: date } as any;
      const usageData = [
        { id: 1, userId: 1, used: 1000, time: 100_000 } as Usage,
        { id: 2, userId: 2, used: 600, time: 2_000_000 } as Usage
      ];
      spyOn(dashboardService, 'getUserUsageDetails').and.resolveTo(usageData);

      fixture.detectChanges();
      tick();

      // Act
      await component.onMinDateChange(dateChangedEvent);
      tick();

      // Assert
      expect(component.usageDetails[0].series.length).toBe(1);
      expect(component.usageDetails[0].series[0].value).toBe(600);
    }));

    it('should exclude all usage details when none of the usage items are within minimum date range', fakeAsync(async () => {
      // Arrange
      const date = new Date(0);
      date.setUTCSeconds(300_000);
      date.setHours(0, 0);
      const dateChangedEvent = { value: date } as any;
      const usageData = [
        { id: 1, userId: 1, used: 1000, time: 100_000 } as Usage,
        { id: 2, userId: 2, used: 600, time: 200_000 } as Usage
      ];
      spyOn(dashboardService, 'getUserUsageDetails').and.resolveTo(usageData);

      fixture.detectChanges();
      tick();

      // Act
      await component.onMinDateChange(dateChangedEvent);
      tick();

      // Assert
      expect(component.usageDetails[0].series.length).toBe(0);
    }));
  });

  describe('#onMaxDateChange', () => {
    it('should include all usage details when all usage data is within maximum date range', fakeAsync(async () => {
      // Arrange
      const date = new Date(2_000_000_000);
      const dateChangedEvent = { value: date } as any;
      const usageData = [
        { id: 1, userId: 1, used: 1000, time: 100_000 } as Usage,
        { id: 2, userId: 2, used: 600, time: 2_000_000 } as Usage
      ];
      spyOn(dashboardService, 'getUserUsageDetails').and.resolveTo(usageData);

      fixture.detectChanges();
      tick();

      // Act
      await component.onMaxDateChange(dateChangedEvent);
      tick();

      // Assert
      console.log(component.rawUsageDetails);
      console.log(component.usageDetails);
      expect(component.usageDetails[0].series.length).toBe(2);
    }));

    it('should exclude usage detail that is outside of the maximum date range', fakeAsync(async () => {
      // Arrange
      const dateChangedEvent = { value: new Date(100_000_000) } as any;
      const usageData = [
        { id: 1, userId: 1, used: 1000, time: 100_000 } as Usage,
        { id: 2, userId: 2, used: 600, time: 2_000_000 } as Usage
      ];
      spyOn(dashboardService, 'getUserUsageDetails').and.resolveTo(usageData);

      fixture.detectChanges();
      tick();

      // Act
      await component.onMaxDateChange(dateChangedEvent);
      tick();

      // Assert
      expect(component.usageDetails[0].series.length).toBe(1);
      expect(component.usageDetails[0].series[0].value).toBe(1000);
    }));

    it('should exclude all usage details when none of the usage items are within maximum date range', fakeAsync(async () => {
      // Arrange
      const dateChangedEvent = { value: new Date(99_999) } as any;
      const usageData = [
        { id: 1, userId: 1, used: 1000, time: 100_000 } as Usage,
        { id: 2, userId: 2, used: 600, time: 2_000_000 } as Usage
      ];
      spyOn(dashboardService, 'getUserUsageDetails').and.resolveTo(usageData);

      fixture.detectChanges();
      tick();

      // Act
      await component.onMaxDateChange(dateChangedEvent);
      tick();

      // Assert
      expect(component.usageDetails[0].series.length).toBe(0);
    }));
  });
});

import { Component, OnInit } from '@angular/core';
import { GetTranslationService } from 'src/app/core/services/getTranslation/get-translation-service';
import { DashboardService } from 'src/app/core/services/dashboard/dashboard-service';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { Usage } from 'src/app/core/models/usage';

@Component({
  selector: 'app-usage',
  templateUrl: './usage.component.html',
  styleUrls: ['./usage.component.css']
})
export class UsageComponent implements OnInit {

  rawUsageDetails: Usage[] = [];

  xAxisLabel = '';
  yAxisLabel = '';

  usageDetails = [];

  minimumUsageDate = new Date();
  maximumUsageDate = new Date();

  minimumUsagePickerValue = new Date();
  maximumUsagePickerValue = new Date();

  constructor(private getTranslationService: GetTranslationService, private dashboardService: DashboardService) { }

  async ngOnInit(): Promise<void> {
    this.xAxisLabel = await this.getTranslationService.getTranslation('dashboard', 'usage-graph-xLabel');
    this.yAxisLabel = await this.getTranslationService.getTranslation('dashboard', 'usage-graph-yLabel');
    this.rawUsageDetails = await this.dashboardService.getUserUsageDetails();
    this.usageDetails = await this.convertUsageDetails(this.rawUsageDetails);
  }

  public async onMinDateChange(event: MatDatepickerInputEvent<Date>) {
    this.minimumUsagePickerValue = event.value;
    this.usageDetails = await this.convertUsageDetails(this.rawUsageDetails,
      this.minimumUsagePickerValue, this.maximumUsagePickerValue);
  }

  public async onMaxDateChange(event: MatDatepickerInputEvent<Date>) {
    this.maximumUsagePickerValue = event.value;
    this.usageDetails = await this.convertUsageDetails(this.rawUsageDetails,
      this.minimumUsagePickerValue, this.maximumUsagePickerValue);
  }


  private setDateVariables(usages: any[]) {
    let tempMinimumDate: Date;
    let tempMaximumDate: Date;
    for (const usage of usages) {
      const date = usage.name
      // date.setUTCMilliseconds(usage.time * 10);
      if (!tempMinimumDate) {
        tempMinimumDate = date;
      }
      if (!tempMaximumDate) {
        tempMaximumDate = date;
      }
      if (date < tempMinimumDate) {
        tempMinimumDate = date;
      } else if (date > tempMaximumDate) {
        tempMaximumDate = date;
      }
    }
    // the minimumUsageDate and maxUsageDate cannot be changed by the datepicker, the pickervalue variables can be changed
    this.minimumUsageDate = tempMinimumDate;
    this.minimumUsagePickerValue = tempMinimumDate;
    this.maximumUsageDate = tempMaximumDate;
    this.maximumUsagePickerValue = tempMaximumDate;
  }

  private async convertUsageDetails(data: Usage[], minimumDate?: Date, maximumDate?: Date): Promise<any[]> {
    const houseUsage: any[] = [];
    if (minimumDate && maximumDate) {
      minimumDate.setHours(0, 0);
      maximumDate.setHours(23, 59);
    }
    for (const usage of data) {
      const dateOfUsage = new Date(0);
      dateOfUsage.setUTCSeconds(usage.time);
      if (minimumDate && maximumDate) {

        if (dateOfUsage >= minimumDate && dateOfUsage <= maximumDate) {
          houseUsage.push({
            value: usage.used,
            name: dateOfUsage
          });
        }
      } else {
        houseUsage.push({
          value: usage.used,
          name: dateOfUsage
        });
      }
    }
    if (!minimumDate && !maximumDate) {
      this.setDateVariables(houseUsage);
    }
    // Translation cannot be dynamically loaded with html, so we use a translation service
    return [{
      name: await this.getTranslationService.getTranslation('dashboard', 'usage-graph-name'),
      series: houseUsage
    }];
  }
}


import { KeycloakService } from 'keycloak-angular';
import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { ToolbarComponent } from './toolbar.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpLoaderFactory } from 'src/app/app.module';
import { HttpClient } from '@angular/common/http';

describe('ToolbarComponent', () => {
  let component: ToolbarComponent;
  let fixture: ComponentFixture<ToolbarComponent>;
  let keycloakService: KeycloakService;


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
          }
        })
      ],
      declarations: [
        ToolbarComponent
      ],
      providers: [
        KeycloakService
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToolbarComponent);
    component = fixture.componentInstance;

    keycloakService = TestBed.inject(KeycloakService);
  });

  it('should not be logged in by default', () => {
    expect(component.isLoggedIn).toBe(false);
  });

  it('should have XXX as default username', () => {
    expect(component.username).toBe('XXX');
  });

  describe('#ngOnInit', () => {
    it('should get username from keycloak', fakeAsync(async () => {
      // Arrange
      spyOn(keycloakService, 'isLoggedIn').and.resolveTo(true);
      spyOn(keycloakService, 'getUsername').and.returnValue('John Doe');

      // Act
      fixture.detectChanges();
      tick();

      // Assert
      expect(component.username).toBe('John Doe');
    }));

    it('should set user to logged in when user is logged in', fakeAsync(async () => {
      // Arrange
      spyOn(keycloakService, 'isLoggedIn').and.resolveTo(true);
      spyOn(keycloakService, 'getUsername').and.returnValue('John Doe');

      // Act
      fixture.detectChanges();
      tick();

      // Assert
      expect(component.isLoggedIn).toBe(true);
    }));
  });

  describe('#login', () => {
    it('should call keycloak login', fakeAsync(async () => {
      // Arrange
      spyOn(keycloakService, 'login').and.stub();

      // Act
      component.login();
      tick();

      // Assert
      expect(keycloakService.login).toHaveBeenCalledTimes(1);
    }));
  });

  describe('#logout', () => {
    it('should call keycloak logout', fakeAsync(async () => {
      // Arrange
      spyOn(keycloakService, 'logout').and.stub();

      // Act
      component.logOut();
      tick();

      // Assert
      expect(keycloakService.logout).toHaveBeenCalledTimes(1);
    }));
  });
});

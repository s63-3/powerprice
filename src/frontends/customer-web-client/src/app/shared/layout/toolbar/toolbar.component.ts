import { Component, OnInit } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
})
export class ToolbarComponent implements OnInit {
  isLoggedIn = false;
  username = 'XXX';

  constructor(
    private keycloak: KeycloakService,
    iconRegistry: MatIconRegistry,
    sanitizer: DomSanitizer
  ) {
    iconRegistry.addSvgIcon(
      'nl',
      sanitizer.bypassSecurityTrustResourceUrl('assets/nl.svg')
    );

    iconRegistry.addSvgIcon(
      'en',
      sanitizer.bypassSecurityTrustResourceUrl('assets/gb.svg')
    );
  }

  async ngOnInit(): Promise<void> {
    await this.setUsername();
  }

  private async setUsername() {
    this.isLoggedIn = await this.keycloak.isLoggedIn();

    if (this.isLoggedIn) {
      this.username = this.keycloak.getUsername();
    }
  }

  login() {
    this.keycloak.login();
  }

  logOut() {
    this.keycloak.logout();

  }
}

import { Component, OnInit, EventEmitter } from '@angular/core';
import { GetTranslationService } from 'src/app/core/services/getTranslation/get-translation-service';
import { ProductionSource } from 'src/app/core/models/energy-source'
import { DashboardService } from 'src/app/core/services/dashboard/dashboard-service';
import { Production } from 'src/app/core/models/production';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';

@Component({
  selector: 'app-production',
  templateUrl: './production.component.html',
  styleUrls: ['./production.component.css']
})
export class ProductionComponent implements OnInit {

  private rawProductionDetails: Production[] = [];

  xAxisLabel = '';
  yAxisLabel = '';

  productionDetails = [];

  minimumProductionDate = new Date();
  maximumProductionDate = new Date();

  minimumProductionPickerValue = new Date();
  maximumProductionPickerValue = new Date();

  constructor(private getTranslationService: GetTranslationService, private dashboardService: DashboardService) { }

  async ngOnInit(): Promise<void> {
    this.xAxisLabel = await this.getTranslationService.getTranslation('dashboard', 'production-graph-xLabel');
    this.yAxisLabel = await this.getTranslationService.getTranslation('dashboard', 'production-graph-yLabel');
    this.rawProductionDetails = await this.dashboardService.getUserProductionDetails();
    this.convertRawProduction(this.rawProductionDetails);
    this.productionDetails = await this.convertProductionDetails(this.rawProductionDetails);
  }

  public async onMinDateChange(event: MatDatepickerInputEvent<Date>) {
    this.minimumProductionPickerValue = event.value;
    this.productionDetails = await this.convertProductionDetails(this.rawProductionDetails,
      this.minimumProductionPickerValue, this.maximumProductionPickerValue);
  }
  public async onMaxDateChange(event: MatDatepickerInputEvent<Date>) {
    this.maximumProductionPickerValue = event.value;
    this.productionDetails = await this.convertProductionDetails(this.rawProductionDetails,
      this.minimumProductionPickerValue, this.maximumProductionPickerValue);
  }

  private convertRawProduction(productions: Production[]) {
    const production: Production[] = [];
    const timestamps: number[] = [...new Set(productions.map(obj => obj.time))];
    for (const timestamp of timestamps) {
      const produced = this.getProductionsFromTimeStampAndSource(productions, timestamp);
      for (const newProduction of produced) {
        production.push(newProduction);
      }
    }
    this.rawProductionDetails = production;
  }

  private getProductionsFromTimeStampAndSource(productionArray: Production[], timestamp: number): Production[] {
    const productions: Production[] = [];
    let solarProduced = 0;
    let windmillProduced = 0;
    let foundSolarProduction = false;
    let foundWindmillProduction = false;
    for (const production of productionArray) {
      if (production.time === timestamp && production.source === ProductionSource.SOLAR_PANEL) {
        solarProduced += production.produced
        foundSolarProduction = true;
      } else if (production.time === timestamp && production.source === ProductionSource.WINDMILL) {
        windmillProduced += production.produced;
        foundWindmillProduction = true;
      }
    }
    const newProductions: Production[] = [];
    if (foundWindmillProduction) {
      const windMillProduction = new Production();
      windMillProduction.time = timestamp;
      windMillProduction.source = ProductionSource.WINDMILL;
      windMillProduction.produced = windmillProduced;
      newProductions.push(windMillProduction);
    }
    if (foundSolarProduction) {
      const solarPanelProduction = new Production();
      solarPanelProduction.time = timestamp;
      solarPanelProduction.source = ProductionSource.SOLAR_PANEL;
      solarPanelProduction.produced = solarProduced;
      newProductions.push(solarPanelProduction);
    }

    return newProductions;
  }

  // Sets the maximum and minimum dates based on the usage objects
  private setDateVariables(productions: any[]) {
    let tempMinimumDate: Date;
    let tempMaximumDate: Date;
    for (const production of productions) {
      const date = production.name;
      // date.setUTCMilliseconds(production.time);
      if (!tempMinimumDate) {
        tempMinimumDate = date;
      }
      if (!tempMaximumDate) {
        tempMaximumDate = date;
      }
      if (date < tempMinimumDate) {
        tempMinimumDate = date;
      } else if (date > tempMaximumDate) {
        tempMaximumDate = date;
      }
    }
    // the minimumProductionDate and maxProductionDate cannot be changed by the datepicker, the pickervalue  variables can be changed
    this.minimumProductionDate = tempMinimumDate;
    this.minimumProductionPickerValue = tempMinimumDate;
    this.maximumProductionDate = tempMaximumDate;
    this.maximumProductionPickerValue = tempMaximumDate;
  }

  private async convertProductionDetails(data: Production[], minimumDate?: Date, maximumDate?: Date): Promise<any[]> {
    const windmillProduction: any[] = [];
    const solarPanelProduction: any[] = [];
    if (minimumDate && maximumDate) {
      minimumDate.setHours(0, 0);
      maximumDate.setHours(23, 59);
    }
    for (const production of data) {
      const dateOfProduction = new Date(0);
      dateOfProduction.setUTCSeconds(production.time);
      // dateOfProduction.setUTCMilliseconds(production.time);
      if (minimumDate && maximumDate) {
        if (dateOfProduction >= minimumDate && dateOfProduction <= maximumDate) {
          this.addProductionToArray(production, dateOfProduction, solarPanelProduction, windmillProduction);

        }
      } else {
        this.addProductionToArray(production, dateOfProduction, solarPanelProduction, windmillProduction);
      }
      if (!minimumDate && !maximumDate) {
        this.setDateVariables(windmillProduction);
      }
    }
    // Translation cannot be dynamically loaded with html, so we use a translation service
    return [{
      name: await this.getTranslationService.getTranslation('dashboard', 'production-windmill'),
      series: windmillProduction
    }, {
      name: await this.getTranslationService.getTranslation('dashboard', 'production-solarpanel'),
      series: solarPanelProduction
    }];
  }

  private addProductionToArray(production: Production, date: Date, solarPanelArray: any[], windmillArray: any[]) {
    const productionSource = production.source;
    if (productionSource === ProductionSource.WINDMILL) {
      const windmillObject = {
        value: production.produced,
        name: date
      }
      windmillArray.push(windmillObject);
    } else if (productionSource === ProductionSource.SOLAR_PANEL) {
      const solarPanelObject = {
        value: production.produced,
        name: date
      }
      solarPanelArray.push(solarPanelObject);
    }
  }
}





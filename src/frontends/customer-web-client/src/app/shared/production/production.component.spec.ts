import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { ProductionComponent } from './production.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MaterialModule } from '../../root/modules/material.module';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpLoaderFactory } from '../../app.module';
import { HttpClient } from '@angular/common/http';
import { GetTranslationService } from '../../core/services/getTranslation/get-translation-service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DashboardService } from '../../core/services/dashboard/dashboard-service';
import { Production } from 'src/app/core/models/production';
import { ProductionSource } from 'src/app/core/models/energy-source';

describe('ProductionComponent', () => {
  let component: ProductionComponent;
  let fixture: ComponentFixture<ProductionComponent>;
  let dashboardService: DashboardService;
  let getTranslationService: GetTranslationService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        MaterialModule,
        BrowserAnimationsModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
          }
        })
      ],
      declarations: [ ProductionComponent ],
      providers: [GetTranslationService, DashboardService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductionComponent);
    component = fixture.componentInstance;
    dashboardService = TestBed.inject(DashboardService);
    getTranslationService = TestBed.inject(GetTranslationService);
  });

  describe('#ngOnInit', () => {
    it('should set xAxisLabel value based on getTranslationService values', fakeAsync(async () => {
      // Arrange
      spyOn(getTranslationService, 'getTranslation')
        .withArgs('dashboard', 'production-graph-xLabel').and.resolveTo('x-prod-label')
        .and.stub();

      // Act
      fixture.detectChanges();
      tick();

      // Assert
      expect(component.xAxisLabel).toBe('x-prod-label');
    }));

    it('should set yAxisLabel value based on getTranslationService values', fakeAsync(async () => {
      // Arrange
      spyOn(getTranslationService, 'getTranslation')
        .withArgs('dashboard', 'production-graph-yLabel').and.resolveTo('y-prod-label')
        .and.stub();

      // Act
      fixture.detectChanges();
      tick();

      // Assert
      expect(component.yAxisLabel).toBe('y-prod-label');
    }));

    it('should get rawProductionDetails through DashboardService and set productionDetails for display', fakeAsync(async () => {
      // Arrange
      spyOn(getTranslationService, 'getTranslation')
        .withArgs('dashboard', 'production-windmill').and.resolveTo('windmill-metric')
        .withArgs('dashboard', 'production-solarpanel').and.resolveTo('solar-panel-metric')
        .and.stub();

      const productionData = [
        { id: 1, userId: 1, produced: 800, time: 500, source: ProductionSource.SOLAR_PANEL } as Production,
        { id: 2, userId: 2, produced: 900, time: 600, source: ProductionSource.WINDMILL } as Production
      ];
      spyOn(dashboardService, 'getUserProductionDetails').and.resolveTo(productionData);

      // Act
      fixture.detectChanges();
      tick();

      // Assert
      const windmillProductionDetails = component.productionDetails[0];
      const solarPanelProductionDetails = component.productionDetails[1];

      // Check both windmill and solar metrics exist within the productionDetails array
      expect(component.productionDetails.length).toBe(2);
      expect(windmillProductionDetails.name).toBe('windmill-metric');
      expect(solarPanelProductionDetails.name).toBe('solar-panel-metric');

      // Check that both metrics have 1 item as the test data included 1 SOLAR_PANEL item and 1 WINDMILL item
      expect(windmillProductionDetails.series.length).toBe(1);
      expect(solarPanelProductionDetails.series.length).toBe(1);

      expect(windmillProductionDetails.series[0].value).toBe(900);
      expect(solarPanelProductionDetails.series[0].name.getDay()).toEqual(new Date(500).getDay());
    }));
  });

  describe('#onMinDateChange', () => {
    it('should include all production details when all production data is within minimum date range', fakeAsync(async () => {
      // Arrange
      const date = new Date(0);
      date.setUTCSeconds(50_000);
      date.setHours(0, 0);
      const dateChangedEvent = { value: date } as any;
      const productionData = [
        { id: 1, userId: 1, produced: 800, time: 500_000_000, source: ProductionSource.SOLAR_PANEL } as Production,
        { id: 2, userId: 2, produced: 900, time: 500_000_000, source: ProductionSource.WINDMILL } as Production
      ];
      spyOn(dashboardService, 'getUserProductionDetails').and.resolveTo(productionData);

      fixture.detectChanges();
      tick();

      // Act
      await component.onMinDateChange(dateChangedEvent);
      tick();

      // Assert
      const windmillProductionListLength = component.productionDetails[0].series.length;
      const solarPanelProductionListLength = component.productionDetails[1].series.length;
      expect(windmillProductionListLength).toBe(1);
      expect(solarPanelProductionListLength).toBe(1);
    }));

    it('should exclude production detail that is outside of the given minimum date range', fakeAsync(async () => {
      // Arrange
      const date = new Date(0);
      date.setUTCSeconds(5_500_000);
      date.setHours(0, 0);
      const dateChangedEvent = { value: date } as any;
      const productionData = [
        { id: 1, userId: 1, produced: 800, time: 5_000_000, source: ProductionSource.SOLAR_PANEL } as Production,
        { id: 2, userId: 2, produced: 900, time: 6_000_000, source: ProductionSource.WINDMILL } as Production
      ];
      spyOn(dashboardService, 'getUserProductionDetails').and.resolveTo(productionData);

      fixture.detectChanges();
      tick();

      // Act
      await component.onMinDateChange(dateChangedEvent);
      tick();

      // Assert
      const windmillProductionListLength = component.productionDetails[0].series.length;
      const solarPanelProductionListLength = component.productionDetails[1].series.length;
      expect(windmillProductionListLength).toBe(1);
      expect(solarPanelProductionListLength).toBe(0);
    }));

    it('should exclude all production details when none of the prodcution items are within the minimum date range', fakeAsync(async () => {
      // Arrange
      const date = new Date(0);
      date.setUTCSeconds(6_500_000);
      date.setHours(0, 0);
      const dateChangedEvent = { value: date } as any;
      const productionData = [
        { id: 1, userId: 1, produced: 800, time: 5_000_000, source: ProductionSource.SOLAR_PANEL } as Production,
        { id: 2, userId: 2, produced: 900, time: 6_000_000, source: ProductionSource.WINDMILL } as Production
      ];
      spyOn(dashboardService, 'getUserProductionDetails').and.resolveTo(productionData);

      fixture.detectChanges();
      tick();

      // Act
      await component.onMinDateChange(dateChangedEvent);
      tick();

      // Assert
      const windmillProductionListLength = component.productionDetails[0].series.length;
      const solarPanelProductionListLength = component.productionDetails[1].series.length;
      expect(windmillProductionListLength).toBe(0);
      expect(solarPanelProductionListLength).toBe(0);
    }));
  });

  describe('#onMaxDateChange', () => {
    it('should include all production details when all production data is within maximum date range', fakeAsync(async () => {
      // Arrange
      const date = new Date(0);
      date.setUTCSeconds(3_000_000);
      date.setHours(23, 59);
      const dateChangedEvent = { value: date } as any;
      const productionData = [
        { id: 1, userId: 1, produced: 800, time: 2_000_000, source: ProductionSource.SOLAR_PANEL } as Production,
        { id: 2, userId: 2, produced: 900, time: 1_000_000, source: ProductionSource.WINDMILL } as Production
      ];
      spyOn(dashboardService, 'getUserProductionDetails').and.resolveTo(productionData);

      fixture.detectChanges();
      tick();

      // Act
      await component.onMaxDateChange(dateChangedEvent);
      tick();

      // Assert
      const windmillProductionListLength = component.productionDetails[0].series.length;
      const solarPanelProductionListLength = component.productionDetails[1].series.length;
      console.log(component.productionDetails);
      expect(windmillProductionListLength).toBe(1);
      expect(solarPanelProductionListLength).toBe(1);
    }));

    it('should exclude production detail that is outside of the maximum date range', fakeAsync(async () => {
      // Arrange
      const date = new Date(0);
      date.setUTCSeconds(1_500_000);
      date.setHours(23, 59)
      const dateChangedEvent = { value: date } as any;
      const productionData = [
        { id: 1, userId: 1, produced: 800, time: 2_000_000, source: ProductionSource.SOLAR_PANEL } as Production,
        { id: 2, userId: 2, produced: 900, time: 1_000_000, source: ProductionSource.WINDMILL } as Production
      ];
      spyOn(dashboardService, 'getUserProductionDetails').and.resolveTo(productionData);

      fixture.detectChanges();
      tick();

      // Act
      await component.onMaxDateChange(dateChangedEvent);
      tick();

      // Assert
      const windmillProductionListLength = component.productionDetails[0].series.length;
      const solarPanelProductionListLength = component.productionDetails[1].series.length;
      expect(windmillProductionListLength).toBe(1);
      expect(solarPanelProductionListLength).toBe(0);
    }));

    it('should exclude all production details when none of the production items are within maximum date range', fakeAsync(async () => {
      // Arrange
      const dateChangedEvent = { value: new Date(99_999) } as any;
      const productionData = [
        { id: 1, userId: 1, produced: 800, time: 200_000, source: ProductionSource.SOLAR_PANEL } as Production,
        { id: 2, userId: 2, produced: 900, time: 100_000, source: ProductionSource.WINDMILL } as Production
      ];
      spyOn(dashboardService, 'getUserProductionDetails').and.resolveTo(productionData);

      fixture.detectChanges();
      tick();

      // Act
      await component.onMaxDateChange(dateChangedEvent);
      tick();

      // Assert
      const windmillProductionListLength = component.productionDetails[0].series.length;
      const solarPanelProductionListLength = component.productionDetails[1].series.length;
      expect(windmillProductionListLength).toBe(0);
      expect(solarPanelProductionListLength).toBe(0);
    }));
  });
});

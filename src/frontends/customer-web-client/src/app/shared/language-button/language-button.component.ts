import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-language-button',
  templateUrl: './language-button.component.html',
  styleUrls: ['./language-button.component.scss']
})
export class LanguageButtonComponent implements OnInit {

  currentOption = 'nl';

  constructor(
    private translateService: TranslateService,
    private iconRegistry: MatIconRegistry,
    private sanitizer: DomSanitizer
  ) {
    this.iconRegistry.addSvgIcon(
      'nl',
      this.sanitizer.bypassSecurityTrustResourceUrl('assets/nl.svg')
    );

    this.iconRegistry.addSvgIcon(
      'en',
      this.sanitizer.bypassSecurityTrustResourceUrl('assets/gb.svg')
    );
  }

  async ngOnInit(): Promise<void> {
    const language = localStorage.getItem('language');
    if (language) {
      this.currentOption = language;
      this.translateService.use(language);
    } else {
      this.currentOption = 'nl';
    }
  }

  changeLanguage(language: string) {
    this.currentOption = language;
    localStorage.setItem('language', language);
    this.translateService.use(language);
  }
}

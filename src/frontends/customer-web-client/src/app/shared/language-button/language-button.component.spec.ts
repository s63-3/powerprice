import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { LanguageButtonComponent } from './language-button.component';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { HttpLoaderFactory } from 'src/app/app.module';
import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('LanguageButtonComponent', () => {
  let component: LanguageButtonComponent;
  let fixture: ComponentFixture<LanguageButtonComponent>;
  let translateService: TranslateService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
          }
        })
      ],
      declarations: [ LanguageButtonComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LanguageButtonComponent);
    component = fixture.componentInstance;

    translateService = TestBed.inject(TranslateService);
  });

  it('should have nl as default language', () => {
    expect(component.currentOption).toBe('nl');
  });

  describe('#ngOnInit', () => {
    it('should base current language on localstorage', () => {
      // Arrange
      spyOn(localStorage, 'getItem').and.returnValue('en-gb');

      // Act
      fixture.detectChanges();

      // Assert
      expect(component.currentOption).toBe('en-gb');
    });

    it('should have nl as default language when localstorage has undefined language', () => {
      // Arrange
      spyOn(localStorage, 'getItem').and.returnValue(undefined);

      // Act
      fixture.detectChanges();

      // Assert
      expect(component.currentOption).toBe('nl');
    });

    it('should set TranslateService to current language based on localstorage', () => {
      // Arrange
      spyOn(localStorage, 'getItem').and.returnValue('en-gb');

      // Act
      fixture.detectChanges();

      // Assert
      expect(translateService.currentLang).toBe('en-gb');
    });
  });

  describe('#changeLanguage', () => {
    it('should set current language of component to given string', () => {
      // Arrange
      spyOn(localStorage, 'setItem').and.stub();

      // Act
      component.changeLanguage('en-gb');

      // Assert
      expect(component.currentOption).toBe('en-gb');
    });

    it('should set localstorage for given language', () => {
      // Arrange
      spyOn(localStorage, 'setItem').and.stub();

      // Act
      component.changeLanguage('en-gb');

      // Assert
      expect(localStorage.setItem).toHaveBeenCalledWith('language', 'en-gb');
    });

    it('should set TranslateService language to given language', () => {
      // Arrange
      spyOn(localStorage, 'setItem').and.stub();

      // Act
      component.changeLanguage('en-gb');

      // Assert
      expect(translateService.currentLang).toBe('en-gb');
    });
  });
});

export class Usage {
  id: number;
  userId: number;
  used: number;
  time: number;
  links: any[];
}
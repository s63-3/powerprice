import { ProductionSource } from './energy-source';

export class Production {
  id: number;
  source: ProductionSource;
  userId: number;
  produced: number;
  time: number;
  links: any[];
}

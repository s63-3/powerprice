import { Injectable } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class GetTranslationService {
  constructor(private translate: TranslateService) {}

  async getTranslation(component: string, key: string) {
    return await this.translate.get(component + '.' + key).toPromise();
  }
}
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Production } from '../../models/production';
import { Usage } from '../../models/usage';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  private baseUrl = environment.gatewayUrl;
  constructor(private http: HttpClient) { }

  getUserUsage(): Promise<number> {
    const endPoint = this.baseUrl + '/grid/customers/usage';
    return this.http.get<number>(endPoint).toPromise();
  }

  getUserUsagePrice(): Promise<number> {
    const endPoint = this.baseUrl + '/grid/customers/usage/price';
    return this.http.get<number>(endPoint).toPromise();
  }

  getUserProducing(): Promise<number> {
    const endPoint = this.baseUrl + '/grid/customers/production';
    return this.http.get<number>(endPoint).toPromise();
  }

  getUserProducingPrice(): Promise<number> {
    const endPoint = this.baseUrl + '/grid/customers/production/price';
    return this.http.get<number>(endPoint).toPromise();
  }

  getUserUsageDetails(): Promise<Usage[]> {
    const endPoint = this.baseUrl + '/grid/customers/usage-details';
    return this.http.get<Usage[]>(endPoint).toPromise();
  }

  getUserProductionDetails(): Promise<Production[]> {
    const endPoint = this.baseUrl + '/grid/customers/production-details';
    return this.http.get<Production[]>(endPoint).toPromise();
  }
}

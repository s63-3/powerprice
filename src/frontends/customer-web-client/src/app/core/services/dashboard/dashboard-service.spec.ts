import { DashboardService } from './dashboard-service';
import { TestBed, fakeAsync, flushMicrotasks } from '@angular/core/testing';
import {
  HttpTestingController,
  HttpClientTestingModule,
} from '@angular/common/http/testing';
import { environment } from 'src/environments/environment';
import { Production } from '../../models/production';
import { Usage } from '../../models/usage';
import { ProductionSource } from '../../models/energy-source';

describe('DashboardService', () => {
  let service: DashboardService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [DashboardService],
    });

    service = TestBed.inject(DashboardService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('#getUserUsage', () => {
    it('should send to valid endpoint and get a valid user usage', async () => {
      // Arrange
      const baseUrl = environment.gatewayUrl;
      const expectedUsage = 21;
      const getUserUsageCall = service.getUserUsage();

      const req = httpMock.expectOne(baseUrl + '/grid/customers/usage');
      req.flush(expectedUsage);

      // Act
      const actualUsage = await getUserUsageCall;

      // Assert
      httpMock.verify();
      expect(req.request.method).toBe('GET');
      expect(actualUsage).toBe(expectedUsage);
    });
  });

  describe('#getUserUsagePrice', () => {
    it('should send to valid endpoint and get a valid user usage price', async () => {
      // Arrange
      const baseUrl = environment.gatewayUrl;
      const expectedUsagePrice = 30;
      const getUserUsagePriceCall = service.getUserUsagePrice();

      const req = httpMock.expectOne(baseUrl + '/grid/customers/usage/price');
      req.flush(expectedUsagePrice);

      // Act
      const actualUserUsagePrice = await getUserUsagePriceCall;

      // Assert
      httpMock.verify();
      expect(actualUserUsagePrice).toBe(expectedUsagePrice);
      expect(req.request.method).toBe('GET');
    });
  });

  describe('#getUserProducing', () => {
    it('should send to valid endpoint and get a valid user production', async () => {
      // Arrange
      const baseUrl = environment.gatewayUrl;
      const expectedProducing = 25;
      const getUserProducingCall = service.getUserProducing();

      const req = httpMock.expectOne(baseUrl + '/grid/customers/production');
      req.flush(expectedProducing);

      // Act
      const actualProducing = await getUserProducingCall;

      // Assert
      httpMock.verify();
      expect(expectedProducing).toBe(actualProducing);
      expect(req.request.method).toBe('GET');
    });
  });

  describe('#getUserProducingPrice', () => {
    it('should send to valid endpoint and get a valid user producing price', async () => {
      // Arrange
      const baseUrl = environment.gatewayUrl;
      const expectedProducingPrice = 28;
      const getProducingPriceCall = service.getUserProducingPrice();

      const req = httpMock.expectOne(
        baseUrl + '/grid/customers/production/price'
      );
      req.flush(expectedProducingPrice);

      // Act
      const actualProducingPrice = await getProducingPriceCall;

      // Assert
      httpMock.verify();
      expect(expectedProducingPrice).toBe(actualProducingPrice);
      expect(req.request.method).toBe('GET');
    });
  });

  describe('#getUserUsageDetails', () => {
    it('should send to a valid endpoint and get valid usage data', async () => {
      // Arrange
      const baseUrl = environment.gatewayUrl;
      const expectedUsage: Usage[] = [
        { id: 1, userId: 1, used: 1000, time: 1000 } as Usage,
        { id: 2, userId: 2, used: 1000, time: 1000 } as Usage
      ];
      const getUserUsageDetailsCall = service.getUserUsageDetails();

      const req = httpMock.expectOne(
        baseUrl + '/grid/customers/usage-details'
      );
      req.flush(expectedUsage);

      // Act
      const actualUsage = await getUserUsageDetailsCall;

      // Assert
      httpMock.verify();
      expect(expectedUsage).toBe(actualUsage);
      expect(req.request.method).toBe('GET');
    });
  });

  describe('#getUserProductionDetails', () => {
    it('should send to a valid endpoint and get valid production data', async () => {
      // Arrange
      const baseUrl = environment.gatewayUrl;
      const expectedProduction: Production[] = [
        { id: 1, userId: 1, time: 1000, produced: 1000, source: ProductionSource.WINDMILL} as Production,
        { id: 2, userId: 2, time: 1000, produced: 1000, source: ProductionSource.WINDMILL} as Production
      ]
      const getUserProductionDetailsCall = service.getUserProductionDetails();

      const req = httpMock.expectOne(
        baseUrl + '/grid/customers/production-details'
      );
      req.flush(expectedProduction);

      // Act
      const actualProduction = await getUserProductionDetailsCall;

      // Assert
      httpMock.verify();
      expect(expectedProduction).toBe(actualProduction);
      expect(req.request.method).toBe('GET');
    });
  });
});

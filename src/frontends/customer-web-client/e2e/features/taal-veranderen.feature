Feature: Weergegeven taal wijzigen

  Als buitenlandse klant
  Wil ik van taal kunnen wisselen
  Zodat ik zelfs bij niet-nederlandse afkomst nog steeds de applicatie kan gebruiken

  Business rules:
  - Er kan geselecteerd worden tussen Nederlands en Engels
  - De standaardtaal is Nederlands

  Scenario: Die ene waar ik de taal naar het Engels wijzig
  
  Given de weergave staat op Nederlands ingesteld
  When ik op de English knop druk
  Then verandert de weergave naar het Engels


  Scenario: Die ene waar ik de taal naar het Nederlands wijzig

  Given de weergave staat op Engels ingesteld
  When ik op de Nederlands knop druk
  Then verandert de weergave naar het Nederlands

  Scenario: Die ene waar ik Nederlands als standaardtaal krijg

  Given ik ben nog nooit op de website geweest
  When ik naar de website navigeer
  Then is de standaardtaal Nederlands
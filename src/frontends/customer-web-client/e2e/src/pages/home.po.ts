import { browser, by, element } from 'protractor';
import { NavBar } from './navbar.po';
import { environment } from '../../../src/environments/environment.e2e';

export class HomePage {

  navBar: NavBar = new NavBar();

  navigateTo(): Promise<unknown> {
    return browser.get(environment.baseUrl) as Promise<unknown>;
  }

  getTitleText() {
    return element(by.xpath('/html/body/app-root/app-home/header/div/p')).getText();
  }
}

import { by, element, browser, protractor } from 'protractor';

export class NavBar {


  async changeToEnglishLanguage() {
    const languageButton = await this.getLanguageButton();
    await languageButton.click();

    const ec = protractor.ExpectedConditions;
    const switchToEnglishButton = element(by.xpath('/html/body/div[2]/div[2]/div/div/div/mat-option[2]/span'));
    await browser.wait(ec.visibilityOf(switchToEnglishButton), 5000, 'Switch to English button took too long to appear on the DOM.');
    await switchToEnglishButton.click();
  }

  async changeToDutchLanguage() {
    const languageButton = await this.getLanguageButton();
    await languageButton.click();

    const ec = protractor.ExpectedConditions;
    const switchToDutchButton = element(by.xpath('/html/body/div[2]/div[2]/div/div/div/mat-option[1]/span'));
    await browser.wait(ec.visibilityOf(switchToDutchButton), 5000, 'Switch to Dutch button took too long to appear on the DOM.');
    await switchToDutchButton.click();
  }

  private async getLanguageButton() {
    const ec = protractor.ExpectedConditions;

    const languageButton = element(by.xpath('/html/body/app-root/app-toolbar/mat-toolbar/app-language-button/button'));

    await browser.wait(ec.presenceOf(languageButton), 5000, 'Language button took too long to appear on the DOM.');

    return languageButton;
  }
}

import { HomePage } from '../../pages/home.po';
import { Before, Given, When, Then } from 'cucumber';
import { expect } from 'chai';

let homePage: HomePage;

Before(() => {
  homePage = new HomePage();
});


Given('de weergave staat op Engels ingesteld', async () => {
  await homePage.navigateTo();
  await homePage.navBar.changeToEnglishLanguage();
});

When('ik op de Nederlands knop druk', async () => {
  await homePage.navBar.changeToDutchLanguage();
});

Then('verandert de weergave naar het Nederlands', async () => {
  expect(await homePage.getTitleText()).to.equal('Brabant gaat voor groene energie! Jij ook?');
});

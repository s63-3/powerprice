import { HomePage } from '../../pages/home.po';
import { Before, Given, When, Then } from 'cucumber';
import { expect } from 'chai';

let homePage: HomePage;

Before(() => {
  homePage = new HomePage();
});

Given('de weergave staat op Nederlands ingesteld', async () => {
  await homePage.navigateTo();
  await homePage.navBar.changeToDutchLanguage();
});

When('ik op de English knop druk', async () => {
  await homePage.navBar.changeToEnglishLanguage();
});

Then('verandert de weergave naar het Engels', async () => {
  expect(await homePage.getTitleText()).to.equal('Your Energy at one glance');
});
import { HomePage } from '../../pages/home.po';
import { Before, Given, When, Then } from 'cucumber';
import { expect } from 'chai';

let homePage: HomePage;

Before(() => {
  homePage = new HomePage();
});


Given('ik ben nog nooit op de website geweest', async () => {
});

When('ik naar de website navigeer', async () => {
  await homePage.navigateTo();
});

Then('is de standaardtaal Nederlands', async () => {
  expect(await homePage.getTitleText()).to.equal('Brabant gaat voor groene energie! Jij ook?');
});

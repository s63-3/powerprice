package com.powerprice.apigateway.services;

import java.io.IOException;
import okhttp3.mockwebserver.MockWebServer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

class GridSimulationServiceTest {
  private MockWebServer mockWebServer;
  private GridService gridSimulationService;

  @BeforeEach
  void setUp() throws IOException {
    mockWebServer = new MockWebServer();
    mockWebServer.start();

    var baseUrl = String.format("http://localhost:%s", mockWebServer.getPort());
    UserService userService = new UserService();
    SecurityTokenService securityTokenService = new SecurityTokenService(userService);
    gridSimulationService = new GridService(securityTokenService, userService);
    gridSimulationService.setBaseUrl(baseUrl);
  }

  // @Test
  // void shouldGetGridUsage() throws JsonProcessingException,
  // InterruptedException {
  // Arrange
  // var expectedGridResponse = new Grid();
  // expectedGridResponse.setTotalEnergyProduction(10);
  // expectedGridResponse.setTotalEnergyUsage(20);
  // expectedGridResponse.setTotalCustomers(50);
  //
  // var objectMapper = new ObjectMapper();
  // mockWebServer.enqueue(new MockResponse()
  // .setBody(objectMapper.writeValueAsString(expectedGridResponse))
  // .addHeader("Content-Type", "application/json")
  // );
  //
  // // Act
  // Grid actualGridResponse = gridSimulationService.getGridUsage().block();
  // var reportedRequest = mockWebServer.takeRequest();
  //
  // // Assert
  // assertAll(
  // () -> assertEquals(10, actualGridResponse.getTotalEnergyProduction_kWh()),
  // () -> assertEquals(20, actualGridResponse.getTotalEnergyUsage_kWh()),
  // () -> assertEquals(50, actualGridResponse.getTotalCustomers()),
  // () -> assertEquals("/grid/usage", reportedRequest.getPath())
  // );
  // }

  // @Test
  // void shouldGetGridProduction() throws JsonProcessingException,
  // InterruptedException {
  // Arrange
  // var expectedGridResponse = new Grid();
  // expectedGridResponse.setTotalEnergyProduction(10);
  // expectedGridResponse.setTotalEnergyUsage(20);
  // expectedGridResponse.setTotalCustomers(50);
  //
  // var objectMapper = new ObjectMapper();
  // mockWebServer.enqueue(new MockResponse()
  // .setBody(objectMapper.writeValueAsString(expectedGridResponse))
  // .addHeader("Content-Type", "application/json")
  // );
  //
  // // Act
  // Grid actualGridResponse = gridSimulationService.getGridProduction().block();
  // var reportedRequest = mockWebServer.takeRequest();
  //
  // // Assert
  // assertAll(
  // () -> assertEquals(10, actualGridResponse.getTotalEnergyProduction_kWh()),
  // () -> assertEquals(20, actualGridResponse.getTotalEnergyUsage_kWh()),
  // () -> assertEquals(50, actualGridResponse.getTotalCustomers()),
  // () -> assertEquals("/grid/production", reportedRequest.getPath())
  // );
  // }

  @AfterEach
  void tearDown() throws IOException {
    mockWebServer.shutdown();
  }
}

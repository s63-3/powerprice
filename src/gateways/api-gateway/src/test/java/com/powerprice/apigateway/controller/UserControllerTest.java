package com.powerprice.apigateway.controller;

import static org.mockito.MockitoAnnotations.initMocks;

import com.powerprice.apigateway.services.UserService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Mock;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class UserControllerTest {
  private UserController sut;

  @Mock private UserService userService;

  @BeforeAll
  void setUp() {
    initMocks(this);
    userService = new UserService();
    sut = new UserController(userService);
  }
}

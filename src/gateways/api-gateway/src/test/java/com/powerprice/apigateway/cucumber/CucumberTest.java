package com.powerprice.apigateway.cucumber;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
    features = "src/test/java/resources",
    plugin = {"pretty"})
public class CucumberTest {}

package com.powerprice.apigateway.controller;

import static org.mockito.MockitoAnnotations.initMocks;

import com.powerprice.apigateway.services.GridService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Mock;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class GridSimulationControllerTest {
  private static GridController sut;

  @Mock private GridService gridSimulationService;

  @BeforeAll
  void setUp() {
    initMocks(this);
    // sut = new GridSimulationController(gridSimulationService);
  }
}

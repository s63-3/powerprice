package com.powerprice.apigateway.services;

import java.io.IOException;
import okhttp3.mockwebserver.MockWebServer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

class UserServiceTest {
  private MockWebServer mockWebServer;
  private UserService userService;

  @BeforeEach
  void setUp() throws IOException {
    mockWebServer = new MockWebServer();
    mockWebServer.start(8080);

    var baseUrl = String.format("http://localhost:%s", mockWebServer.getPort());
    userService = new UserService();
  }

  // @ TODO make registerEmployee() functionality in user-service
  // @Test
  // void shouldRegisterEmployee() throws InterruptedException {
  //        // Arrange
  //        mockWebServer.enqueue(new MockResponse());
  //
  //        // Act
  //        userService.registerEmployee(5, "John Doe");
  //        var recordedRequest = mockWebServer.takeRequest();
  //
  //        // Assert
  //        assertAll(
  //                () -> assertEquals("/grid/register?id=5&name=John%20Doe",
  // recordedRequest.getPath()),
  //                () -> assertEquals("POST", recordedRequest.getMethod())
  //        );
  // }

  @AfterEach
  void tearDown() throws IOException {
    mockWebServer.shutdown();
  }
}

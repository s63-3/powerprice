#Feature: Klantenaccount creëren
#
#  Als klantenmedewerker
#  Wil ik een klant kunnen registreren in het systeem
#  Zodat de klantgegevens bekend zijn voor het energienet en de klant gebruik kan maken van het systeem
#
#  Business rules:
#  - Persoonlijke klantgegevens bestaan uit e-mail, voornaam, achternaam, adres, type woning, woonplaats,
#    en postcode
#  - Klant staat geregistreerd zodra zonnepanelen, windmolens en persoonlijke gegevens staan geregistreerd
#  - Klant ontvangt een e-mail om zijn account te activeren zodra registratie voltooid is
#
#  Scenario Outline: Die ene waar ik de klant registreer
#    Given de hoeveelheid zonnepanelen en windmolens van de klant zijn duidelijk
#    When ik "<email>", "<voornaam>", "<achternaam>", "<adres>", "<type woning>", "<woonplaats>" en "<postcode>" specificeer
#    And ik verstuur de gegevens
#    Then krijg ik "<bericht>" te zien
#    And is er een e-mail verstuurd naar de klant voor het registreren van het account
#
#    Examples:
#    |                       | email               | voornaam | achternaam |       adres       | type woning | woonplaats | postcode |             bericht            |
#    | Valide gegevens       | jandegijn@gmail.com |   jan    |  de gijn   | Snoepjesstraat 40 | Appartement | Eindhoven  |  1012DC  | Klant succesvol geregistreerd. |
#    | E-mail ontbreekt      |                     |   jan    |  de gijn   | Snoepjesstraat 40 | Appartement | Eindhoven  |  1012DC  | E-mail is verplicht.           |
#    | Voornaam ontbreekt    | jandegijn@gmail.com |          |  de gijn   | Snoepjesstraat 40 | Appartement | Eindhoven  |  1012DC  | Voornaam is verplicht.         |
#    | E-mail invalide       | jandegijngmail.com  |   jan    |  de gijn   | Snoepjesstraat 40 | Appartement | Eindhoven  |  1012DC  | E-mail heeft incorrect format. |
#    | Achternaam ontbreekt  | jandegijn@gmail.com |   jan    |            | Snoepjesstraat 40 | Appartement | Eindhoven  |  1012DC  | Achternaam is verplicht.       |
#    | Adres ontbreekt       | jandegijn@gmail.com |   jan    |  de gijn   |                   | Appartement | Eindhoven  |  1012DC  | Adres is verplicht.            |
#    | Type woning ontbreekt | jandegijn@gmail.com |   jan    |  de gijn   | Snoepjesstraat 40 |             | Eindhoven  |  1012DC  | Type woning is verplicht.      |
#    | Woonplaats ontbreekt  | jandegijn@gmail.com |   jan    |  de gijn   | Snoepjesstraat 40 | Appartement |            |  1012DC  | Woonplaats is verplicht.       |
#    | Postcode ontbreekt    | jandegijn@gmail.com |   jan    |  de gijn   | Snoepjesstraat 40 | Appartement | Eindhoven  |          | Postcode is verplicht.         |

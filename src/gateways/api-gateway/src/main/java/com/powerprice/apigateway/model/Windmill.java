package com.powerprice.apigateway.model;

public class Windmill {
  private int customerId;
  private WindmillType windmillType;
  private int amount;

  public Windmill() {}

  public int getCustomerId() {
    return customerId;
  }

  public WindmillType getWindmillType() {
    return windmillType;
  }

  public void setCustomerId(int customerId) {
    this.customerId = customerId;
  }

  public void setWindmillType(WindmillType windmillType) {
    this.windmillType = windmillType;
  }

  public int getAmount() {
    return amount;
  }

  public void setAmount(int amount) {
    this.amount = amount;
  }
}

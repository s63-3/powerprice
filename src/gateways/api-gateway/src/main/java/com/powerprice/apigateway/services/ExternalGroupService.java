package com.powerprice.apigateway.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.powerprice.apigateway.model.GridStatus;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

@Service
public class ExternalGroupService {
  private WebClient webClientGroup1;
  private WebClient webClientGroup2;
  private WebClient webClientGroup4;
  private final Logger logger = LogManager.getLogger(ExternalGroupService.class);

  @Autowired
  public ExternalGroupService() {}

  @PostConstruct
  public void init() {
    this.webClientGroup1 =
        WebClient.builder()
            .baseUrl("http://fsenergyservice.daphneprojects.online")
            .defaultHeader(HttpHeaders.CONTENT_TYPE, "application/json")
            .build();
    this.webClientGroup2 =
        WebClient.builder()
            // TODO: set group 2 baseurl
            .baseUrl("groep 2 url")
            .defaultHeader(HttpHeaders.CONTENT_TYPE, "application/json")
            .build();
    this.webClientGroup4 =
        WebClient.builder()
            .baseUrl("http://35.189.86.8")
            .defaultHeader(HttpHeaders.CONTENT_TYPE, "application/json")
            .build();
  }

  public List<GridStatus> getExternalGridStatus() {
    List<GridStatus> gridStatuses = new ArrayList<>();

    try {
      gridStatuses.add(getGroup1GridStatus());
    }
    catch(Exception e) {
      logger.error(e.getMessage());
      gridStatuses.add(null);
    }

    try {
      gridStatuses.add(getGroup2GridStatus());
    }
    catch(Exception e) {
      logger.error(e.getMessage());
      gridStatuses.add(null);
    }

    try {
      gridStatuses.add(getGroup4GridStatus());
    }
    catch(Exception e) {
      logger.error(e.getMessage());
      gridStatuses.add(null);
    }

    return gridStatuses;
  }

  public GridStatus getGroup1GridStatus() {
    var dateForGroup1Status = LocalDate.now();

    return webClientGroup1
        .get()
        .uri(String.format("/energie/nh/%d/%d/%d", dateForGroup1Status.getYear(), dateForGroup1Status.getMonthValue(), dateForGroup1Status.getDayOfMonth()))
        .retrieve()
        .bodyToMono(GridStatus.class)
        .block();
  }

  public GridStatus getGroup2GridStatus() {
    return null;
  }

  public GridStatus getGroup4GridStatus() throws JsonProcessingException {
    // Groep 4 stuurt hun response als text/plain dus de ontvangen JSON moet nog naar een object worden geconverteerd
    String jsonResponse = webClientGroup4
            .get()
            .uri("/rest-forwarder/regional")
            .retrieve()
            .bodyToMono(String.class)
            .block();

    return new ObjectMapper().readValue(jsonResponse, GridStatus.class);
  }
}

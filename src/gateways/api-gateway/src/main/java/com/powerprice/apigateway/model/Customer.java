package com.powerprice.apigateway.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Customer {
  private String code;
  private String emailAddress;
  private String firstName;
  private String lastName;
  private String streetName;
  private String streetNumber;
  private String postAddress;
  private String country;
  private BuildingType buildingType;
  private String preferredLanguage;
  private String companyName;
  private Long maxVoltageLevel;
  private Long baseLoad;
  private Long peakLoad;

  public Customer() {}
}

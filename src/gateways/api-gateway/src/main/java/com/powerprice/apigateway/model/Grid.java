package com.powerprice.apigateway.model;

public class Grid {
  private int totalEnergyProduction_kWh;
  private int totalEnergyUsage_kWh;
  private int totalCustomers;

  public Grid() {}

  public Grid(int EnergyProduction) {
    this.totalEnergyProduction_kWh = EnergyProduction;
  }

  public int getTotalEnergyUsage_kWh() {
    return totalEnergyUsage_kWh;
  }

  public void setTotalEnergyUsage(int totalEnergyUsage) {
    this.totalEnergyUsage_kWh = totalEnergyUsage;
  }

  public int getTotalEnergyProduction_kWh() {
    return totalEnergyProduction_kWh;
  }

  public void setTotalEnergyProduction(int totalEnergyProduction) {
    this.totalEnergyProduction_kWh = totalEnergyProduction;
  }

  public int getTotalCustomers() {
    return totalCustomers;
  }

  public void setTotalCustomers(int totalCustomers) {
    this.totalCustomers = totalCustomers;
  }
}

package com.powerprice.apigateway.services;


import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import com.powerprice.apigateway.model.*;
import com.powerprice.apigateway.model.Production;
import com.powerprice.apigateway.model.ProductionSource;
import com.powerprice.apigateway.model.Usage;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;
import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
public class GridService {
  private WebClient webClient;

  private SecurityTokenService securityTokenService;

  private UserService userService;

  @Value("${gw.gridServiceUrl}")
  private String baseUrl;

  public void setBaseUrl(String baseUrl) {
    this.baseUrl = baseUrl;
  }

  @Autowired
  public GridService(SecurityTokenService securityTokenService, UserService userService) {
    this.securityTokenService = securityTokenService;
    this.userService = userService;
  }

  @PostConstruct
  public void init() {
    ExchangeStrategies exchangeStrategies = ExchangeStrategies.builder()
            .codecs(configurer -> configurer.defaultCodecs().maxInMemorySize(1000000)).build();
    this.webClient =
      WebClient
        .builder()
              .exchangeStrategies(exchangeStrategies)
        .baseUrl(baseUrl)
        .defaultHeader(HttpHeaders.CONTENT_TYPE, "application/json")
        .build();
  }

  public GridStatus getStatus() {
    return new GridStatus(
        new Date(), "Noord-Brabant", getGridProduction().block(), getGridUsage().block());
  }

  public Mono<Integer> getGridUsage() {
    return webClient.get().uri("/grid/usage").retrieve().bodyToMono(Integer.class);
  }

  public Mono<Integer> getGridProduction() {
    return webClient.get().uri("/grid/production").retrieve().bodyToMono(Integer.class);
  }

  public Mono<Integer> getCustomerUsage(HttpServletRequest request) {
    Long userId = securityTokenService.getUserIdFromRequest(request);
    return webClient
        .get()
        .uri("/grid/customers/usage/" + userId)
        .retrieve()
        .bodyToMono(Integer.class);
  }

  public Mono<Usage[]> getCustomerUsageDetails(HttpServletRequest request) {
    //return generateTotalUsage(1L);
    Long userId = securityTokenService.getUserIdFromRequest(request);
    // return generateTotalUsage(userId);
    return webClient
        .get()
        .uri("/grid/customers/usage-details/" + userId)
        .retrieve()
        .bodyToMono(Usage[].class);

  }

  public Mono<Production[]> getCustomerProductionDetails(HttpServletRequest request)
      throws ParseException {
    Long userId = securityTokenService.getUserIdFromRequest(request);
    // return generateTotalProduction(userId);
    return webClient
        .get()
        .uri("/grid/customers/production-details/" + userId)
        .retrieve()
        .bodyToMono(Production[].class);
  }

  private Mono<Usage[]> generateTotalUsage(Long userId) {
    Date now = new Date();
    Usage[] usages = new Usage[30];
    Calendar calendar = Calendar.getInstance();
    for (int i = 0; i <= 29; i++) {
      usages[i] =
          new Usage(
              (long) (i),
              userId,
              ThreadLocalRandom.current().nextDouble(1000, 10000 + 1),
              now.getTime());
      calendar.setTime(now);
      calendar.add(Calendar.DATE, 1);
      now = calendar.getTime();
    }
    Mono<Usage[]> mono = Mono.just(usages);
    return mono;
  }

  private Mono<Production[]> generateTotalProduction(Long userId) {
    Date now = new Date();
    Production[] productions = new Production[60];
    Calendar calendar = Calendar.getInstance();
    for (int i = 0; i <= 58; i += 2) {
      productions[i] =
          new Production(
              (long) (i),
              ProductionSource.SOLAR_PANEL,
              userId,
              ThreadLocalRandom.current().nextDouble(1000, 10000 + 1),
              now.getTime());
      productions[i + 1] =
          new Production(
              (long) (i),
              ProductionSource.WINDMILL,
              userId,
              ThreadLocalRandom.current().nextDouble(1000, 10000 + 1),
              now.getTime());
      calendar.setTime(now);
      calendar.add(Calendar.DATE, 1);
      now = calendar.getTime();
    }
    Mono<Production[]> mono = Mono.just(productions);
    return mono;
  }

  public Mono<Integer> getCustomerProduction(HttpServletRequest request) {
    Long userId = securityTokenService.getUserIdFromRequest(request);
    return webClient
        .get()
        .uri("/grid/customers/production/" + userId)
        .retrieve()
        .bodyToMono(Integer.class);
  }

  public Mono<Integer> getCustomerUsagePrice(HttpServletRequest request) {
    Long userId = securityTokenService.getUserIdFromRequest(request);
    return webClient
        .get()
        .uri("/grid/customers/usage/price/" + userId)
        .retrieve()
        .bodyToMono(Integer.class);
  }

  public Mono<Integer> getCustomerProductionPrice(HttpServletRequest request) {
    Long userId = securityTokenService.getUserIdFromRequest(request);
    return webClient
        .get()
        .uri("/grid/customers/production/price/" + userId)
        .retrieve()
        .bodyToMono(Integer.class);
  }
}

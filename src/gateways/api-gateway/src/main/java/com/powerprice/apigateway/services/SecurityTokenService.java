package com.powerprice.apigateway.services;

import javax.servlet.http.HttpServletRequest;
import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.keycloak.representations.AccessToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SecurityTokenService {
  private UserService userService;

  @Autowired
  public SecurityTokenService(UserService userService) {
    this.userService = userService;
  }

  public AccessToken getTokenFromRequest(HttpServletRequest request) {
    KeycloakAuthenticationToken token = (KeycloakAuthenticationToken) request.getUserPrincipal();
    KeycloakPrincipal keycloakPrincipal = (KeycloakPrincipal) token.getPrincipal();
    KeycloakSecurityContext session = keycloakPrincipal.getKeycloakSecurityContext();
    return session.getToken();
  }

  public Long getUserIdFromRequest(HttpServletRequest request) {
    String email = getTokenFromRequest(request).getEmail();
    return userService.getCustomerIdFromEmail(email);
  }
}

package com.powerprice.apigateway.controller;





import com.powerprice.apigateway.model.*;

import javax.servlet.http.HttpServletRequest;

import com.powerprice.apigateway.services.GridService;

import com.powerprice.apigateway.model.Production;
import com.powerprice.apigateway.model.Usage;
import com.powerprice.apigateway.model.Weather;
import com.powerprice.apigateway.model.WeatherSimulation;
import com.powerprice.apigateway.services.GridSimulationService;
import java.text.ParseException;
import com.powerprice.apigateway.services.GridService;
import com.powerprice.apigateway.services.GridSimulationService;
import java.text.ParseException;
import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/grid")
public class GridController {
  private final Logger logger = LogManager.getLogger(GridController.class);

  private GridService gridService;
  private GridSimulationService gridSimulationService;

  @Autowired
  public GridController(GridSimulationService gridSimulationService, GridService gridService) {
    this.gridSimulationService = gridSimulationService;
    this.gridService = gridService;
  }

  /**
   * Gets the grid status too compare with the grid status of other grid applications
   *
   * @return Grid status with consumption, production, date and region
   */
  @GetMapping("/employees/status")
  public GridStatus getStatus() {
    return gridService.getStatus();
  }

  /**
   * Gets the total usage of the grid from the grid service
   *
   * @return total usage in kwH
   */
  @GetMapping("/employees/usage")
  public Mono<Integer> getTotalUsage() {
    logger.log(Level.INFO, "getGridUsage");
    return gridService.getGridUsage();
  }

  /**
   * Gets the total production of the grid from the grid service
   *
   * @return total production in kwH
   */
  @GetMapping("/employees/production")
  public Mono<Integer> getTotalProduction() {
    logger.log(Level.INFO, "getGridProduction");
    return gridService.getGridProduction();
  }

  /**
   * Gets the total amount of production from the currently logged in customer
   *
   * @param request, used for identifying the user with a keycloak token
   * @return total production in kwH
   */
  @GetMapping("/customers/production")
  public Mono<Integer> getCustomerProduction(HttpServletRequest request) {
    logger.log(Level.INFO, "getCustomerProduction");
    return gridService.getCustomerProduction(request);
  }

  /**
   * Gets the total amount of production from the currently logged in customer, but the data is
   * returned with production objects
   *
   * @param request, used for identifying the user with a keycloak token
   * @return production of the user packaged in Production objects
   * @throws ParseException
   */
  @GetMapping("/customers/production-details")
  public Mono<Production[]> getCustomerProductionDetails(HttpServletRequest request)
      throws ParseException {
    logger.log(Level.INFO, "getCustomerProductionDetails");
    return gridService.getCustomerProductionDetails(request);
  }

  @GetMapping("/customers/usage-details")
  public Mono<Usage[]> getCustomerUsageDetails(HttpServletRequest request) {
    logger.log(Level.INFO, "getCustomerUsageDetails");
    return gridService.getCustomerUsageDetails(request);
  }

  /**
   * Gets the total amount of production from the currently logged in customer and calculates the
   * price
   *
   * @param request, used for identifying the user with a keycloak token
   * @return total production price in Euro's
   */
  @GetMapping("/customers/production/price")
  public Mono<Integer> getCustomerProductionPrice(HttpServletRequest request) {
    logger.log(Level.INFO, "getCustomerProductionPrice");
    return gridService.getCustomerProductionPrice(request);
  }

  /**
   * Gets the total amount of usage from the currently logged in customer
   *
   * @param request, used for identifying the user with a keycloak token
   * @return total usage in kwH
   */
  @GetMapping("/customers/usage")
  public Mono<Integer> getCustomerUsage(HttpServletRequest request) {
    logger.log(Level.INFO, "getCustomerUsage");
    return gridService.getCustomerUsage(request);
  }

  /**
   * Gets the total amount of usage from the currently logged in customer and calculates the price
   *
   * @param request, used for identifying the user with a keycloak token
   * @return total usage price in Euro's
   */
  @GetMapping("/customers/usage/price")
  public Mono<Integer> getCustomerUsagePrice(HttpServletRequest request) {
    logger.log(Level.INFO, "getCustomerUsagePrice");
    return gridService.getCustomerUsagePrice(request);
  }

  /**
   * Gets the total amount of production for windmills and solar panels with a certain weather
   * forecast
   *
   * @param weather, used passing the weather to the simulation service to calculate the production
   * @return total amount of production for windmills and solar panels
   */
  @PostMapping("/simulation/weather")
  public WeatherSimulation getSimulatedProduction(@RequestBody Weather weather) {
    logger.log(Level.INFO, "getSimulatedProduction");
    return gridSimulationService.getSimulatedProduction(weather);
  }
}

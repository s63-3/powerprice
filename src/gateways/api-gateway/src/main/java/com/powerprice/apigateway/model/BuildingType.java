package com.powerprice.apigateway.model;

public enum BuildingType {
  RIJTJESWONING,
  VRIJSTAANDEWONING,
  APPARTEMENT,
  BEDRIJFSGEBOUW,
}

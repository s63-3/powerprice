package com.powerprice.apigateway.services;

import com.powerprice.apigateway.model.SolarPanel;
import java.util.List;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
public class SolarPanelService {
  private WebClient webClient;

  @Value("${gw.solarPanelServiceUrl}")
  private String solarPanelServiceUrl;

  @PostConstruct
  private void init() {
    this.webClient =
        WebClient.builder()
            .baseUrl(solarPanelServiceUrl)
            .defaultHeader(HttpHeaders.CONTENT_TYPE, "application/json")
            .build();
  }

  public String addSolarPanels(List<SolarPanel> solarPanels) {
    return webClient
        .post()
        .uri("/solar-panels")
        .body(Mono.just(solarPanels), SolarPanel.class)
        .retrieve()
        .bodyToMono(String.class)
        .block();
  }
}

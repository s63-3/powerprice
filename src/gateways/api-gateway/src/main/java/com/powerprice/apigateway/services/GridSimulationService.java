package com.powerprice.apigateway.services;

import com.powerprice.apigateway.model.Weather;
import com.powerprice.apigateway.model.WeatherSimulation;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
public class GridSimulationService {
  private WebClient webClient;

  @Value("${gw.gridSimulationServiceUrl}")
  private String baseUrl;

  public void setBaseUrl(String baseUrl) {
    this.baseUrl = baseUrl;
  }

  public GridSimulationService() {}

  @PostConstruct
  public void init() {
    this.webClient =
        WebClient.builder()
            .baseUrl(baseUrl)
            .defaultHeader(HttpHeaders.CONTENT_TYPE, "application/json")
            .build();
  }

  public WeatherSimulation getSimulatedProduction(Weather weather) {
    return webClient
        .post()
        .uri("/simulation/weather")
        .body(Mono.just(weather), Weather.class)
        .retrieve()
        .bodyToMono(WeatherSimulation.class)
        .block();
  }
}

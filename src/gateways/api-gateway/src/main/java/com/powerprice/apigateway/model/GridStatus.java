package com.powerprice.apigateway.model;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GridStatus {
  private Date date;
  private String region;
  private double consumption;
  private double production;
}

package com.powerprice.apigateway.services;

import com.powerprice.apigateway.model.Windmill;
import java.util.List;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
public class WindmillService {
  private WebClient webClient;

  @Value("${gw.windmillServiceUrl}")
  private String windmillServiceUrl;

  @PostConstruct
  private void init() {
    this.webClient =
        WebClient.builder()
            .baseUrl(windmillServiceUrl)
            .defaultHeader(HttpHeaders.CONTENT_TYPE, "application/json")
            .build();
  }

  public String addWindmills(List<Windmill> windmills) {
    return webClient
        .post()
        .uri("/windmills")
        .body(Mono.just(windmills), Windmill.class)
        .retrieve()
        .bodyToMono(String.class)
        .block();
  }
}

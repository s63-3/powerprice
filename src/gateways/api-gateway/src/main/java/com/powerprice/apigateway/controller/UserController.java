package com.powerprice.apigateway.controller;

import com.powerprice.apigateway.model.Customer;
import com.powerprice.apigateway.services.UserService;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
  private final Logger logger = LogManager.getLogger(UserController.class);

  private UserService userService;

  @Autowired
  public UserController(UserService userService) {
    this.userService = userService;
  }

  /**
   * Register a new employee, is currently not used and might not be needed in production
   *
   * @param id
   * @param name
   */
  @PostMapping("/employees/register")
  public void registerEmployee(@RequestParam int id, @RequestParam String name) {
    logger.log(Level.INFO, "registerEmployee");
    userService.registerEmployee(id, name);
  }

  /**
   * Registers a new customer and makes a keycloak account so he can log into the application, used
   * by employees
   *
   * @param customer, complex customer object which contains all data needed for registration
   * @return id of the new customer
   */
  @PostMapping("/customers/register")
  public Long registerCustomer(@RequestBody Customer customer) {
    logger.log(Level.INFO, "registerCustomer");
    return userService.registerCustomer(customer);
  }

  /**
   * Gets the customer by email, is mostly used so the api gateway can get the id of the user to use
   * in other services
   *
   * @param email
   * @return customer id
   */
  @GetMapping("/customers/get/{email}")
  public Long getCustomerIdByEmail(@RequestParam(value = "email") String email) {
    logger.log(Level.INFO, "getCustomerIdByEmail");
    return userService.getCustomerIdFromEmail(email);
  }
}

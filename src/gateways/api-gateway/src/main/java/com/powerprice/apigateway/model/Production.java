package com.powerprice.apigateway.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.RepresentationModel;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Production extends RepresentationModel {
  private Long id;
  private ProductionSource source;
  private Long userId;
  private double produced;
  private Long time;
}

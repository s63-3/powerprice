package com.powerprice.apigateway.services;

import com.powerprice.apigateway.model.Customer;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
public class UserService {
  @Value("${gw.userServiceUrl}")
  private String userServiceUrl;

  private WebClient webClient;

  @PostConstruct
  private void init() {
    this.webClient =
        WebClient.builder()
            .baseUrl(userServiceUrl)
            .defaultHeader(HttpHeaders.CONTENT_TYPE, "application/json")
            .build();
  }

  public void registerEmployee(int id, String name) {
    webClient
        .post()
        .uri(
            uriBuilder ->
                uriBuilder
                    .path("/employees/register")
                    .queryParam("id", id)
                    .queryParam("name", name)
                    .build())
        .retrieve()
        .bodyToMono(String.class)
        .block();
  }

  public Long getCustomerIdFromEmail(String email) {
    return webClient
        .get()
        .uri("/customers/email/" + email)
        .retrieve()
        .bodyToMono(Long.class)
        .block();
  }

  public Long registerCustomer(Customer customer) {
    return webClient
        .post()
        .uri("/customers/register")
        .body(Mono.just(customer), Customer.class)
        .retrieve()
        .bodyToMono(Long.class)
        .block();
  }
}

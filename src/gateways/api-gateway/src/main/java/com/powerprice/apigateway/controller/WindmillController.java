package com.powerprice.apigateway.controller;

import com.powerprice.apigateway.model.Windmill;
import com.powerprice.apigateway.services.WindmillService;
import java.util.List;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/employees/windmills")
public class WindmillController {
  private final Logger logger = LogManager.getLogger(WindmillController.class);
  private WindmillService windmillService;

  @Autowired
  public WindmillController(WindmillService windmillService) {
    this.windmillService = windmillService;
  }

  @PostMapping
  public void addWindmills(@RequestBody List<Windmill> windmills) {
    logger.log(Level.INFO, windmillService.addWindmills(windmills));
  }
}

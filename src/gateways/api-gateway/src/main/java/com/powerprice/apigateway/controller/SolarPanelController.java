package com.powerprice.apigateway.controller;

import com.powerprice.apigateway.model.SolarPanel;
import com.powerprice.apigateway.services.SolarPanelService;
import java.util.List;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/employees/solar-panels")
public class SolarPanelController {
  private final Logger logger = LogManager.getLogger(SolarPanelController.class);
  private SolarPanelService solarPanelService;

  @Autowired
  public SolarPanelController(SolarPanelService solarPanelService) {
    this.solarPanelService = solarPanelService;
  }

  @PostMapping
  public void addSolarPanels(@RequestBody List<SolarPanel> solarPanels) {
    logger.log(Level.INFO, solarPanelService.addSolarPanels(solarPanels));
  }
}

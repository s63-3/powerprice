package com.powerprice.apigateway.controller;

import com.powerprice.apigateway.model.GridStatus;
import com.powerprice.apigateway.services.ExternalGroupService;
import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/external")
public class ExternalController {

  private ExternalGroupService externalGroupService;

  public ExternalController(ExternalGroupService externalGroupService) {
    this.externalGroupService = externalGroupService;
  }

  @GetMapping("/status")
  public List<GridStatus> getExternalGridStatus() {
    return externalGroupService.getExternalGridStatus();
  }
}

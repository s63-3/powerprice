package com.powerprice.apigateway.model;

import com.fasterxml.jackson.annotation.JsonValue;

public enum ProductionSource {
  SOLAR_PANEL,
  WINDMILL;

  @JsonValue
  public int toValue() {
      return ordinal();
  }
}

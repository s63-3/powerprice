package com.powerprice.apigateway.model;

public class SolarPanel {
  private int customerId;
  private int numberOfSquareMeters;
  private int amount;

  public SolarPanel() {}

  public int getCustomerId() {
    return customerId;
  }

  public void setCustomerId(int customerId) {
    this.customerId = customerId;
  }

  public int getNumberOfSquareMeters() {
    return numberOfSquareMeters;
  }

  public void setNumberOfSquareMeters(int numberOfSquareMeters) {
    this.numberOfSquareMeters = numberOfSquareMeters;
  }

  public int getAmount() {
    return amount;
  }

  public void setAmount(int amount) {
    this.amount = amount;
  }
}

package com.powerprice.apigateway.model;

public enum WindmillType {
  TURBINE(50, 90),
  MOD_2(50, 120),
  SAVONIUS(70, 100),
  DARRIEUS(20, 130),
  GROWIAN(40, 100);

  private final int minHourCapacity;
  private final int maxHourCapacity;

  private WindmillType(int minHourCapacity, int maxHourCapacity) {
    this.minHourCapacity = minHourCapacity;
    this.maxHourCapacity = maxHourCapacity;
  }

  public int getMaxHourCapacity() {
    return maxHourCapacity;
  }

  public int getMinHourCapacity() {
    return minHourCapacity;
  }
}

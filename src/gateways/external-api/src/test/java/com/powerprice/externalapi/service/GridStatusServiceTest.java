package com.powerprice.externalapi.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.powerprice.externalapi.model.GridStatus;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import java.io.IOException;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertAll;

class GridStatusServiceTest {

    private static MockWebServer mockWebServer;
    private GridStatusService gridStatusService;

    @BeforeAll
    static void initialize() throws IOException {
        mockWebServer = new MockWebServer();
        mockWebServer.start();
    }

    @BeforeEach
    void setUp() {
        var baseUrl = String.format("http://localhost:%s", mockWebServer.getPort());
        gridStatusService = new GridStatusService(baseUrl);
    }

    @Test
    void getCurrentUsage_shouldReturnDataFromMockRequest_whenDataIsValid() throws JsonProcessingException, InterruptedException {
        // Arrange
        var objectMapper = new ObjectMapper();
        mockWebServer.enqueue(new MockResponse()
            .setBody(objectMapper.writeValueAsString(10))
            .addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
        );

        // Act
        int response = gridStatusService.getCurrentUsage();
        RecordedRequest reportedRequest = mockWebServer.takeRequest();

        // Assert
        assertEquals(10, response);
        assertEquals("/grid/usage", reportedRequest.getPath());
        assertEquals("GET", reportedRequest.getMethod());
    }

    @Test
    void getCurrentProduction_shouldReturnDataFromMockRequest_whenDataIsValid() throws JsonProcessingException, InterruptedException {
        // Arrange
        var objectMapper = new ObjectMapper();
        mockWebServer.enqueue(new MockResponse()
                .setBody(objectMapper.writeValueAsString(20))
                .addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
        );

        // Act
        int response = gridStatusService.getCurrentProduction();
        RecordedRequest reportedRequest = mockWebServer.takeRequest();

        // Assert
        assertEquals(20, response);
        assertEquals("/grid/production", reportedRequest.getPath());
        assertEquals("GET", reportedRequest.getMethod());
    }

    @Test
    void getGridStatus_shouldReturnDataFromMockRequest_whenDataIsValid() throws JsonProcessingException, InterruptedException {
        // Arrange
        // It is required that usage is retrieved before production in the SUT, because MockWebServer uses a queue for the requests
        var gridUsage = 10;
        var gridProduction = 20;

        var objectMapper = new ObjectMapper();
        mockWebServer.enqueue(new MockResponse()
                .setBody(objectMapper.writeValueAsString(gridUsage))
                .addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
        );
        mockWebServer.enqueue(new MockResponse()
                .setBody(objectMapper.writeValueAsString(gridProduction))
                .addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
        );

        // Act
        GridStatus response = gridStatusService.getGridStatus();
        RecordedRequest reportedGetUsageRequest = mockWebServer.takeRequest();
        RecordedRequest reportedGetProductionRequest = mockWebServer.takeRequest();

        // Assert
        assertAll("grid status should have properties assigned based on response",
            () -> assertEquals(10, response.getConsumption()),
            () -> assertEquals(20, response.getProduction()),
            () -> assertEquals("Noord-Brabant", response.getRegion())
        );

        assertEquals("/grid/usage", reportedGetUsageRequest.getPath());
        assertEquals("/grid/production", reportedGetProductionRequest.getPath());
    }

    @AfterAll
    static void teardown() throws IOException {
        mockWebServer.shutdown();
    }
}

package com.powerprice.externalapi.controller;

import com.powerprice.externalapi.model.GridStatus;
import com.powerprice.externalapi.service.GridStatusService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
class GridStatusControllerTest {

    private GridStatusController gridStatusController;

    @Mock
    private GridStatusService gridStatusService;

    @BeforeEach
    void setUp() {
        gridStatusController = new GridStatusController(gridStatusService);
    }

    @Test
    void getGridStatus_shouldRespondWithGridStatusServiceData_WhenReceivedDataIsValid() {
        // Arrange
        var gridStatusServiceResponse = new GridStatus(new Date(10000), "Noord-Brabant", 10, 20);
        Mockito.when(gridStatusService.getGridStatus()).thenReturn(gridStatusServiceResponse);

        // Act
        GridStatus response = gridStatusController.getGridStatus();

        // Assert
        assertAll("grid status should have properties assigned based on response from controller",
                () -> assertEquals(10, response.getConsumption()),
                () -> assertEquals(20, response.getProduction()),
                () -> assertEquals("Noord-Brabant", response.getRegion()),
                () -> assertEquals(new Date(10000), response.getDate())
        );
    }

    @Test
    void getGridStatusAlternative_shouldRespondWithGridStatusServiceData_WhenReceivedDataIsValid() {
        // Arrange
        var gridStatusServiceResponse = new GridStatus(new Date(10000), "Noord-Brabant", 10, 20);
        Mockito.when(gridStatusService.getGridStatus()).thenReturn(gridStatusServiceResponse);

        // Act
        GridStatus response = gridStatusController.getGridStatusAlternative();

        // Assert
        assertAll("grid status should have properties assigned based on response from controller",
                () -> assertEquals(10, response.getConsumption()),
                () -> assertEquals(20, response.getProduction()),
                () -> assertEquals("Noord-Brabant", response.getRegion()),
                () -> assertEquals(new Date(10000), response.getDate())
        );
    }
}

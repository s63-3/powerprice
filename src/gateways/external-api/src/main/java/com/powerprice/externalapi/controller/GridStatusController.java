package com.powerprice.externalapi.controller;

import com.powerprice.externalapi.model.GridStatus;
import com.powerprice.externalapi.service.GridStatusService;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/status")
public class GridStatusController {
  private final Logger logger = LogManager.getLogger(GridStatusController.class);

  private GridStatusService service;

  @Autowired
  public GridStatusController(GridStatusService service) {
    this.service = service;
  }


  @GetMapping
  public GridStatus getGridStatus() {
    logger.log(Level.INFO, "getStatus");
    return service.getGridStatus();
  }

  @GetMapping("/getStatus")
  public GridStatus getGridStatusAlternative() {
    logger.log(Level.INFO, "getStatusAlternative");
    return service.getGridStatus();
  }

}

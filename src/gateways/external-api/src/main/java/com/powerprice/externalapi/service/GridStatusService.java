package com.powerprice.externalapi.service;

import com.powerprice.externalapi.model.GridStatus;
import java.util.Date;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

@Service
public class GridStatusService {

  private WebClient webClient;

  public GridStatusService(
          @Value("${serviceUrl.grid}") String gridServiceUrl
  ) {
    this.webClient =
            WebClient.builder()
                    .baseUrl(gridServiceUrl)
                    .defaultHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                    .build();
  }

  public int getCurrentUsage() {
    return webClient.get()
            .uri("/grid/usage")
            .retrieve()
            .bodyToMono(int.class)
            .block();
  }

  public GridStatus getGridStatus() {
    return new GridStatus(new Date(), "Noord-Brabant", getCurrentUsage(), getCurrentProduction());
  }

  public int getCurrentProduction() {
    return webClient.get()
            .uri("/grid/production")
            .retrieve()
            .bodyToMono(int.class)
            .block();
  }

}

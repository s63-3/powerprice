package com.powerprice.externalapi.model;

import java.util.Date;

public class GridStatus {
  private Date date;
  private String region;
  private double consumption;
  private double production;

  public GridStatus() {}

  public GridStatus(Date date, String region, double consumption, double production) {
    this.date = date;
    this.region = region;
    this.consumption = consumption;
    this.production = production;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  public String getRegion() {
    return region;
  }

  public void setRegion(String region) {
    this.region = region;
  }

  public double getConsumption() {
    return consumption;
  }

  public void setConsumption(double consumption) {
    this.consumption = consumption;
  }

  public double getProduction() {
    return production;
  }

  public void setProduction(double production) {
    this.production = production;
  }
}
